function e=auditoryfbank(x,fs)
% AUDITORYFBANK performs an auditory filterbank analysis on a signal
%   E=AUDITORYFBANK(X,FS) passes signal X, sampled at FS samples/sec
%   through a 19-channel auditory filterbank, smoothing and downsampling
%   energies to 100 frames per second. Output is in decibels.
%
 
% get output energy sample points
x=reshape(x,[length(x) 1]);         % force to be a column
eidx=fs/100:fs/100:length(x);       % sampling indices (100 frame/sec)
ne=length(eidx);                    % number of output energy frames
 
% these are the filter cut-offs
cuts=[180 300 420 540 660 780 900 1075 1225 1375 1525 1700 1900 2100 2300 2550 2850 3150 3475 4000];
nf=length(cuts)-1;                  % number of filters
 
% initialise the output energy table
e=zeros(ne,nf);
 
% build a smoothing filter
[sb,sa]=butter(4,2*50/fs);          % low-pass at half output frame rate
 
% for each channel in turn
for i=1:nf
    fprintf('Calculating channel %d (%d-%dHz)\n',i,cuts(i),cuts(i+1));
    % build the band-pass filter
    [b,a]=butter(4,[2*cuts(i)/fs 2*cuts(i+1)/fs]);
    % filter the signal
    y=filter(b,a,x);
    % rectify and smooth
    sy=filter(sb,sa,abs(y));
    % sample and convert to decibels
    sy=max(sy,eps);                   % force to be > 0
    e(:,i)=100+20*log10(sy(eidx));    % downsample and convert to dB
end