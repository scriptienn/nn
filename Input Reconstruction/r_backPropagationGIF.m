function reconstruction = r_backPropagationGIF(net, spectrogram, layer, ...
                                            class, average, t, f, startingSpectrogram)
%% r_backPropagationGIF
% Author: Thomas Churchman
% 
% Performs backpropagation with gradient descent to reconstruct network
% input.
%
% Saves an animated gif with frames of certain stages of the 
% reconstruction.
% 
% Input:
% - net: The network to reconstruct the input for.
% - spectrogram: The input into the network that is to be reconstructed.
% - layer: The layer to use the activation pattern of for reconstruction.
% - class: The class of the spectrogram (only required for the loss layer).
% - average: The average spectrogram.
% - t: The time-point label vector.
% - f: The frequency-bin label vector.
% - startingSpectrogram (optional): The spectrogram to start
%   backpropagating from. If not supplied, the algorithm will generate a
%   random starting point.
%
% Output:
% - reconstruction: The reconstructed input.

    opts.reportEvery = 300;
    opts.lambda = 0.1;
    % Adagrad:
    %opts.adaMasterStepSize = 1e-2;
    %opts.adaFudgeFactor = 1e-15;    
    % Simpler learning rate:
    %opts.learningRate = 0.001*[...
    %    ones(1,1600), ...
    %    0.1 * ones(1,1000), ...
    %    0.01 * ones(1,1000), ...
    %    0.001 * ones(1,700), ...
    %    0.0001 * ones(1,600), ...
    %    0.00001 * ones(1,600)];
    % Simpler learning rate, longer schedule:
    opts.learningRate = 0.00001*[...
        ones(1,2000), ...
        0.5 * ones(1,1700), ...
        0.1 * ones(1,1000), ...
        0.05 * ones(1,700), ...
        0.01 * ones(1,600), ...
        0.005 * ones(1,600), ...
        0.001 * ones(1,400), ...
        0.0005 * ones(1,400), ...
        0.0001 * ones(1,200)];
    % Momentum:
    opts.momentumDecay = 0.9; 
    errors = zeros([numel(opts.learningRate) 1]);
    if layer < numel(net.layers)
        % Slice the network after the layer that is going to be used for
        % reconstruction.
        net.layers(layer+1:end) = [];
    else
        % Network is not going to be sliced, last layer is softmax-loss
        % which requires the ground class.
        net.layers{end}.class = class;
    end
    
    % Calculate the target activations
    target = vl_simplenn(net, spectrogram, [], [], 'disableDropout', true);
    
    % Slight optimization: add a custom layer at the end of the network
    % that holds the feature we're trying to reproduce, as to allow the
    % gradient calculation to be done immediately in Matconvnet's 
    % backpropagation step.
    derivativeLayer.type = 'custom';
    derivativeLayer.activationToReconstruct = target(end).x;
    derivativeLayer.forward = @customLayerForward;
    derivativeLayer.backward = @customLayerBackward;
    
    % Set the custom layer's mask which is used to select which units to 
    % use of the filter layer.
    derivativeLayer.mask = ones(size(target(end).x), 'single'); 
    
    % Add the layer to the network.
    net.layers{end+1} = derivativeLayer;
    
    % Initialize the reconstruction.
    s = size(spectrogram);
    s = s(1:2);
    
    if exist('startingSpectrogram', 'var')
        reconstruction = startingSpectrogram;
    else
        reconstruction = randn([s(1) s(2)], 'single') * 0.002;
    end
    %reconstruction = zeros([s(1) s(2)], 'single');
    %reconstruction = spectrogram(:,:,1) + randn([s(1) s(2)], 'single') * 0.01;
    
    frameEvery = 10;
    figure(1);
    filename = 'reconstruction.gif';
    plotSpectrogram(reconstruction+average, t, f);
    set(gcf,'color','w');
    drawnow;
    frame = getframe(1);
    im = frame2im(frame);
    [imind,cm] = rgb2ind(im,256);
    imwrite(imind,cm,filename,'gif', 'DelayTime', 0, 'Loopcount',inf);
    
    % Initialize training variables.
    % historicalReconstructionGradients = zeros([s(1) s(2)], 'single');
    momentum = 0;
    previousLearningRate = 0;
    
    boldDriverFactor = 1;
    prevError = -1;
    for epoch=1:numel(opts.learningRate)
    %for epoch=1:50000
        res = vl_simplenn(net, reconstruction, single(1), [], 'disableDropout', true, 'autoencoder', false);
        
        dzdx = res(1).dzdx;
        dzdx = dzdx(:,:,1) + reconstruction .* opts.lambda;
        
        % Adagrad
        %gradient = dzdx;
        %historicalReconstructionGradients = historicalReconstructionGradients + gradient .^ 2;
        %adjustedReconstructionGradient = gradient ./ (opts.adaFudgeFactor + sqrt(historicalReconstructionGradients));
        
        % Update reconstruction
        %reconstruction = reconstruction - opts.adaMasterStepSize .* adjustedReconstructionGradient;
        %reconstruction = reconstruction - 0.0001 * dzdx;
        %%{
        learningRate = opts.learningRate(min(epoch, numel(opts.learningRate))) ;
        if previousLearningRate ~= learningRate
            fprintf('Learning rate: %f \n', learningRate);
            momentum = 0;
            previousLearningRate = learningRate;
        end
        momentum = opts.momentumDecay * momentum ...
            - learningRate * boldDriverFactor * dzdx;
        reconstruction = reconstruction + momentum;
        %%}
        
        % Calculate the sum of squared errors of the reconstruction 
        % activation versus the target activations.
        error = sum(sum((target(end).x - res(end-1).x).^2));
        errors(epoch) = error;
        if prevError ~= -1
            if error > prevError
                boldDriverFactor = boldDriverFactor * 0.9;
            else
                boldDriverFactor = boldDriverFactor * 1.01;
            end
        end
        prevError = error;
            
        % Early cut-off
        if(epoch > 200)
            if(not(error < errors(epoch-200) * 0.999))
                return; 
            end
        end
        
        if boldDriverFactor < 0.0005
            % Error has only been increasing for a long time, cancel
            % learning.
            return 
        end
        
        % Error reporting
        if mod(epoch-1, opts.reportEvery) == 0
            fprintf('Sum squared errors: %f; Bold Driver: %f\n', error, boldDriverFactor);
        end
        
        if(mod(epoch, frameEvery) == 0)
            plotSpectrogram(reconstruction+average, t, f);
            set(gcf,'color','w');
            drawnow;
            frame = getframe(1);
            im = frame2im(frame);
            [imind,cm] = rgb2ind(im,256);
            imwrite(imind,cm,filename,'gif', 'DelayTime', 0, 'WriteMode','append');
        end
    end
    
end

function thisRes = customLayerForward(~, previousRes, thisRes)
%% customLayerForward
% Author: Thomas Churchman
%
% Function that implements the custom layer's forward propagation.
%
% Input: 
% - layer: The layer this function is being invoked for.
% - previousRes: The result struct of the layer in front of this layer in
%   the network.
% - thisRes: The result struct of this layer in the network.
% Output:
% - thisRes: The result struct of this layer in the network.

    thisRes.x = previousRes.x;
end

function previousRes = customLayerBackward(layer, previousRes, thisRes)
%% customLayerBackward
% Author: Thomas Churchman
%
% Function that implements the custom layer's backward propagation.
%
% Input: 
% - layer: The layer this function is being invoked for.
% - previousRes: The result struct of the layer in front of this layer in
%   the network.
% - thisRes: The result struct of this layer in the network.
% Output:
% - thisRes: The result struct of this layer in the network.

    previousRes.dzdx = (thisRes.x-layer.activationToReconstruct) .* layer.mask;
end