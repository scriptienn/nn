function r_runExperiments
%% r_runExperiments
% Author: Thomas Churchman
%
% Run reconstruction experiments.

    [~, ~, matconvnetPath, inputPath, ~] = setPaths();
    
    run([matconvnetPath '\matlab\vl_setupnn.m']);

    dir = ['D:\Uni2\Scriptie\reconstructionOutput\' datestr(datetime, 'yyyy-mm-dd HHMMSS')];
    if ~exist(dir, 'dir')
        mkdir(dir); 
    end
    
    data = load ([inputPath '\test\_main.mat']);
    
    % Set up paths to the various required networks
    normalNetworkPath = 'D:\Uni2\Scriptie\output\experiments cnn_arch_frequencyConvolution\experiment 2015-06-05 cnn_arch_frequencyConvolution newMap real fft 201 freq bins 16 time bins\net-epoch-372-GATHERED.mat';
    decoderNetworkLayer1Path  = 'D:\Uni2\Scriptie\output\experiments decoder cnn_arch_frequencyConvolution_fullPadding fromRandom\experiment dec 2015-06-26 cnn_arch_frequencyConvolution_fullPadding layer1 newMap real fft 201 freq bins 16 time bins\net-epoch-37.mat';
    decoderNetworkLayer3Path  = 'D:\Uni2\Scriptie\output\experiments decoder cnn_arch_frequencyConvolution_fullPadding fromRandom\experiment dec 2015-06-28 v3 cnn_arch_frequencyConvolution_fullPadding layer3 newMap real fft 201 freq bins 16 time bins\net-epoch-4.mat';
    decoderNetworkLayer6Path  = 'D:\Uni2\Scriptie\output\experiments decoder cnn_arch_frequencyConvolution_fullPadding fromRandom\experiment dec 2015-06-28 v3 cnn_arch_frequencyConvolution_fullPadding layer6 newMap real fft 201 freq bins 16 time bins\net-epoch-4.mat';
    decoderNetworkLayer9Path  = 'D:\Uni2\Scriptie\output\experiments decoder cnn_arch_frequencyConvolution_fullPadding fromRandom\experiment dec 2015-06-28 v3 cnn_arch_frequencyConvolution_fullPadding layer9 newMap real fft 201 freq bins 16 time bins\net-epoch-4.mat';
    decoderNetworkLayer12Path = 'D:\Uni2\Scriptie\output\experiments decoder cnn_arch_frequencyConvolution_fullPadding fromRandom\experiment dec 2015-06-28 v3 cnn_arch_frequencyConvolution_fullPadding layer12 newMap real fft 201 freq bins 16 time bins\net-epoch-5.mat';
    decoderNetworkLayer14Path = 'D:\Uni2\Scriptie\output\experiments decoder cnn_arch_frequencyConvolution_fullPadding fromRandom\experiment dec 2015-06-28 v2 cnn_arch_frequencyConvolution_fullPadding layer14 newMap real fft 201 freq bins 16 time bins\net-epoch-7.mat';
    decoderNetworkLayer16Path = 'D:\Uni2\Scriptie\output\experiments decoder cnn_arch_frequencyConvolution_fullPadding fromRandom\experiment dec 2015-06-28 v2 cnn_arch_frequencyConvolution_fullPadding layer16 newMap real fft 201 freq bins 16 time bins\net-epoch-8.mat';
    decoderNetworkLayer18Path = 'D:\Uni2\Scriptie\output\experiments decoder cnn_arch_frequencyConvolution_fullPadding fromRandom\experiment dec 2015-06-28 v2 cnn_arch_frequencyConvolution_fullPadding layer18 newMap real fft 201 freq bins 16 time bins\net-epoch-11.mat';
    
    nets = containers.Map;
    nets('normalNetwork') = load(normalNetworkPath);
    nets('decoderNetworkLayer1') = load(decoderNetworkLayer1Path);
    nets('decoderNetworkLayer3') = load(decoderNetworkLayer3Path);
    nets('decoderNetworkLayer6') = load(decoderNetworkLayer6Path);
    nets('decoderNetworkLayer9') = load(decoderNetworkLayer9Path);
    nets('decoderNetworkLayer12') = load(decoderNetworkLayer12Path);
    nets('decoderNetworkLayer14') = load(decoderNetworkLayer14Path);
    nets('decoderNetworkLayer16') = load(decoderNetworkLayer16Path);
    nets('decoderNetworkLayer18') = load(decoderNetworkLayer18Path);
    
    net  = nets('normalNetwork').net;
    info = nets('normalNetwork').info;
    
    % One of each phones
    % generated with:
    % ps = []; for i=1:48 a = find(cell2mat(phonemes)==i); b = randperm(numel(a)); ps = [ps a(b(1:5))']; end
    ps = [28158,5801,21616,5531,38558,27881,23029,33747,3727,6594,26404,40999,14156,4137,11419,18978,26374,6203,1457,8598,13691,28110,32800,715,31381,25133,40804,40188,45343,7155,41341,20426,37249,29579,18838,43586,45261,42999,42763,45368,5974,29369,17306,20161,4533,4755,41842,7203];
    %ps = [28158, 5801];
    
    % Must be a (not necessarily strict) subset of
    % {'simulatedAnnealing', 'backPropagation', 'decoding', 'decodingBackPropagation'}
    types = {'decoding', 'decodingBackPropagation', 'backPropagation', 'simulatedAnnealing'};
    
    averaging = 10;
    
    layers = [1 3 6 9 12];
    
    % Four chirps: one from 0 to 8000hz, one from 2000 to 6000hz, etc.
    chirps = [0 8000; 2000 6000; 8000 0; 6000 2000];
    
    meta = cell([length(types) 2]);
    errors = cell([length(types) 1]);
    reconstructionFileNames = cell([length(types) 1]);
    
    for typeIdx = 1:length(types)
        type = types{typeIdx};
        
        fprintf(['================\nExperiment: ' type '\n']);
        
        d = size(chirps);
        
        meta{typeIdx, 1} = type;
        
        meta{typeIdx, 2} = struct('chirps', {cell([d(1) 2])}, ...
                                  'phones', {cell([numel(ps) 2])});
        settings =  struct('layers', layers, ...
                           'averaging', averaging, ...
                           'phones', ps, ...
                           'chirps', chirps, ...
                           'average', data.average);
        errors{typeIdx, 1} = struct('chirps', {cell([d(1) 1])}, ...
                                    'phones', {cell([numel(ps) 1])});
        reconstructionFileNames{typeIdx, 1} = struct('chirps', {cell([d(1) 1])}, ...
                                                     'phones', {cell([numel(ps) 1])});
                                
        % Reconstruct chirps
        chirpErrorsTotal = cell([d(1) 1]);
        for chirp=1:d(1)
            fs = 16000;
            samples = 2554;

            startFreq = chirps(chirp,1);
            endFreq   = chirps(chirp,2);

            filePrefix = [type '_c_' num2str(chirp)];

            fprintf(['================\nChirp ' num2str(chirp) '\n']);
            reconstructInfo = ['Chirp: ' num2str(chirp)];
            reconstructInfo = [reconstructInfo '\r\n' num2str(startFreq) 'hz -> ' num2str(endFreq) 'hz'];

            meta{typeIdx, 2}.chirps{chirp, 1} = struct('chirp', chirp, ...
                                                        'startFreq', startFreq, ...
                                                        'endFreq', endFreq);
            
            % Generate file names
            for layerIdx = 1:numel(layers)
                layer = layers(layerIdx);
                fileName = [filePrefix '-' num2str(layer) '.mat'];
                reconstructionFileNames{typeIdx, 1}.chirps{chirp, 1}{layerIdx, 1} = fileName;
            end
            
            % Generate chirp
            wave = chirpGen(startFreq, endFreq, samples, fs);

            % Generate spectrogram
            input = preprocess(wave, fs, 'fft');

            meta{typeIdx, 2}.chirps{chirp, 2} = struct('spectrogram', input, ...
                                                        'wave', wave);
            
            % Prepare input for reconstruction
            input = input - data.average;

            [reconstructions, reconstructionInfoTemp, reconstructionErrors] = r_reconstruct(type, nets, input, wave, data.t, data.f, data.average, layers, ...
                averaging, dir, filePrefix, 1);

            errors{typeIdx, 1}.chirps{chirp, 1} = reconstructionErrors;
            
            reconstructInfo = [reconstructInfo reconstructionInfoTemp]; 

            fileID = fopen([dir '\' filePrefix '.txt'],'w');
            fprintf(fileID, reconstructInfo);
            fclose(fileID);
        end

        % Reconstruct actual spectrograms
        phoneErrorsTotal = cell([numel(ps) 1]);
        for i=1:numel(ps)
            p = ps(i);
            phone = data.phonemes{p};

            filePrefix = [type '_p_' num2str(p)];
            
            fprintf(['================\nPhone ' num2str(phone) '\n']);
            reconstructInfo = ['Phone: ' num2str(phone) ' - ' info.phones{phone}];
            reconstructInfo = [reconstructInfo '\r\n' 'Phone (folded): ' num2str(info.phoneGroups(phone)) ' - ' info.phones{info.phoneGroups(phone)}];

            meta{typeIdx, 2}.phones{i, 1} = struct('phoneIdx', p, ...
                                                        'phone', phone, ...
                                                        'phoneLetter', info.phones{phone}, ...
                                                        'phoneFolded', info.phoneGroups(phone), ...
                                                        'phoneFoldedLetter', info.phones{info.phoneGroups(phone)});
            
            % Generate file names
            for layerIdx = 1:numel(layers)
                layer = layers(layerIdx);
                fileName = [filePrefix '-' num2str(layer) '.mat'];
                reconstructionFileNames{typeIdx, 1}.phones{i, 1}{layerIdx, 1} = fileName;
            end

            % Read spectrogram from file.
            input = readSpectrogram([inputPath '\test\' data.phonemeFileNames{p}], data.height, data.width, data.depth);
            wave = load([inputPath '\test\' data.waveFileNames{p}]);

            meta{typeIdx, 2}.phones{i, 2} = struct('spectrogram', input, ...
                                                   'wave', wave.phonemeWave);
            
            % Prepare input for reconstruction.
            input = input - data.average;
            % input = gpuArray(input);

            % Generate network prediction.
            predictions = utils_getPredictions(net, input, phone);
            if info.phoneGroups(predictions(1)) == info.phoneGroups(phone)
                fprintf('Network predicts correctly.\n');
                reconstructInfo = [reconstructInfo '\r\nNetwork predicts correctly.'];
            else
                fprintf('Network predicts erroneously.\n');
                reconstructInfo = [reconstructInfo '\r\nNetwork predicts erroneously.'];

                if sum(info.phoneGroups(predictions(1:5)) == info.phoneGroups(phone)) > 0
                    fprintf('Correct prediction is in top 5 predictions.\n');
                    reconstructInfo = [reconstructInfo '\r\nCorrect prediction is in top 5 predictions.'];
                end
            end

            [reconstructions, reconstructionInfoTemp, reconstructionErrors] = r_reconstruct(type, nets, input, wave.phonemeWave, data.t, data.f, data.average, layers, ...
                averaging, dir, filePrefix, phone);

            errors{typeIdx, 1}.phones{i, 1} = reconstructionErrors;
            
            reconstructInfo = [reconstructInfo reconstructionInfoTemp];

            fileID = fopen([dir '\' filePrefix '.txt'],'w');
            fprintf(fileID, reconstructInfo);
            fclose(fileID);
        end
    end
    
    save([dir '\_experiments.mat'], ...
                'meta', ...
                'settings', ...
                'errors', ...
                'reconstructionFileNames');
end