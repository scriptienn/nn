%% testReconstruction
% Author: Thomas Churchman

[~, ~, matconvnetPath, inputPath, ~] = setPaths();

run([matconvnetPath '\matlab\vl_setupnn.m']);

dir = ['D:\Uni2\Scriptie\reconstructionOutput\' datestr(datetime, 'yyyy-mm-dd HHMMSS')];
if ~exist(dir, 'dir')
    mkdir(dir); 
end

% One of each phoneme
% generated with:
% ps = []; for i=1:48 a = find(cell2mat(phonemes)==i); b = randperm(numel(a)); ps = [ps a(b(1:5))']; end
ps = [28158,5801,21616,5531,38558,27881,23029,33747,3727,6594,26404,40999,14156,4137,11419,18978,26374,6203,1457,8598,13691,28110,32800,715,31381,25133,40804,40188,45343,7155,41341,20426,37249,29579,18838,43586,45261,42999,42763,45368,5974,29369,17306,20161,4533,4755,41842,7203];

averaging = 10;
layers = [1 3 6 9 12 14 17];

% Reconstruct chirps
chirps = [0 8000; 2000 6000; 8000 0; 6000 2000];
d = size(chirps);
chirpErrorsTotal = cell([d(1) 1]);
for chirp=1:d(1)
    fs = 16000;
    samples = 2554;
    
    startFreq = chirps(chirp,1);
    endFreq   = chirps(chirp,2);
    
    filePrefix = ['c' num2str(chirp)];
    
    fprintf(['================\Chirp ' num2str(chirp) '\n']);
    reconstructInfo = ['Chirp: ' num2str(chirp)];
    reconstructInfo = [reconstructInfo '\r\n' num2str(startFreq) 'hz -> ' num2str(endFreq) 'hz'];

    % Generate chirp
    wave = chirpGen(startFreq, endFreq, samples, fs);
    
    % Generate spectrogram
    input = preprocess(wave, fs, 'fft');
    
    % Prepare input for reconstruction
    input = input - average;
    
    [reconstructions, reconstructionInfoTemp, errors] = reconstruct(net, input, t, f, average, layers, ...
        averaging, dir, filePrefix, 1);
    chirpErrorsTotal{chirp} = errors;
    
    reconstructInfo = [reconstructInfo reconstructionInfoTemp]; 
    
    fileID = fopen([dir '\' filePrefix '.txt'],'w');
    fprintf(fileID, reconstructInfo);
    fclose(fileID);
    
    save([dir '\' filePrefix '.mat'], 'reconstructions', 'errors');
end
save([dir '\_chirpErrorsTotal.mat'], 'chirpErrorsTotal');

% Reconstruct actual spectrograms
phoneErrorsTotal = cell([numel(ps) 1]);
for i=1:numel(ps)
    p = ps(i);
    phone = phonemes{p};
    
    fprintf(['================\nPhone ' num2str(phone) '\n']);
    reconstructInfo = ['Phone: ' num2str(phone) ' - ' info.phones{phone}];
    reconstructInfo = [reconstructInfo '\r\n' 'Phone (folded): ' num2str(info.phoneGroups(phone)) ' - ' info.phones{info.phoneGroups(phone)}];
    
    filePrefix = ['p' num2str(p)];
    
    % Read spectrogram from file.
    input = readSpectrogram([inputPath '\test\' phonemeFileNames{p}], height, width, depth);
    
    % Prepare input for reconstruction.
    input = input - average;
    % input = gpuArray(input);
    
    % Generate network prediction.
    predictions = utils_getPredictions(net, input, phonemes{p});
    if info.phoneGroups(predictions(1)) == info.phoneGroups(phonemes{p})
        fprintf('Network predicts correctly.\n');
        reconstructInfo = [reconstructInfo '\r\nNetwork predicts correctly.'];
    else
        fprintf('Network predicts erroneously.\n');
        reconstructInfo = [reconstructInfo '\r\nNetwork predicts erroneously.'];

        if sum(info.phoneGroups(predictions(1:5)) == info.phoneGroups(phonemes{p})) > 0
            fprintf('Correct prediction is in top 5 predictions.\n');
            reconstructInfo = [reconstructInfo '\r\nCorrect prediction is in top 5 predictions.'];
        end
    end
    
    [reconstructions, reconstructionInfoTemp, errors] = reconstruct(net, input, t, f, average, layers, ...
        averaging, dir, filePrefix, phonemes{p});
    phoneErrorsTotal{i} = errors;
    
    reconstructInfo = [reconstructInfo reconstructionInfoTemp];
    
    fileID = fopen([dir '\' filePrefix '.txt'],'w');
    fprintf(fileID, reconstructInfo);
    fclose(fileID);
    
    save([dir '\' filePrefix '.mat'], 'reconstructions', 'errors');
end
save([dir '\_phoneErrorsTotal.mat'], 'phoneErrorsTotal');