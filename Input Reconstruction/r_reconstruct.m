function [reconstructions, reconstructInfo, errors] = r_reconstruct(type, nets, input, wave, t, f, ...
    average, layers, averaging, dir, filePrefix, class)
%% r_reconstruct
% Author: Thomas Churchman
% 
% Reconstruct a spectrogram with a given network.
%
% Input:
% - type: The type of reconstruction to perform. One of:
%   {'backPropagation', 'simulatedAnnealing', 'decoding', 
%    'decodingBackPropagation'};
% - nets: The various networks to perform reconstruction for. This should
%   be a map containing networks for the keys:
%   - normalNetwork: if type is backPropagation, simulatedAnnealing or 
%      decodingBackPropagation
%   - decoderNetworkLayer%n: if type is decoding or
%     decodingBackPropagation, this network is required for each layer %n 
%     in 'layers'.
% - input: The input to reconstruct
% - wave: The original waveform of the input
% - t: The time-point label vector
% - f: The frequency-bin label vector
% - average: The average spectrogram
% - layers: The layers to perform reconstruction for
% - averaging: The number of different reconstructions to make per layer. 
%   The average reconstructions will be returned.
% - dir: The directory in which to save reconstructions and information.
% - filePrefix: The file prefix to use when saving spectrograms and
%   information.
% - class: Necessary when reconstructing from softmax layers. In general
%   this can just be set to some numerical value, such as '1'.
%
% Output:
% - reconstructions: The average reconstructions made per layer
% - reconstructInfo: The verbose (and verbal) reconstruction information
% - errors: A structucture of reconstruction error information per layer

    reconstructions = cell([length(layers) 1]);
    errors = cell([length(layers) 1]);
    
    reconstructInfo = [];
    for j=1:numel(layers)
        layer = layers(j);

        % Get the network to be used for the reconstruction from this layer
        if strcmp(type, 'backPropagation')
            net = nets('normalNetwork').net;
        elseif strcmp(type, 'simulatedAnnealing')
            net = nets('normalNetwork').net;
        elseif strcmp(type, 'decoding')
            net = nets(['decoderNetworkLayer' num2str(layer)]).net;
        elseif strcmp(type, 'decodingBackPropagation')
            net = nets(['decoderNetworkLayer' num2str(layer)]).net;
        end
        
        fprintf(['----------------\nLayer ' num2str(layer) '\n']);
        reconstructInfo = [reconstructInfo '\r\n----------------\r\nLayer ' num2str(layer)];
    
        % Plot input
        figureHandle = figure(1);
        subplot(2,1,1);
        plotSpectrogram(input(:,:,1) + average, t, f, 'abs');
        title('Input Spectrogram');
        
        errors{j}.layer = layer;
        errors{j}.errors = [];
        errors{j}.xcorrs = [];
        errors{j}.errorOfAverage = [];
        errors{j}.xcorrOfAverage = [];
        errors{j}.waveformCorrelation = [];
        
        % Reconstruct the input.
        averageReconstruction = zeros(size(input(:,:,1)), 'single');
        for k=1:averaging
            fprintf(['................\nRound ' num2str(k) '\n']);
            reconstructInfo = [reconstructInfo '\r\n    Round ' num2str(k)];
            
            if strcmp(type, 'simulatedAnnealing')
                % Simulated Annealing
                netCopy = net;
                netCopy.layers = netCopy.layers(1:layer);
                reconstruction = r_simulatedAnnealing(netCopy, input, class);
            end
            
            if strcmp(type, 'backPropagation')
                % Back Propagation
                reconstruction = r_backPropagation(net, input, layer, class);
            end
            
            if strcmp(type, 'decoding')
                % Decoding
                noise = randn(size(input)) * std2(input) * 0.05;
                reconstruction = r_decoding(net, input + noise, layer, class);
            end
            
            if strcmp(type, 'decodingBackPropagation')
                % Decoding followed by back propagation
                % Decoding
                noise = randn(size(input)) * std2(input) * 0.05;
                reconstruction = r_decoding(net, input + noise, layer, class);
                reconstruction = r_backPropagation(nets('normalNetwork').net, input, layer, class, reconstruction);
            end
            reconstruction(~isfinite(reconstruction)) = 0;
            %reconstruction = r_normalize(input, reconstruction);
            
            error = sqrt(sum(sum((reconstruction - input(:,:,1)).^2)));
            corr = max(max(normxcorr2(reconstruction,input)));
            
            errors{j}.errors = [errors{j}.errors error];
            errors{j}.xcorrs = [errors{j}.xcorrs corr];
            
            fprintf('Euclidean error reconstruction: %f\n', error);
            fprintf('Normxcorr2 reconstruction: %f\n', corr);
            
            reconstructInfo = [reconstructInfo '\r\n        Euclidean error reconstruction: ' num2str(error)];
            reconstructInfo = [reconstructInfo '\r\n        Normxcorr2 reconstruction: ' num2str(corr) '\r\n'];
            
            averageReconstruction = averageReconstruction + reconstruction(:,:,1);
        end
        reconstructInfo = [reconstructInfo '\r\n    Average:'];
        averageReconstruction = averageReconstruction / averaging;
        
        reconstructions{j} = averageReconstruction + average;
        
        % Calculate reconstruction errors.
        error = sqrt(sum(sum((averageReconstruction - input(:,:,1)).^2)));
        corr = max(max(normxcorr2(averageReconstruction,input)));
        
        errors{j}.errorOfAverage = error;
        errors{j}.xcorrOfAverage = corr; 
        fprintf('Euclidean error average reconstruction: %f\n', error);
        fprintf('Normxcorr2 average reconstruction: %f\n', corr);
        
        reconstructInfo = [reconstructInfo '\r\n        Euclidean error average reconstruction: ' num2str(error)];
        reconstructInfo = [reconstructInfo '\r\n        Normxcorr2 average reconstruction: ' num2str(corr)];
        
        % Calculate error to average phone spectrogram
        errorToAverage = sqrt(sum(sum((averageReconstruction - average(:,:,1)).^2)));
        errors{j}.errorOfAverageToAverage = errorToAverage;
        fprintf('Euclidean error average reconstruction to average spectrogram: %f\n', errorToAverage);
        
        reconstructInfo = [reconstructInfo '\r\n        Euclidean error average reconstruction to average spectrogram: ' num2str(errorToAverage)];
        
        % Generate waveform from reconstructed spectrogram
        reconstructedWave = spectrogramToWave(reconstructions{j});        
        % Get waveform correlation with original wave
        waveCorrelation = max(xcorr(wave, reconstructedWave, 'coeff'));
        fprintf('Waveform correlation: %f\n', waveCorrelation);
        errors{j}.waveformCorrelation = waveCorrelation;
        reconstructInfo = [reconstructInfo '\r\n        Waveform correlation: ' num2str(waveCorrelation)];
        
        subplot(2,1,2);
        plotSpectrogram(averageReconstruction + average, t, f, 'abs');
        title(['Reconstructed Spectrogram L' num2str(layer)]);
        
        drawnow;
        
        saveas(figureHandle, [dir '\' filePrefix '-' num2str(layer) '.png']);
        save([dir '\' filePrefix '-' num2str(layer) '.mat'], ...
                'averageReconstruction', 'reconstructedWave');
    end
end