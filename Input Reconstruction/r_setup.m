function r_setup()
%% r_setup
% Author: Thomas Churchman
% 
% Set up MATLAB's path for input reconstruction.

    addpath 'Input Reconstruction/Decoding';
    addpath 'Input Reconstruction/Postprocessing';
end