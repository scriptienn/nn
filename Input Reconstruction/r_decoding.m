function reconstruction = decoding(net, spectrogram, layer, class)
%% decoding
% Author: Thomas Churchman
% 
% Puts an input through a network (trained for classification) that then
% had a decoder trained from a certain layer. E.g., if the original
% network had 18 layers, a decoder can be trained from layer 6 (which will
% yield a 6 layered decoder). Then, the 6 decoder layers will attempt to
% decode the activations from the 6th encoder layer to reconstruct the
% input.
% 
% Input:
% - net: The network to reconstruct the input for, including the decoder.
% - spectrogram: The input into the network that is to be reconstructed.
% - layer: The layer to use the activation pattern of for reconstruction.
% - class: The class of the spectrogram (only required for the loss layer).
%
% Output:
% - reconstruction: The reconstructed input.

    result = vl_simplenn(net, spectrogram, [], [], 'disableDropout', true);
    reconstruction = result(end).x;
end
