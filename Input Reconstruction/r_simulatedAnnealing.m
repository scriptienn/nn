function reconstruction = r_simulatedAnnealing(net, spectrogram, class)
%% r_simulatedAnnealing
% Author: Thomas Churchman
%
% Performs input reconstruction through simulated annealing.
%
% Input:
% - net: The network to reconstruct the input for.
% - spectrogram: The input into the network that is to be reconstructed.
% - class: The class of the spectrogram (only required for the loss layer).
%
% Output:
% - reconstruction: The reconstructed input.

    net.layers{end}.class = class;

    target = vl_simplenn(net, spectrogram, [], [], 'disableDropout', true);
    
    s = size(spectrogram);
    s = s(1:2);
    reconstruction = single(zeros(s));
    energy = calculateObjective(reconstruction, net, target, spectrogram(:,:,1));

    bandHeight = 5;
    kMax = 5000;
    for k = 1:kMax
        temp = k/kMax;
        temp = (0.2 * (1 - temp))^2;
        %nRand = sprand(floor(s(1)/4), floor(s(2)/2), 0.016);
        %nRand = 1*nRand - 0.5*spones(nRand);
        %nRand = imresize(full(nRand), s);
        nRand = sprand(bandHeight, s(2), 0.5);
        nRand = 0.1 * full(1*nRand - 0.5*spones(nRand));
        
        if(rand(1) > 0.5)
            nRand = min(nRand, 0);
        else
            nRand = max(nRand, 0);
        end
             
        from = round(rand(1)*(s(1) + (bandHeight - 1) * 2)) + 1 - (bandHeight - 1);
        to = from + bandHeight - 1;
        from = max(from, 1);
        to = min(to, s(1));
        
        neighbour = reconstruction;
        neighbour(from:to, :) = neighbour(from:to, :) + single(nRand(1:to-from+1, :));
        energyNeighbour = calculateObjective(neighbour, net, target, spectrogram(:,:,1));
        
        if(energyNeighbour < energy)
            reconstruction = neighbour;
            energy = energyNeighbour;
        else
            p = exp((energy - energyNeighbour) / temp);
            
            if p > rand(1)
                reconstruction = neighbour;
                energy = energyNeighbour;
            end
        end
    end
    
    %[x,fval,exitFlag,output] = simulannealbnd(@(x) calculateObjective(x, target),reconstruction);
    %reconstruction = rand(size(spectrogram));
    %while true
    %    activations = vl_simplenn(net, reconstruction, [], [], 'disableDropout', true);
    %    
    %end
end

function y = calculateObjective(x, net, target, z)
    % x = createDeltas(x);
    activations = vl_simplenn(net, x, [], [], 'disableDropout', true);
    y = sum(sum(sum((target(end).x - activations(end).x).^2)));
    y = y + 10 * sum(sum(sum(x.^2)));
    %y = sum(sum((x-z).^2))/(201*16);
end