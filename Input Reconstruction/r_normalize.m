function reconstructionNormalized = r_normalize(original, reconstruction)
%% r_normalize
% Author: Thomas Churchman
%
% Normalize a reconstruction to have values in the same  range as the 
% original. This uses information that should, strictly speaking, not be 
% available to the reconstruction process.
%
% Input: 
% - original: The orginal spectrogram
% - reconstruction: The  reconstructed spectrogram
% 
% Output:
% - recosntructionNormalized: The reconstruction with normalized values.

    values = original(:);
    low = prctile(values, 1);
    high = prctile(values, 99);
    range = high-low;

    rValues = reconstruction(:);
    rLow = prctile(rValues, 1);
    rHigh = prctile(rValues, 99);

    rRange = rHigh - rLow;
    reconstructionNormalized = (reconstruction - rLow) / rRange * range + low;
end