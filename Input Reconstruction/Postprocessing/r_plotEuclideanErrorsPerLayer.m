function r_plotEuclideanErrorsPerLayer(spectrogramType)
%% r_plotEuclideanErrorsPerLayer
% Author: Thomas Churchman
%
% Plot the average euclidean error of the reconstructed phone spectrograms
% per layer per reconstruction method.
%
% Input:
% - spectrogramType: Either 'phones' or 'chirps'.

    r = r_info();
    err = zeros(numel(r.errors), numel(r.settings.layers));
    for e = 1:numel(r.errors)
        errors = r.errors{e}.(spectrogramType);
        for i = 1:numel(errors)
            for l = 1:numel(r.settings.layers)
                err(e,l) = err(e,l) + errors{i}{l}.errorOfAverage;
            end
        end
    end
    
    err = err ./ numel(errors);
    
    plot(r.settings.layers, err', 'LineWidth', 2);
    ax = gca;
    ax.XTick = r.settings.layers;
    
    legend(cellfun(@(x) legendTitle(x), r.meta(:,1), 'UniformOutput', false));
    xlabel('Layer');
    ylabel('Euclidean Error');
end

function t = legendTitle(type)
    switch(type)
        case 'backPropagation'
            t = 'Backprop';
        case 'decoding'
            t = 'Decoding';
        case 'decodingBackPropagation';
            t = 'Dec. & Backpr.';
        case 'simulatedAnnealing'
            t = 'Sim. Annealing';
        otherwise
            t = 'unknown';
    end
end