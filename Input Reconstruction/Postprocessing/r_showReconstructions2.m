function r_showReconstructions2(t, f)
%% r_showReconstructions2
% Author: Thomas Churchman
% 
% Show reconstructions in a grid-layout. This layout is different than the
% other function, r_showReconstructions.
%
% Input:
% - t: The time-point label vector
% - f: The frequency-bin label vector

    r = r_info();
    
    type = 2;
    
    % The chirp indices to show reconstructions for:
    %showChirps = [1,4];
    showChirps = [];
    
    % The phone indices to show reconstructions for:
    showPhones = [36,48];
    %showPhones = [];
    
    % The layers to shown reconstructions for: (note, these are indices of
    % the actual layers that were reconstructed for, not the layers
    % themselves. See r.settings.layers).
    layers = [3,5];
    
    h = length(showChirps) + length(showPhones);
    w = numel(layers)+1;
    
    chirps = r.meta{type,2}.chirps;
    phones = r.meta{type,2}.phones;
    
    for i=1:length(showChirps)
        chirp = showChirps(i);
        
        subplot(h,w,1+(i-1)*w);
        if i == 1
            plotSpectrogramNoTicks(chirps{chirp,2}.spectrogram, t, f);
            title('Input');
        else
            plotSpectrogramNoLabelsNoTicks(chirps{chirp,2}.spectrogram, t, f);
        end
        
        fileNames = r.reconstructionFileNames{type,1}.chirps{chirp,1};
        for lyr=1:numel(layers) 
            l = layers(lyr);
            data = load([r_path '\' fileNames{l}]);

            subplot(h,w,lyr+1+(i-1)*w);
            plotSpectrogramNoLabelsNoTicks(data.averageReconstruction, t, f);
            if i == 1
                title(['L' num2str(r.settings.layers(l))]);
            end
        end
    end
    
    for i=1:length(showPhones)
        phone = showPhones(i);
        
        subplot(h,w,1+(length(showChirps)+i-1)*w);
        if i == 1 && length(showChirps) == 0
            plotSpectrogramNoTicks(phones{phone,2}.spectrogram, t, f);
            title('Input');
        else
            plotSpectrogramNoLabelsNoTicks(phones{phone,2}.spectrogram, t, f);
        end
        
        fileNames = r.reconstructionFileNames{type,1}.phones{phone,1};
        for lyr=1:numel(layers) 
            l = layers(lyr);
            data = load([r_path '\' fileNames{l}]);

            subplot(h,w,lyr+1+(length(showChirps)+i-1)*w);
            plotSpectrogramNoLabelsNoTicks(data.averageReconstruction, t, f);
            if i == 1 && length(showChirps) == 0
                title(['L' num2str(r.settings.layers(l))]);
            end
        end
    end
    
end

function t = legendTitle(type)
    switch(type)
        case 'backPropagation'
            t = 'Backprop';
        case 'decoding'
            t = 'Decoding';
        case 'decodingBackPropagation';
            t = 'Dec. and Backpr.';
        case 'simulatedAnnealing'
            t = 'Sim. Annealing';
        otherwise
            t = 'unknown';
    end
end