function r_generateAndPlotSpectrogramCorrelationsPerLayer(spectrogramType)
%% r_generateAndPlotSpectrogramCorrelationsPerLayer
% Author: Thomas Churchman
%
% Generate and plot the average max-normxcorr2 errors of the reconstructed 
% phone specrograms per layer per reconstruction method.
%
% Input:
% - spectrogramType: Either 'phones' or 'chirps'.

    r = r_info();
    
    err = zeros(numel(r.errors), numel(r.settings.layers));
    
    for type=1:4
        spectrograms = r.meta{type,2}.(spectrogramType);
        d = size(spectrograms);
        for i=1:d(1)
            original = spectrograms{i,2}.spectrogram;

            fileNames = r.reconstructionFileNames{type,1}.(spectrogramType){i,1};
            for l=1:numel(r.settings.layers) 
                data = load([r_path '\' fileNames{l}]);

                crr = max(xcorr(data.averageReconstruction(:), original(:), 'coeff'));
                
                err(type, l) = err(type, l) + crr;
            end
        end
    end
    
    
    
    err = err ./ d(1);
    
    plot(r.settings.layers, err', 'LineWidth', 2);
    ax = gca;
    ax.XTick = r.settings.layers;
    
    legend(cellfun(@(x) legendTitle(x), r.meta(:,1), 'UniformOutput', false));
    xlabel('Layer');
    ylabel('mxcorr2');
    %ylim([0,1]);
end

function t = legendTitle(type)
    switch(type)
        case 'backPropagation'
            t = 'Backprop';
        case 'decoding'
            t = 'Decoding';
        case 'decodingBackPropagation';
            t = 'Dec. and Backpr.';
        case 'simulatedAnnealing'
            t = 'Sim. Annealing';
        otherwise
            t = 'unknown';
    end
end