function r_plotSpectrogramCorrelationsPerLayer(spectrogramType)
%% r_plotSpectrogramCorrelationsPerLayer
% Author: Thomas Churchman
%
% Plot the average normxcorr2 errors of the reconstructed phone specrograms
% per layer per reconstruction method.
%
% Input:
% - spectrogramType: Either 'phones' or 'chirps'.

    r = r_info();
    err = zeros(numel(r.errors), numel(r.settings.layers));
    for e = 1:numel(r.errors)
        errors = r.errors{e}.(spectrogramType);
        for i = 1:numel(errors)
            for l = 1:numel(r.settings.layers)
                err(e,l) = err(e,l) + errors{i}{l}.xcorrOfAverage;
            end
        end
    end
    
    err = err ./ numel(errors);
    
    plot(r.settings.layers, err', 'LineWidth', 2);
    ax = gca;
    ax.XTick = r.settings.layers;
    
    legend(cellfun(@(x) legendTitle(x), r.meta(:,1), 'UniformOutput', false));
    xlabel('Layer');
    ylabel('Spectrogram xcorr');
    ylim([0,1]);
end

function t = legendTitle(type)
    switch(type)
        case 'backPropagation'
            t = 'Backprop';
        case 'decoding'
            t = 'Decoding';
        case 'decodingBackPropagation';
            t = 'Dec. & Backpr.';
        case 'simulatedAnnealing'
            t = 'Sim. Annealing';
        otherwise
            t = 'unknown';
    end
end