function r_showReconstructions(p, t, f)
%% r_showReconstructions
% Author: Thomas Churchman
% 
% Shows reconstructions in a grid layout for a phone with the given 
% phone ID. If the phone ID is not in the reconstructions, no plot is made.
% 
% Inputs:
% - p: The phone ID to show the intensity for
% - t: The time-point label vector
% - f: The frequency-bin label vector

    r = r_info();
    
    h = 4;
    w = numel(r.settings.layers)+1;
    
    for type=1:4
        phones = r.meta{type,2}.phones;
        d = size(phones);
        for i=1:d(1)
            if phones{i,1}.phoneIdx == p
                subplot(h,w,1+(type-1)*w);
                
                if type == 1
                    plotSpectrogramNoTicks(phones{i,2}.spectrogram, t, f);
                else
                    plotSpectrogramNoLabelsNoTicks(phones{i,2}.spectrogram, t, f);
                end
                title(legendTitle(r.meta{type,1}));
                

                fileNames = r.reconstructionFileNames{type,1}.phones{i,1};
                for l=1:numel(r.settings.layers) 
                    data = load([r_path '\' fileNames{l}]);

                    subplot(h,w,l+1+(type-1)*w);
                    
                    plotSpectrogramNoLabelsNoTicks(data.averageReconstruction, t, f);
                    if type == 1
                        title(['L' num2str(r.settings.layers(l))]);
                    end
                end
                break;
            end
        end
    end
end

function t = legendTitle(type)
    switch(type)
        case 'backPropagation'
            t = 'Backprop';
        case 'decoding'
            t = 'Decoding';
        case 'decodingBackPropagation';
            t = 'Dec. and Backpr.';
        case 'simulatedAnnealing'
            t = 'Sim. Annealing';
        otherwise
            t = 'unknown';
    end
end