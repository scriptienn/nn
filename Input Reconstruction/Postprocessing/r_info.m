function r = r_info
    %% r_info
    % Load the reconstruction information.
    r = load([r_path '\_experiments.mat']);
end