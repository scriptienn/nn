function r_averageDistanceToSilence
%% r_averageDistanceToSilence
% Author: Thomas Churchman
%
% Calculate the average euclidean distance of phone spectrograms to
% silence.

    r = r_info();
    
    phones = r.meta{1,2}.phones;
    d = size(phones);
    distance = 0;
    for p = 1:d(1)
        phone = phones{p,2};
        for p2 = 1:d(1)
            if p2 == p
                continue
            end
            phone2 = phones{p2,2};
            distance = distance + sqrt(sum(sum(sum((phone.spectrogram - phone2.spectrogram).^2))));
        end
    end
    
    distance = distance / (d(1)^2 - d(1))
end

function t = legendTitle(type)
    switch(type)
        case 'backPropagation'
            t = 'Backprop';
        case 'decoding'
            t = 'Decoding';
        case 'decodingBackPropagation';
            t = 'Dec. & Backpr.';
        case 'simulatedAnnealing'
            t = 'Sim. Annealing';
        otherwise
            t = 'unknown';
    end
end