function path = r_path()
%% r_path
% Author: Thomas Churchman
%
% Returns the path where reconstructions are stored.

   %path = 'D:\Uni2\Scriptie\reconstructionOutput\2015-07-03 064501 - normalized';
   path = 'D:\Uni2\Scriptie\reconstructionOutput\2015-07-02 030104 - combined';
end