function r_showIntensities(p)
%% r_showIntensities
% Author: Thomas Churchman
%
% Show spectrogram intensities for a phone with the given phone ID. If the 
% phone ID is not in the reconstructions, no plot is made.
%
% Inputs:
% - p: The phone ID to show the intensity for

    r = r_info();
    types = [1 3];
    h = length(types);
    w = numel(r.settings.layers)+1;
    bins = 27;
    xaxis = [-0.1 0.1];
    yaxis = [0 2700];
    for t=1:length(types)
        type = types(t);
        phones = r.meta{t,2}.phones;
        d = size(phones);
        for i=1:d(1)
            if phones{i,1}.phoneIdx == p
                subplot(h,w,1+(t-1)*w);
                
                hist = histogram(phones{i,2}.spectrogram, [xaxis(1) : (xaxis(2)-xaxis(1))/bins : xaxis(2)]);
                ylim(yaxis);
                if t == 1
                    xlabel('Intensity');
                    ylabel('Occurences');
                else
                    
                end
                title(legendTitle(r.meta{type,1}));
                

                fileNames = r.reconstructionFileNames{t,1}.phones{i,1};
                for l=1:numel(r.settings.layers) 
                    data = load([r_path '\' fileNames{l}]);

                    subplot(h,w,l+1+(t-1)*w);
                    %plotSpectrogramNoLabelsNoTicks(data.averageReconstruction, t, f);
                    %histogram(data.averageReconstruction);
                    %set(gca,'xlim',[-0.02 0.02]);
                    
                    
                    
                    
                    values = phones{i,2}.spectrogram(:);
                    low = prctile(values, 1);
                    high = prctile(values, 99);
                    %low = min(values);
                    %high = max(values);
                    range = high-low;

                    rValues = data.averageReconstruction(:);
                    rLow = prctile(rValues, 1);
                    rHigh = prctile(rValues, 99);
                    %rLow = min(rValues);
                    %rHigh = max(rValues);
                    rRange = rHigh - rLow;
                    reconstructionNormalized = (data.averageReconstruction - rLow) / rRange * range + low;
                    
                    
                    
                    hist = histogram(data.averageReconstruction, [xaxis(1) : (xaxis(2)-xaxis(1))/bins : xaxis(2)]);
                    %hist = histogram(reconstructionNormalized, [xaxis(1) : (xaxis(2)-xaxis(1))/bins : xaxis(2)]);
                    ylim(yaxis);
                    if t == 1
                        title(['L' num2str(r.settings.layers(l))]);
                    end
                end
                break;
            end
        end
    end
end

function t = legendTitle(type)
    switch(type)
        case 'backPropagation'
            t = 'Backprop';
        case 'decoding'
            t = 'Decoding';
        case 'decodingBackPropagation';
            t = 'Dec. and Backpr.';
        case 'simulatedAnnealing'
            t = 'Sim. Annealing';
        otherwise
            t = 'unknown';
    end
end