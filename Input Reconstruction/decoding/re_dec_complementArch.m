function net = re_dec_complementArch(net, toLayer, fromTranspose)
%% re_dec_complementArch
% Author: Thomas Churchman
%
% Creates a new network architecture from an encoder such that the new
% network is the old network with a corresponding decoder at the end.
%
% Input: 
% - net: The (encoding) network to generate an "autocoder" for. See the
%   output for more information on the actual network generated.
% - toLayer: The layer in the encoder after which the decoder has to be
%   placed. The decoder is then meant to decode the activations of that
%   layer back to the input that created those activations.
% - fromTranspose (optional): If set to true, convt filters will be 
%   initialised from their conv filter counterparts through taking the 
%   transpose of those filters.
%
% Output:
% - net: The "autocoder" generated from the network. Unlike an actual
%   autocoder the generated network starts from (part of) a trained encoder
%   (i.e., the network passed into the function) and only the decoder part
%   will be trained further.

    if ~exist('fromTranspose', 'var')
        fromTranspose = false;
    end

    layers = net.layers;
    layers(toLayer+1:end) = [];
    
    numLayers = length(layers);
    
    for i = numLayers:-1:1
        layer = layers{i};
        
        if strcmp(layer.type, 'conv')
            layers{i}.learnFilters = 0;
            layers{i}.learnBiases  = 0;
            
            if fromTranspose
                layers{end+1} = struct('type', 'convt', ...
                                    'learnFilters', 1, ...
                                    'learnBiases', 1, ...
                                    'filters', dec_cnn_filtertranspose(layers{i}.filters));
            else
                layers{end+1} = struct('type', 'convt', ...
                                    'learnFilters', 1, ...
                                    'learnBiases', 1);
            end
            
        elseif strcmp(layer.type, 'pool')
            layers{end+1} = struct('type', 'unpool');
        elseif strcmp(layer.type, 'relu')
            layers{end+1} = struct('type', 'relu');
        elseif strcmp(layer.type, 'dropout')
            layers(i) = [];
        else
            error('Unrecognized layer type.');
        end
    end
    
    % Complete the architecture.
    layers = dec_cnn_autocompletearch(layers);
    
    net.layers = layers;
end