function info = plotReconstruction( net, bracketText, spectrogramNumber )
%PLOTRECONSTRUCTION Summary of this function goes here
%   Detailed explanation goes here

if ~exist('spectrogramNumber', 'var')
    spectrogramNumber = ceil(126691*rand);
end
if ~exist('bracketText', 'var') || strcmp(bracketText, '')
        bracketText = '';
else
        bracketText = strcat(' (', bracketText, ')');
end

[~, outputPath, ~] = setPaths();
load(strcat(outputPath, '\train\_main.mat'), 'f', 't', 'average');

sprintfFormat = '%06d';
phoneFilename = strcat(outputPath, '\train\' , sprintf(sprintfFormat, spectrogramNumber), '_phoneme.bin');
spectrogram = readSpectrogram(phoneFilename, 201, 16, 1);

if exist('net', 'var')
    subplot(2, 1, 1);
else
    subplot(1, 1, 1);
end
plotSpectrogram(spectrogram, t, f, 'abs');
title(sprintf('Phone #%d - original spectrogram', spectrogramNumber));

if exist('net', 'var')
    subplot(2, 1, 2);
    res = vl_simplenn(net, spectrogram-average);
    reconstruction = res(end).x+average;

    plotSpectrogram(reconstruction, t, f, 'abs');
    title(strcat(sprintf('Reconstruction for phone #%d', ...
          spectrogramNumber), bracketText));

    error = sqrt(sum(sum(sum((reconstruction - spectrogram) .^ 2, 1), 2), 3));
    info.error = error;
    info.reconstruction = reconstruction;
    info.number = spectrogramNumber;
    info.spectrogram = spectrogram;
end

end

