function info = plotReconstructionFor(netname, netEpoch, bracketText, spectrogramNumber)
if ~exist('netEpoch', 'var') || netEpoch == 0
    modelPath = @(ep) fullfile('exp', netname, sprintf('net-epoch-%d.mat', ep));
    epoch = 0;
    while exist(modelPath(epoch+1), 'file')
        epoch = epoch + 1;
    end
    if epoch == 0
        error('Net name not found');
    end
    load(modelPath(epoch), 'net');
end

if ~exist('bracketText', 'var')
    info = plotReconstruction(net);
elseif ~exist('spectrogramNumber', 'var')
    info = plotReconstruction(net, bracketText);
else
    info = plotReconstruction(net, bracketText, spectrogramNumber);
end

info.netname = netname;

end