function plotGammatonogram(gammatonogram, t, f)
%% plotGammatonogram
% Author: Thomas Churchman
% 
% "Plot" a gammatonogram. Sets axes labels, values and y-axis orientation.
%
% Input:
% - gammatonogram: The gammatonogram to plot
% - t: The time-point label vector
% - f: The frequency-bin label vector

    colormap('jet');
    imagesc(t, f, gammatonogram(:,:,1));
    set(gca,'YDir', 'normal');
    ylabel('Frequency (Hz)');
    xlabel('Time (s)');
end