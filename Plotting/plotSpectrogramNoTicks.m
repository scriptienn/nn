function plotSpectrogramNoTicks(spectrogram, t, f, type)
%% plotSpectrogramNoLabels
% Author: Thomas Churchman
% 
% "Plot" a spectrogram. Sets values and y-axis orientation.
%
% Input:
% - spectrogram: The spectrogram to plot
% - t: The time-point label vector
% - f: The frequency-bin label vector
% - type: The type of spectrogram to plot. Either 'abs', or 'real'.

    if nargin < 4 
        type = 'abs';
    end
    
    colormap('jet');
    if(strcmp(type, 'abs'))
        imagesc(t, f, abs(spectrogram(:,:,1)));
    else
        img = spectrogram(:,:,1);
        img = img - min(img(:));
        
        sorted = unique(img(:));
        bottom = sorted(ceil(numel(sorted)*0.02));
        top = sorted(floor(numel(sorted)*0.98));
        
        imagesc(t, f, img);
        caxis([bottom,sorted(end)]);
    end
        
    set(gca,'YDir', 'normal');
    ylabel('Frequency (Hz)');
    xlabel('Time (s)');
    set(gca,'Xtick',[],'Ytick',[]);
end