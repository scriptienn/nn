function [Y] = forwardPass( spectrum, nrOfLayers, net)
%forwardPass Feeds in the spectrum into the net up to the given nr of
%layers
%   Y = struct with fields: dimensions, activation, switches

%keep track of sizes, activations and switches
Y = struct('dimensions', cell(1, nrOfLayers+1), 'activation', cell(1, nrOfLayers+1), 'switches', cell(1, nrOfLayers+1));
Y(1).activation = spectrum;
Y(1).dimensions = size(Y(1).activation);

%forward pass: generate activations. F = activations, G = switches.
for layerIndex = 1 : nrOfLayers %do not need to generate activations of last layer
    
    switch net.layers{layerIndex}.type
        
        case 'conv'
            
            Y(layerIndex + 1).activation = vl_nnconv(Y(layerIndex).activation, net.layers{layerIndex}.filters, net.layers{layerIndex}.biases, 'pad', net.layers{layerIndex}.pad, 'stride', net.layers{layerIndex}.stride);
            Y(layerIndex + 1).dimensions = size(Y(layerIndex + 1).activation);
            
        case 'relu'
            
            Y(layerIndex + 1).activation = vl_nnrelu(Y(layerIndex).activation);
            Y(layerIndex + 1).dimensions = size(Y(layerIndex + 1).activation);
            
        case 'normalize'
            
            Y(layerIndex + 1).activation = vl_nnnormalize(Y(layerIndex).activation, net.layers{layerIndex}.param);
            Y(layerIndex + 1).dimensions = size(Y(layerIndex + 1).activation);
            
        case 'pool'
            %only switches when pool layer
            [Y(layerIndex + 1).activation, Y(layerIndex + 1).switches] = deconvnet_pool(Y(layerIndex).activation, net.layers{layerIndex}.method, net.layers{layerIndex}.pad, net.layers{layerIndex}.pool, net.layers{layerIndex}.stride);
            Y(layerIndex + 1).dimensions = size(Y(layerIndex + 1).activation);
            
        case 'dropout'
            Y(layerIndex + 1).activation = Y(layerIndex).activation;
            Y(layerIndex + 1).dimensions = Y(layerIndex).dimensions;
            
        otherwise
            
            error('%s is not supported', net.layers{layerIndex}.type);
            
    end
    
end

end

