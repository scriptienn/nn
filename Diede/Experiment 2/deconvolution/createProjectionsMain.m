function [] = createProjectionsMain()
%CREATEPROJECTIONSMAIN
% Applies deconvolution for each feature, for each 3 highest input phones
% Save projection back to input space in separate .mat files
%

%start matConvnet
MatConvNet_path = 'C:\Users\Diede\Documents\MATLAB\matconvnet-win';
run([MatConvNet_path '/matlab/vl_setupnn']);

net = getNet();

%load averagePhones
phoneLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\';
filename = 'equalRangeAveragePhones.mat';
data = load(strcat(phoneLocation, filename), 'averagePhones');
averagePhones = data.averagePhones;

convLayers = [1,4,7];

for layer = 1:3
    
    disp(['layer' num2str(layer)]);
    
    %load top5 phones for the features of this layer
    location = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\top5phonesPerFeatureWithLocations\';
    filename = strcat('layer', num2str(layer), 'top5phonesPerFeature.mat'); 
    data = load(strcat(location,filename),'phoneLabelMatrix');
    top5phones = data.phoneLabelMatrix;
    
    layerIndex = convLayers(layer);
    createProjections(net, averagePhones, top5phones, layerIndex);
end
end

