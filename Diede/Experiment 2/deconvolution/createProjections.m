function [] = createProjections(net, averagePhones, top5phones, layerIndex)
%CREATEPROJECTIONS
% Applies deconvolution for each feature, for each 3 highest input phones
% averagePhones = cell array with in the second column all average phone spectra
% top5phones = nrOfFeatures x 1 cell array. Each cell contains a 5x4 matrix
% with for each top 5 phone:
% - the index of the phone
% - the max value of the feature for this phone
% - the x coordinate of this max value
% - the y coordinate ""

saveLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\top3projections\';
set = 'test';

[nrOfFeatures,~] = size(top5phones);
%walk through features within layer
for featureIndex = 1:nrOfFeatures
    
    %walk through top 3
    for j = 1:3
        
        %get input spectrum
        infoMatrix = top5phones{featureIndex,1};
        phoneIndex = infoMatrix(j,1);
        
        locationx = infoMatrix(j,3);
        locationy = infoMatrix(j,4);
        
        spectrum = averagePhones{phoneIndex,2};
        
        projection = applyDeconvnet2(net, layerIndex, featureIndex, spectrum, locationx, locationy);
        
        %save result in .mat file
        filename = strcat(saveLocation, ...
            '\l', num2str(layerIndex), ...
            '_f', num2str(featureIndex), ...
            '_top', num2str(j));
        save(filename, 'projection', 'phoneIndex');
        
    end
end
end

