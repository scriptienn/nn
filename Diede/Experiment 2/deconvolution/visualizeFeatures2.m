function [] = visualizeFeatures2()

%set all paths
dataPath = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\top3projections';
figuresPath = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\figures\top3projections';

%load f and t to be able to plot spectrograms
data = load('C:\Users\Diede\Documents\MATLAB\nn\FandT.mat', 'f', 't');
f = data.f;
t = data.t;

%load average phones
file = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\equalRangeAveragePhones.mat';
data = load(file,'averagePhones');
averagePhones = data.averagePhones;

%take indices of relevant layers
layerIndices = [1,4,7];
featureNumbers = [8, 16, 32];

%walk through all layers
for i = 1:3
    layer = layerIndices(i);
    nrOfFeatures = featureNumbers(i);
    
    %walk through all features within the layer
    for feature = 1:nrOfFeatures
        
        disp(strcat('layer: ', num2str(layer), ...
            ', feature: ', num2str(feature)));
        
        fileName = strcat(dataPath, ...
            '\l', num2str(layer), ...
            '_f', num2str(feature), ...
            '_top1.mat');
        
        %check whether first file exist for this feature
        if(exist(fileName,'file'))
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %       STEP 1: projections
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            figure('visible', 'off');
            colormap('jet');
            
            phoneIndices = zeros(1,3);
            
            %walk through the topN projections
            for topN=1:3
                
                %file containing projection
                file = strcat(dataPath, ...
                    '\l', num2str(layer), ...
                    '_f', num2str(feature), ...
                    '_top', num2str(topN), ...
                    '.mat');
                
                %check whether the file exist
                if(exist(file,'file'))
                    
                    %load projection
                    data = load(file, 'projection', 'phoneIndex');
                    projection = data.projection;
                    phoneIndices(topN) = data.phoneIndex;
                    
                    projection = setRangeTo(projection,1);
                    
                    %plot projection
                    subplot(2,3,topN);
                    plotSpectrogramReluWithoutColorbar2(projection, t, f);
                    title(strcat('Top ', num2str(topN), '.'));
                    
                end
            end
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %       STEP 2: Corresponding inputs
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            %walk through the topN corresponding inputs
            for topN=1:3
                
                phoneIndex = phoneIndices(topN);
                
                if(not(phoneIndex == 0))
                    
                    %load average phone spectrogram
                    spectrum = averagePhones{phoneIndex,2};
                    
                    %load corresponding phone label
                    phonemeLabel = indexToPhone(phoneIndex);
                    
                    %plot spectrogram
                    subplot(2,3,(3+topN));
                    plotSpectrogramReluWithoutColorbar2(spectrum,t,f);
                    title(strcat('Top ', num2str(topN), '. "', ...
                        phonemeLabel, '"'));
                    
                    if(topN == 1)
                        ylabel('Frequency (Hz)');
                        xlabel('Time (s)');
                        bar = colorbar('Position', [0.92 0.12 0.025 0.80]);
                        set(get(bar,'ylabel'),'String', 'Intensity');
                    end
                end
            end
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %       STEP 3: save figures
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                       
            %save all plots within one figure
            filename = strcat(figuresPath, ...
                '\L', num2str(layer), ...
                '_F', num2str(feature), ...
                '_projections');
            saveas(gcf, filename, 'png');
            
        end
    end
end