There are different ways in which I have tried to classify to which
phones a feature is mainly sensitive.

1. per phone, sort the features on their activation and give them all a rank.
Then, select per feature only those phones for which the feature was in the
TopN ranking.
--> the same feature(s) were in the top3 of all phones.


2. Per phone, sort the features on their activation and give them all a rank.
Then, select per feature only those phones for which the feature had its highest
rank.
--> some features were close to bottom rank for all phones.


3. Per feature, decide the max activity per phone. Then, select per feature
the topN phones for which the feature had its most activity.
--> these results are used in my thesis.