%
% Main
%
%   get the phones for which the features were in the topN ranking
%

topN = 2;
filelocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\activatedFeatures.mat';
data = load(filelocation, 'activatedFeatures');
activatedFeatures = data.activatedFeatures;

phoneLabelMatrix = getPhoneLabelsPerFeature1(3, activatedFeatures, topN);