function [ activatedFeatures] = selectActivatedFeatures(net, averagePhones)
%selectActivatedFeatures selects the activated features
%   net = the network from which the features are selected
%   averagePhones = a cell array of averagePhones, for each the topN
%   activated features should be selected
%   topNLayer1 = the topN for the first convolutional layer
%   topNLayer2 = the topN for the second ""
%   topNLayer3 = "" third ""

%   output:
%activatedFeatures = cell array, 48x3, nrOfPhones x nrOfConvolutionalLayers.
%Per cell it contains a matrix with the indices of the features that were
%most activated by the phone for that layer, together with the x and y
%coordinate of this maximal activity.
%E.g.:          5   1   1       feature 5, location (1,1)
%               7   5   16      feature 7, location (5,16)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%initialize variables
activatedFeatures = cell(48,3);

%for every average phone
nrOfPhones = length(averagePhones);
for phoneIndex=1:nrOfPhones
    
    spectrum = averagePhones{phoneIndex,2};
    convIndices = cell2mat(getConvLayerIndices(net));
    convIndices = convIndices(1:3,:);
    nrOfLayers = max(convIndices(:,1));
    
    %apply forward propagation up to the last convolutional layer
    Y = forwardPass(spectrum,nrOfLayers,net);
    
    [nrOfConvLayers,~] = size(convIndices);
    for convIndex=1:nrOfConvLayers %walk through convolutional layers
        
        layer = convIndices(convIndex,1);
        
        activationMatrix = Y(layer+1).activation;
        maxima = getMaxima(activationMatrix); %maxima = Fx3 cell array
        maxima = sortMaxima(maxima);
        
        %save
        activatedFeatures{phoneIndex,convIndex} = maxima;
    end
end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [maxima] = getMaxima(activationMatrix)
%--------------------------------------------------------------------------
%GETMAXIMA returns the maximum values of activationMatrix and their
%coordinates
%
%   input:
%   - activationMatrix = a 3D matrix with dimensions HxWxD
%
%   output:
%   - maxima = a Dx3 matrix. For every HxW submatrix of the
%   activationMatrix, it contains the maximum value, its x-coordinate
%   and its y-coordinate

[~, ~, nrOfFeatures] = size(activationMatrix);
maxima = zeros(nrOfFeatures, 3);

for i=1:nrOfFeatures
    
    feature = activationMatrix(:,:,i);          %feature = HxW submatrix
    [subMax, subIndex] = max(feature,[],1);
    [maxValue, rowIndex] = max(subMax,[],2);
    columnIndex = subIndex(rowIndex);
    
    maxima(i,1) = maxValue;     %save maximum value
    maxima(i,2) = columnIndex;  %and its location
    maxima(i,3) = rowIndex;
    
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [sortedMaxima] = sortMaxima(maxima)
%--------------------------------------------------------------------------
%SORTMAXIMA sort the maxima
%
%   input:
%   - maxima = a Fx3 matrix. For every feature F it contains the
%   maximum value, its x-coordinate and its y-coordinate.
%
%   output:
%   - sortedMaxima = a Fx4 matrix. The features are sorted on their maximum
%   values, from high to low. For every feature it contains: the index, the
%   maximum value, its x-coordinate and its y-coordinate.

[nrOfFeatures,~] = size(maxima);
indices = (1:nrOfFeatures)';

sortedMaxima = [indices maxima];
sortedMaxima = sortrows(sortedMaxima, -2);

end


