function [ phoneLabelMatrix ] = getPhoneLabelsPerFeature3( layerIndex, activatedFeatures, topN )
%SHOWPHONELABELSPERFEATURE Gets for each feature in the given layer, the topN
%phoneLabels for which the feature had the highest activity
%   activatedFeatures = nrOfPhones x nrOfLayers cell array. Per cell it
%   contains a nrOfFeatures x 4 double matrix, with the columns:
%       - feature index
%       - max value
%       - x coordinate
%       - y coordinate                      
%   It is sorted on max value.

% phoneLabelMatrix = nrOfFeatures x 1 cell. Per cell it contains a
% cell array of strings, containing the phone labels this feature reacts
% heavily to.

%initialize
[nrOfPhones,~] = size(activatedFeatures);
tempMatrix = activatedFeatures{1,layerIndex};
[nrOfFeatures, ~] = size(tempMatrix);
phoneLabelMatrix = cell(nrOfFeatures,1);

for featureIndex = 1:nrOfFeatures

    maxMatrix = zeros(nrOfPhones,3); %contains for each phone the score of the current feature
    
    for phoneIndex = 1:nrOfPhones
    
        %save featureScore for this phone
        matrix = activatedFeatures{phoneIndex,layerIndex};
        maxIndex = find(matrix(:,1) == featureIndex);
        maxMatrix(phoneIndex,1) = matrix(maxIndex,2); %save activity
        maxMatrix(phoneIndex,2) = matrix(maxIndex,3); %save x coordinate
        maxMatrix(phoneIndex,3) = matrix(maxIndex,4); %save y coordinate
      
    end
    
    %sort activities for this feature from high to low
    indices = (1:nrOfPhones)';
    maxMatrix = [indices maxMatrix];
    maxMatrix = sortrows(maxMatrix,-2);
    
    %select only those phones with the topN highest activities
    phoneIndices = maxMatrix(1:topN,:);
   
    %save it in phoneLabelMatrix
    phoneLabelMatrix{featureIndex,1} = phoneIndices;

end

end
