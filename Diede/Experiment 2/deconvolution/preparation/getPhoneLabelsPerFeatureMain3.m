%
% Main
%

%
% Save top5 phone indices per feature, along with the max value and its x and y location
%

filelocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\activatedFeatures.mat';
data = load(filelocation, 'activatedFeatures');
activatedFeatures = data.activatedFeatures;

layer = 3;
phoneLabelMatrix = getPhoneLabelsPerFeature3(layer, activatedFeatures, 5);

%saving
location = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\top5phonesPerFeatureWithLocations\';
filename = ['layer' num2str(layer) 'top5phonesPerFeature'];
save(strcat(location,filename),'phoneLabelMatrix');