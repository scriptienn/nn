%
% Main
%
%   Select the phones for each feature for which the feature had its top ranking
%

%initializing
filelocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\activatedFeatures.mat';
data = load(filelocation, 'activatedFeatures');
activatedFeatures = data.activatedFeatures;

layer = 3;

phoneLabelMatrix = getPhoneLabelsPerFeature2(layer, activatedFeatures);

%saving
location = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\highestRankPerFeature\';
filename = ['layer' num2str(layer) 'highestRankPerFeature'];
save(strcat(location,filename),'phoneLabelMatrix');