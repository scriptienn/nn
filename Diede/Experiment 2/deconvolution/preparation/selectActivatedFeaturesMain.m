%
% Select activated features main


%start matConvnet
MatConvNet_path = 'C:\Users\Diede\Documents\MATLAB\matconvnet-win';
run([MatConvNet_path '/matlab/vl_setupnn']);

%load averagePhones
phoneLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\';
filename = 'equalRangeAveragePhones.mat';
data = load(strcat(phoneLocation, filename), 'averagePhones');
averagePhones = data.averagePhones;

%set other variables
net=getNet();
topNLayer1 = 2;
topNLayer2 = 4;
topNLayer3 = 8;

activatedFeatures = selectActivatedFeatures(net, averagePhones);

%save activatedFeatures
location = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part';
filename = strcat(location,'\activatedFeatures.mat');
save(filename, 'activatedFeatures');