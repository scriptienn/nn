function [ phoneLabelMatrix ] = getPhoneLabelsPerFeature2( layerIndex, activatedFeatures)
%SHOWPHONELABELSPERFEATURE Gets for each feature in the given layer, the
%phoneLabels for which the feature had its best position in the classification.
%   For example: for phone 'aa' the feature was 3rd place highest activation feature,
%   for phone 'ay' the feature also was 3rd place and for all other phones it had
%   a lower ranking, then the phones 'aa' and 'ay' are saved for the feature.
%
%   activatedFeatures = nrOfPhones x nrOfLayers cell array. Per cell it
%   contains a nrOfFeatures x 4 double matrix, with the columns:
%       - feature index
%       - max value
%       - x coordinate
%       - y coordinate
%   It is sorted on max value.

% phoneLabelMatrix = nrOfFeatures x 2 cell array. Per cell in the first columns
% it contains a cell array of strings, containing the phone labels this feature reacts
% heavily to. Per cell in the second column it contains the highest rank this feature
% scored.

%initialize
[nrOfPhones,~] = size(activatedFeatures);
tempMatrix = activatedFeatures{1,layerIndex};
[nrOfFeatures, ~] = size(tempMatrix);
phoneLabelMatrix = cell(nrOfFeatures,1);

for featureIndex = 1:nrOfFeatures

    scoreMatrix = zeros(nrOfPhones,1); %contains for each phone the score of the current feature
    
    for phoneIndex = 1:nrOfPhones
    
        %save featureScore for this phone
        matrix = activatedFeatures{phoneIndex,layerIndex};
        scoreMatrix(phoneIndex,1) = getFeatureScore(matrix, featureIndex);
      
    end
    
    %select the phones that this feature is most sensitive to
    %select only those phones where feature scores highest
    maxScore = min(scoreMatrix); %1st place is best!
    phoneIndices = find(scoreMatrix == maxScore);
    
    %create cell array of strings
    phoneLabels = indicesToLabels(phoneIndices);
    
    %save it in phoneLabelMatrix
    phoneLabelMatrix{featureIndex,1} = phoneLabels;
    phoneLabelMatrix{featureIndex,2} = maxScore;
    
end

end

function [featureScore] = getFeatureScore(matrix, featureIndex)
%
%   matrix = nrOfFeatures x 4 matrix (for content see above comments)
%   featureIndex = the index of the feature we're looking at.
%
%   featureScore = the score of the feature within the matrix, with score
%   being measured as index position within the matrix.
%   

featureScore = find(matrix(:,1) == featureIndex);

end