function [labels] = getLabels(set)
%--------------------------------------------------------------------------
%GETLABELS Loads the labels of the phones of the given set
% Input:
%   set = 'train' or 'test'
%
% Output:
%   labels = nrOfPhones x 1 cell array

%error messaging
if(~strcmp(set, 'train') && ~strcmp(set, 'test'))
    error('Expected set to be either train or test');
end

%stuff to make it possible to load the input data
[~, ~, ~, dataPath] = setPaths();

%check train or test set
if(strcmp(set, 'train'))
    metaDataPath = strcat(dataPath, '\train\_main.mat');
else
    metaDataPath = strcat(dataPath, '\test\_main.mat');
end

%load metaData and labels
metaData = load(metaDataPath, 'phonemes');
labels = metaData.phonemes;

end