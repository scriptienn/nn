

%layer = 3;

%get data
dataLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\phoneFrequencies\';
filename = ['totalTop3List.mat'];
data = load(strcat(dataLocation,filename),'totalTop3List');

%create figure
figure('visible', 'off');
plotPhoneFrequency(data.totalTop3List);

%save figure
saveLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\figures\phoneFrequencies\';
filename = ['frequencyOfPhonesInTop3_2'];
saveas(gcf, strcat(saveLocation,filename),'png');