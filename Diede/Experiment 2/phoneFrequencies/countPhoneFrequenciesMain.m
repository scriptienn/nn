%
%   countPhoneFrequencies Main
%

nrOfPhones = 48;
nrOfLayers = 3;
dataLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\top5phonesPerFeatureWithLocations\';
phoneFrequencies = zeros(nrOfPhones,nrOfLayers);

for layer = 1:nrOfLayers
    
    filename = ['layer' num2str(layer) 'top5phonesPerFeature.mat'];
    data = load(strcat(dataLocation,filename), 'phoneLabelMatrix');

    phoneFrequencies(:,layer) = countPhoneFrequencies(data.phoneLabelMatrix);
 
end

%save results
saveLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\';
filename = 'phoneFrequencies';
save(strcat(saveLocation,filename), 'phoneFrequencies');