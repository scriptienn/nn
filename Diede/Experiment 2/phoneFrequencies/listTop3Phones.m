function [ listOfPhones ] = listTop3Phones(phoneLabelMatrix )
%listTop3Phones Lists all the phone top3s in the phoneLabelMatrix
% Goal: create a matrix that can easily be used by the hist() function
%
%   phoneLabelMatrix = nrOfFeatures x 1 cell array, with in each cell a 5x4
%   matrix. For each top5 phone:
% phone index
% max activity
% x coordinate
% y coordinate

nrOfFeatures = length(phoneLabelMatrix);
listOfPhones = zeros(nrOfFeatures*3,1); %for every feature a top 3

for featureIndex = 1:nrOfFeatures
    
    top5matrix = phoneLabelMatrix{featureIndex,1};
    lastIndex = featureIndex*3;
    listOfPhones((lastIndex-2):lastIndex,1) = top5matrix(1:3,1);
end

end


