%
%   listTop3Phones Main
%

nrOfPhones = 48;
nrOfLayers = 3;
layers = [1, 4, 7];
features = [8, 16, 32];
saveLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\phoneFrequencies\';
dataFile = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\maximaPerFeature\Network2_Real_NoDeltas\featuresMaxima.mat';
data = load(dataFile, 'result');

totalTop3List = [];

for layerIndex = 1:nrOfLayers
    
    %get correct row from result matrix
    layer = layers(layerIndex);
    nrOfFeatures = features(layerIndex);
    
    top3List = listTop3PhonesOldResults(data.result, layer, nrOfFeatures);
    
    %save results
    filename = ['oldTop3List_L' num2str(layerIndex)];
    save(strcat(saveLocation,filename), 'top3List');
    
    totalTop3List = [totalTop3List; top3List];
 
end

%save results
filename = 'oldTotalTop3List';
save(strcat(saveLocation,filename), 'totalTop3List');