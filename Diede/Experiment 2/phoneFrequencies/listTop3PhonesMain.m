%
%   listTop3Phones Main
%

nrOfPhones = 48;
nrOfLayers = 3;
saveLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\phoneFrequencies\';
dataLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\top5phonesPerFeatureWithLocations\';
totalTop3List = [];

for layer = 1:nrOfLayers
    
    filename = ['layer' num2str(layer) 'top5phonesPerFeature.mat'];
    data = load(strcat(dataLocation,filename), 'phoneLabelMatrix');

    top3List = listTop3Phones(data.phoneLabelMatrix);
    
    %save results
    filename = ['top3List_L' num2str(layer)];
    save(strcat(saveLocation,filename), 'top3List');
    
    totalTop3List = [totalTop3List; top3List];
 
end

%save results
filename = 'totalTop3List';
save(strcat(saveLocation,filename), 'totalTop3List');