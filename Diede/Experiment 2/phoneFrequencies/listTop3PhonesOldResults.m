function [ listOfPhones ] = listTop3PhonesOldResults(resultMatrix, layer, nrOfFeatures)
%listTop3Phones Lists all the phone top3s in the phoneLabelMatrix
% Goal: create a matrix that can easily be used by the hist() function
%
%   resultMatrix = 18 x 1000 cell array, with in each cell a 9x4 double
%   matrix. For each top5 phone:
% input index from test set
% max activity
% x coordinate
% y coordinate

listOfPhones = zeros(nrOfFeatures*3,1); %for every feature a top 3
listOfIndices = zeros(nrOfFeatures*3,1);

%get input indices
for featureIndex = 1:nrOfFeatures
    
    top9matrix = resultMatrix{layer,featureIndex};
    lastIndex = featureIndex*3;
    listOfIndices((lastIndex-2):lastIndex,1) = top9matrix(1:3,1);
end

disp(['layer' num2str(layer)]);

%turn input indices into phone indices
phoneIndices = getLabels('test');
for index = 1:nrOfFeatures*3
    inputIndex = listOfIndices(index);
    if(inputIndex>0)
        listOfPhones(index) = phoneIndices{listOfIndices(index),1}; 
    end
end
listOfPhones(all(listOfPhones==0,2),:)=[]; %throw away zeros
end


