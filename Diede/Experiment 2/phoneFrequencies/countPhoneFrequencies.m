function [ phoneFrequencies] = countPhoneFrequencies(phoneLabelMatrix)
%COUNTPHONEFREQUENCIES Counts per phone the number of times it was
%mentioned in a top 3 in the phoneLabelMatrix
%   phoneLabelMatrix = nrOfFeatures x 1 cell array, with in each cell a 5x4
%   matrix. For each top5 phone:
% phone index
% max activity
% x coordinate
% y coordinate

nrOfPhones = 48;
nrOfFeatures = length(phoneLabelMatrix);
phoneFrequencies = zeros(nrOfPhones,1);

for phoneIndex = 1:nrOfPhones
    for featureIndex = 1:nrOfFeatures
        
        top5matrix = phoneLabelMatrix{featureIndex,1};
        
        for i = 1:3 %walk through top3
            
            if(top5matrix(i,1) == phoneIndex) %the phoneIndex appears in a top 3
                phoneFrequencies(phoneIndex,1) = phoneFrequencies(phoneIndex,1) + 1;
            end
        end
    end
    
end

end

