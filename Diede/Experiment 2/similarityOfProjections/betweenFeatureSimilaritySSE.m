%
%   computes for every layer its between feature similarity: SSE
%
dataLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\sections\';
SSEs = zeros(3,1);
layers = [1,4,7];

%for each layer
for layer = 1:3
    
    %load sectionsMatrix
    filename = ['L' num2str(layers(layer)) '_allFeatures.mat'];
    data = load(strcat(dataLocation,filename),'sectionsMatrix');
    
    %make range and size equal
    sectionsMatrix = makeRangeAndSizeEqual(data.sectionsMatrix, 1);
    
    [nrOfRows,~] = size(sectionsMatrix);
    subSSEs = zeros(nrOfRows,1);
    
    %walk through the topNs
    for topN = 1:nrOfRows
        
        nrOfFeatures = length(sectionsMatrix);
        sumSSE = 0;
        count = 0;
        for feature = 1:nrOfFeatures
            
            thisMatrix = sectionsMatrix{topN,feature};
            if(not(isempty(thisMatrix)))
                for otherFeature = 1:nrOfFeatures
                    
                    otherMatrix = sectionsMatrix{topN,otherFeature};
                    if(not(feature==otherFeature) && not(isempty(otherMatrix)))
                        %compare all projection combinations
                        sumSSE = sumSSE + sumOfSquaredErrors(thisMatrix,otherMatrix);
                        count = count + 1;
                    end
                end
            end
        end
        
        subSSEs(topN,1) = sumSSE/count;
        
    end
    SSEs(layer,1) = sum(subSSEs)/nrOfRows;
end

%save
saveLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\featureSimilarities';
filename = '\SSEs_betweenFeature_oldResults';
save(strcat(saveLocation,filename),'SSEs');
