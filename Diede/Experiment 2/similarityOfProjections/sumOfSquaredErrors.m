function [ sse] = sumOfSquaredErrors( matrix1, matrix2 )
%SUMOFSQUAREDERRORS 

D = abs(matrix1 - matrix2).^2;
sse = sum(D(:));

end

