%
%   computes for every layer its between feature similarity: corr
%
%experiment 1
%dataLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\sections\';
%layers = [1,4,7];

%experiment 2
dataLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\sections\';

correlations = zeros(3,1);


%for each layer
for layer = 1:3
    
    %load sectionsMatrix, experiment 1
    %filename = ['L' num2str(layers(layer)) '_allFeatures.mat'];
    
    %load sectionsMatrix, experiment 2
    filename = ['sectionsMatrix_L' num2str(layer) '.mat'];
    
    data = load(strcat(dataLocation,filename),'sectionsMatrix');
    
    %make range and size equal
    sectionsMatrix = makeRangeAndSizeEqual(data.sectionsMatrix, 1);
    
    [nrOfRows,~] = size(sectionsMatrix);
    subCorrs = zeros(nrOfRows,1);
    
    %walk through the topNs
    for topN = 1:nrOfRows
        
        nrOfFeatures = length(sectionsMatrix);
        sumCorrs = 0;
        count = 0;
        for feature = 1:nrOfFeatures
            
            thisMatrix = sectionsMatrix{topN,feature};
            if(not(isempty(thisMatrix)))
                for otherFeature = 1:nrOfFeatures
                    
                    otherMatrix = sectionsMatrix{topN,otherFeature};
                    if(not(feature==otherFeature) && not(isempty(otherMatrix)))
                        %compare all projection combinations
                        sumCorrs = sumCorrs + matrixCrossCorrelation(thisMatrix,otherMatrix);
                        count = count + 1;
                    end
                end
            end
        end
        
        subCorrs(topN,1) = sumCorrs/count;
        
    end
    correlations(layer,1) = sum(subCorrs)/nrOfRows;
end

%save
saveLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\featureSimilarities';
filename = '\corr_betweenFeature_newResults';
save(strcat(saveLocation,filename),'correlations');
