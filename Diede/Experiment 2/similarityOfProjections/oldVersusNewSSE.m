%
%   How similar are the same features, projected by two different methods?
%   Saves these similarities

oldLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\sections\';
newLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\sections\';
layers = [1,4,7];

SSEs = zeros(3,1);

for layerIndex = 1:3
    
    disp(['layer' num2str(layerIndex)]);
    
    %load sectionsMatrix for old and for new results
    
    %old
    filename = ['L' num2str(layers(layerIndex)) '_allFeatures.mat'];
    data = load(strcat(oldLocation,filename),'sectionsMatrix');
    oldSections = data.sectionsMatrix;
    
    %new
    filename = ['sectionsMatrix_L' num2str(layerIndex) '.mat'];
    data = load(strcat(newLocation,filename),'sectionsMatrix');
    newSections = data.sectionsMatrix;
    
    %make range and size equal
    oldSections = makeRangeAndSizeEqualTemp(oldSections,1);
    newSections = makeRangeAndSizeEqualTemp(newSections,1);
    
    [~,nrOfFeatures] = size(oldSections);
    
    subError = 0;
    countFeatures = 0;
    
    %for each feature
    for feature = 1:nrOfFeatures
        
        sumError = 0;
        countTops = 0;
        
        for i = 1:3
            
            oldMatrix = oldSections{i,feature};
            for j = 1:3
                
                newMatrix = newSections{j,feature};
                
                if(not(isempty(oldMatrix)) && not(isempty(newMatrix)))
                
                %compare them
                sumError = sumError + sumOfSquaredErrors(oldMatrix,newMatrix);
                countTops = countTops + 1;
                
                end
            end
        end
        
        if(not(sumError == 0))
            subError = subError + sumError/countTops;
            countFeatures = countFeatures + 1;
        end
    end
    
    %calculate layer average
    SSEs(layerIndex,1) = subError/countFeatures;
    
end

%save
saveLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\featureSimilarities\';
filename = 'SSEs_betweenMethods.mat';
save(strcat(saveLocation,filename), 'SSEs');
