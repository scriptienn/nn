%
%   computes for every feature its within feature similarity
%
%experiment 1
%dataLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\sections\';
%layers = [1,4,7]; %data experiment 1

%experiment 2
dataLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\sections\';

correlations = cell(3,1);

%for each layer
for layer = 1:3
    
    %load sectionsMatrix, experiment 1
    %filename = ['L' num2str(layers(layer)) '_allFeatures.mat'];
    
    %load sectionsMatrix, experiment 2
    filename = ['sectionsMatrix_L' num2str(layer) '.mat'];
    
    data = load(strcat(dataLocation,filename),'sectionsMatrix');
    
    %make range and size equal
    sectionsMatrix = makeRangeAndSizeEqual(data.sectionsMatrix, 1);
    
    %walk through the features
    nrOfFeatures = length(sectionsMatrix);
    
    corrMatrix = zeros(nrOfFeatures,3);
    
    for feature = 1:nrOfFeatures
        
        %compare all projection combinations
        matrix1 = sectionsMatrix{1,feature};
        matrix2 = sectionsMatrix{2,feature};
        matrix3 = sectionsMatrix{3,feature};
        
        corrMatrix(feature,1) = matrixCrossCorrelation(matrix1, matrix2);
        corrMatrix(feature,2) = matrixCrossCorrelation(matrix2, matrix3);
        corrMatrix(feature,3) = matrixCrossCorrelation(matrix1, matrix3);
        
    end
    
    correlations{layer,1} = corrMatrix;
end

%save
saveLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\featureSimilarities';
filename = '\corr_withinFeature_newResults';
save(strcat(saveLocation,filename),'correlations');
