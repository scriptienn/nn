%
%   computes for every feature its within feature similarity
%
dataLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\sections\';
SSEs = cell(3,1);
layers = [1,4,7];

%for each layer
for layer = 1:3
    
    %load sectionsMatrix
    filename = ['L' num2str(layers(layer)) '_allFeatures.mat'];
    data = load(strcat(dataLocation,filename),'sectionsMatrix');
    
    %make range and size equal
    sectionsMatrix = makeRangeAndSizeEqual(data.sectionsMatrix, 1);
    
    %walk through the features
    nrOfFeatures = length(sectionsMatrix);
    
    SSEmatrix = zeros(nrOfFeatures,3);
    
    for feature = 1:nrOfFeatures
        
        %compare all projection combinations
        matrix1 = sectionsMatrix{1,feature};
        matrix2 = sectionsMatrix{2,feature};
        matrix3 = sectionsMatrix{3,feature};
        
        SSEmatrix(feature,1) = sumOfSquaredErrors(matrix1, matrix2);
        SSEmatrix(feature,2) = sumOfSquaredErrors(matrix2, matrix3);
        SSEmatrix(feature,3) = sumOfSquaredErrors(matrix1, matrix3);
        
    end
    
    SSEs{layer,1} = SSEmatrix;
end

%save
saveLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\featureSimilarities';
filename = '\SSEs_withinFeature_oldResults';
save(strcat(saveLocation,filename),'SSEs');
