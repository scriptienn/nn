%
% createSections2
%
%   Create sections of the projections of the second experiment. selects only those sections that are
%   activated.

dataLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\top3projections';
layers = [1, 4, 7];

nrOfFeatures = 32;

sectionsMatrix = cell(3,nrOfFeatures);

for i = 3:3
    layer = layers(i);
    
    %walk through all features of the layer
    for feature=1:nrOfFeatures
        
        for topN=1:3
            
            %search for file
            fileName = strcat(dataLocation, ...
                '\l', num2str(layer), ...
                '_f', num2str(feature), ...
                '_top', num2str(topN), ...
                '.mat');
            
            %if it exists
            if(exist(fileName,'file'))
                data = load(fileName, 'projection');
                %load the projection
                projection = data.projection;
                projection(all(projection==0,2),:)=[]; %remove zeros
                %save activated section
                sectionsMatrix{topN,feature} = double(projection);
            end
            
        end
    end
end
%save matrix on computer
resultLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\sections';
filename = strcat(resultLocation, ...
    '\sectionsMatrix_L3');
save(filename, 'sectionsMatrix');