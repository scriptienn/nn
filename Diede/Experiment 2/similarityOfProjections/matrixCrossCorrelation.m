function [ crossCorr ] = matrixCrossCorrelation(matrix1, matrix2 )
%MatrixCrossCorrelation Computes the cross correlation of two matrices of
%equal size.
%Details: per column the crosscorrelation sequence is calculated plus the
%maximum value within this sequence. crossCorr is the average of all these
%values of all columns.

[~, nrOfColumns1] = size(matrix1);
[~, nrOfColumns2] = size(matrix2);
assert(nrOfColumns1 == nrOfColumns2, 'number of columns of both matrices is not equal');

maxCorrs = zeros(nrOfColumns1, 1);

for i = 1:nrOfColumns1
    
    column1 = matrix1(:,i);
    column2 = matrix2(:,i);
    maxCorrs(i,1) = max(xcorr(column1, column2, 'coeff'));
    
end

crossCorr = mean(maxCorrs);

end

