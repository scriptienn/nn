function [ featureIndices] = selectConsonantFeatures( phoneLabelMatrix)
%SELECTCONSONANTFEATURES

%phoneLabelMatrix = nrOfFeatures x 1 cell array, in each cell the top five
%phone labels are given in a 5x1 cell array.
featureIndices = [];
nrOfFeatures = length(phoneLabelMatrix);
for featureIndex = 1:nrOfFeatures
    
    if(isConsonantFeature(phoneLabelMatrix{featureIndex,1}))
        featureIndices = [featureIndices, featureIndex];
    end
end
end

function [yesOrNo] = isConsonantFeature(phoneLabelCell)

%phoneLabelCell = 5x1 cell array with in each cell a phone label

%A feature is a consonants feature when at least 3 out of these five phone labels is
%a consonant label and that there is at most 1 consonant (taking into account
%that there are also phones that are not vowels or consonant, because their score is
%unknown.

vowelCount = 0;
consonantCount = 0;

for i = 1:5
    
    phoneLabel = phoneLabelCell{i,1};
    phoneIndex = phoneToIndex(phoneLabel);
    
    if(isVowel(phoneIndex))
        vowelCount = vowelCount + 1;
    end
    if(isConsonant(phoneIndex))
        consonantCount = consonantCount + 1;
    end
    
end

if(vowelCount<2 && consonantCount>2)
    yesOrNo = true;
else
    yesOrNo = false;
    
end
end