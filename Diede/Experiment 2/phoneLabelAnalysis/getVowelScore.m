function [ score ] = getVowelScore( phoneIndex )
%GETVOWELSCORE
%   Gives the score of the vowel with the index phoneIndex

% If vowel is a back vowel: 7 points
%             a central vowel: 4 points
%             a front vowel: 1 point
%             a diphthong between back and front: 4 points

if(not(isVowel(phoneIndex)))
    disp('Phone is geen vowel, dus mag geen vowelscore krijgen');
    score = -1;
else
    %back vowels
    if(phoneIndex == 1 || ...
            phoneIndex == 4 || ...
            phoneIndex == 5 || ...
            phoneIndex == 32 || ...
            phoneIndex == 41 || ...
            phoneIndex == 42)
        
        score = 7;
        
        %central vowels and diphthongs
    elseif(phoneIndex == 3 || ...
            phoneIndex == 6 || ...
            phoneIndex == 7 || ...
            phoneIndex == 33)
        
        score = 4;
        
        %front vowels
    else
        
        score = 1;
    end
    
end
end

