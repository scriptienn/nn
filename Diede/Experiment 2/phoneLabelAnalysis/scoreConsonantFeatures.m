function [ scores ] = scoreConsonantFeatures( phoneLabelMatrix )
%SCORECONSONANTFEATURES Based on the top 5 phones per feature,
% selects the consonant sensitive features and scores them in their
% degree of backness or frontness
%   scores = matrix with for each consonant sensitive feature its index
%   and its score
%       1   5       feature 1, scored 5
%       3   7       feature 3, scored 7

%select consonant sensitive features
featureIndices = selectConsonantFeatures(phoneLabelMatrix);

nrOfFeatures = length(featureIndices);
scores = zeros(nrOfFeatures,2);

%score them
for i=1:nrOfFeatures
    
   featureIndex = featureIndices(i);
   scores(i,1) = featureIndex;
   scores(i,2) = getFeatureScore(phoneLabelMatrix{featureIndex,1});
    
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [score] = getFeatureScore(phoneLabelCell)
%scores the feature based on the five phones it is most sensitive to

%phoneLabelCell = 5x1 cell

count = 0;
score = 0;

for i = 1:5
    if(count<3) %for the three best consonants
        phoneLabel = phoneLabelCell{i,1};
        phoneIndex = phoneToIndex(phoneLabel);
        
        if(isConsonant(phoneIndex))
            count = count + 1;
            score = score + getConsonantScore(phoneIndex);
        end
    end
end
 
end