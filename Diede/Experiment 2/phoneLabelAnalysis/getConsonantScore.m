function [ score ] = getConsonantScore_old2( phoneIndex )
%GETCONSONANTSCORE Gives the score of the consonant with the index phoneIndex

% front score: 1   (most front & front & bit front)
% middle score: 4   (middle)
% back score: 7    (most back & back & bit back)


if(not(isConsonant(phoneIndex)))
    disp('Phone is geen consonant, dus mag geen consonantscore krijgen');
    score = -1;
else
    %most front
    if(phoneIndex == 8 || ...
            phoneIndex == 29 || ...
            phoneIndex == 34 || ...
            phoneIndex == 45)
        
        score = 1;
        
        %front
    elseif(phoneIndex == 20 || ...
            phoneIndex == 43)
        
        score = 1;
        
        %bit front
    elseif(phoneIndex == 12 || ...
            phoneIndex == 40)
        
        score = 1;
        
        %skip middle
        
        %bit back
    elseif(phoneIndex == 9 || ...
            phoneIndex == 26 || ...
            phoneIndex == 35 || ...
            phoneIndex == 37)
        
        score = 7;
        
        %back
    elseif(phoneIndex == 21 || ...
            phoneIndex == 27 || ...
            phoneIndex == 31)
        
        score = 7;
        
        %most back
    elseif(phoneIndex == 22)
        
        score = 7;
        
        %middle
    else
        score = 4;
        
    end
end

end