%
%   Plots the scores of the consonant and vowel sensitive features of each layer
%

%intialize
dataLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\phoneLabelAnalysis\';
saveLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\figures\phoneLabelAnalysis\';

for layerIndex = 1:3


%load data
filename = ['phoneLabelAnalysis3_layer' num2str(layerIndex)];
data = load(strcat(dataLocation,filename), 'consonantFeatures', 'vowelFeatures');

%consonants plot and save
plotFeatureScores(data.consonantFeatures);
filename = ['consonantFeatures3_L' num2str(layerIndex)];
saveas(gcf, strcat(saveLocation,filename), 'png');

%vowels plot and save
plotFeatureScores(data.vowelFeatures);
filename = ['vowelFeatures3_L' num2str(layerIndex)];
saveas(gcf, strcat(saveLocation,filename), 'png');

end