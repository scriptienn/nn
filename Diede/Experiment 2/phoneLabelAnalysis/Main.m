%
%   For each layer: selects consonant and vowel sensitive features, scores them and saves these scores
%

dataLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\top5phonesPerFeature\';
saveLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\phoneLabelAnalysis\';

for layerIndex = 1:3
    
    %load phoneLabelMatrix
    filename = ['layer' num2str(layerIndex) 'top5phonesPerFeature.mat'];
    data = load(strcat(dataLocation,filename), 'phoneLabelMatrix');
    
    %select and score features
    consonantFeatures = scoreConsonantFeatures(data.phoneLabelMatrix);
    vowelFeatures = scoreVowelFeatures(data.phoneLabelMatrix);
    
    %save data
    filename = ['phoneLabelAnalysis3_layer' num2str(layerIndex)];
    save(strcat(saveLocation,filename), 'consonantFeatures', 'vowelFeatures');
    
end