function [maxIntensities] = saveMaxIntensities()
%saveMaxIntensities Saves for each phone of the given set the max intensity
%   maxIntensities = nrOfPhones x 1 vector with the max Intensities

%Code for getInput
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
set = 'test';


%error messaging
if(~strcmp(set, 'train') && ~strcmp(set, 'test'))
    error('Expected set to be either train or test');
end

%stuff to make it possible to load the input data
[~, ~, ~, dataPath] = setPaths();

%check train or test set
if(strcmp(set, 'train'))
    metaDataPath = strcat(dataPath, '\train\_main.mat');
else
    metaDataPath = strcat(dataPath, '\test\_main.mat');
end

%load metaData: info about filenames and size of spectrograms
metaData = load(metaDataPath, 'phonemeFileNames', 'height', 'width', 'depth', 'average', 't', 'f');
filenames = metaData.phonemeFileNames;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nrOfPhones = 45744; %whole testset

maxIntensities = zeros(nrOfPhones,1);

for i=1:nrOfPhones
    
    if(mod(i,1000)==0)
        disp(['Bij phone ' num2str(i)]);
    end
    spectrum = getInput(i,set);
    maxIntensities(i) = max(max(spectrum));
    
end

    function [spectrum] = getInput( index, set)
        %--------------------------------------------------------------------------
        %GETINPUT Loads the spectrum of the phoneme with the given index
        %from the given set
        % Input:
        %   index = index the phoneme that should be loaded
        %   set = 'train' or 'test'
        %
        % Output:
        %   spectrum = the spectrum of the phoneme: HxWxD
        
        %load file with given index
        filePath = strcat(dataPath, '\', set, '\');
        file = strcat(filePath,filenames{index});
        
        %read its spectrogram
        spectrum = readSpectrogram(file, metaData.height, metaData.width, metaData.depth);
        
        %normalize data
        avg = metaData.average;
        spectrum(:,:,:) = (spectrum(:,:,:) - avg);
        
    end


end

