%
%   plot average phones
%

% Plots the average phones that are in the averageCell variable.
% asserts that this variable already exists in workspace
%

[nrOfPhones, ~] = size(averageCell);

for i = 1:nrOfPhones
   
    spectrum = averageCell{i,2};
    figure('visible', 'off');
    plotSpectrogramRelu(spectrum, t, f);
    phone = indexToPhone(i);
    title(['average "' phone '", ' num2str(averageCell{i,1}) ' examples']);
    
   %save figure
    location = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\equalRangeAveragePhones\';
    filename = ['Average_phone_' num2str(i) '_' phone];
    saveas(gcf, strcat(location,filename), 'png');
    
end