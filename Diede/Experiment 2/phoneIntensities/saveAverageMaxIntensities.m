%
%   Save for each phone the average max intensity of all its spectrograms, in the variable averages
%

labels = getLabels('test');
labels = cell2mat(labels);

averages = zeros(48,1);

for i=1:48 %walk through phone labels
    
   indices = find(labels == i);
   maxes = maxIntensities(indices);
   averages(i,1) = mean(maxes);
    
end