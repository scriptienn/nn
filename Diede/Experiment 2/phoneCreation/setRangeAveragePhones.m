%
% setRange of Average Phones
%

%load average phones
dataLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\second part\';
file = 'averagePhones.mat';

data = load(strcat(dataLocation,file));
averagePhones = data.averageCell;

%for each average phone
nrOfPhones = length(averagePhones);
for phone=1:nrOfPhones

    %set range to vary between 0 and 1
    averagePhones{phone,2} = setRangeTo(averagePhones{phone,2},1);

end

%save equalRangedAveragePhones
filename = 'equalRangeAveragePhones';
save(strcat(dataLocation,filename),'averagePhones');