function [countAndSumCell] = createAverageSpectrograms()


%Code for getInput
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

set = 'test';

%error messaging
if(~strcmp(set, 'train') && ~strcmp(set, 'test'))
    error('Expected set to be either train or test');
end

%stuff to make it possible to load the input data
[~, ~, ~, dataPath] = setPaths();

%check train or test set
if(strcmp(set, 'train'))
    metaDataPath = strcat(dataPath, '\train\_main.mat');
else
    metaDataPath = strcat(dataPath, '\test\_main.mat');
end

%load metaData: info about filenames and size of spectrograms
metaData = load(metaDataPath, 'phonemeFileNames', 'height', 'width', 'depth', 'average', 't', 'f');
filenames = metaData.phonemeFileNames;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

labels = getLabels(set);
nrOfPhones = 45744;
nrOfLabels = 48;

%initialize cell array to save results in
countAndSumCell = cell(nrOfLabels,2);

%fill with zeros
for i=1:nrOfLabels
    
    countAndSumCell{i,1} = 0;
    countAndSumCell{i,2} = zeros(201,16);
    
end

%loop through all test phones.
for i=1:nrOfPhones
    
    if(mod(i,1000)==0)
        disp(['Bij phone ' num2str(i)]);
    end
    
    spectrum = getInput(i,set);
    labelIndex = labels{i,1};
    
    % sum cell array values with spectrum
    countAndSumCell{labelIndex,2} = countAndSumCell{labelIndex,2} + spectrum;
    countAndSumCell{labelIndex,1} = countAndSumCell{labelIndex,1} + 1;
    
end

%devide all values by the nr of phones with the label
for i=1:nrOfLabels
    
    factor = countAndSumCell{i,1};
    countAndSumCell{i,2} = countAndSumCell{i,2} ./ factor;
    
end


function [spectrum] = getInput( index, set)
%--------------------------------------------------------------------------
%GETINPUT Loads the spectrum of the phoneme with the given index
%from the given set
% Input:
%   index = index the phoneme that should be loaded
%   set = 'train' or 'test'
%
% Output:
%   spectrum = the spectrum of the phoneme: HxWxD

%load file with given index
filePath = strcat(dataPath, '\', set, '\');
file = strcat(filePath,filenames{index});

%read its spectrogram
spectrum = readSpectrogram(file, metaData.height, metaData.width, metaData.depth);

%normalize data
avg = metaData.average;
spectrum(:,:,:) = (spectrum(:,:,:) - avg);

end

end