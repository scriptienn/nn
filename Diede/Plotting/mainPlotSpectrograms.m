function [] = createSpectrograms(N, labels, f, t)
%CREATESPECTROGRAMS creates N spectograms per phone label

set = 'train';

%stuff to make it possible to load the input data
[~, ~, ~, dataPath] = setPaths();

%check train or test set
if(strcmp(set, 'train'))
    metaDataPath = strcat(dataPath, '\train\_main.mat');
else
    metaDataPath = strcat(dataPath, '\test\_main.mat');
end

%load metaData: info about filenames and size of spectrograms
metaData = load(metaDataPath, 'phonemeFileNames', 'height', 'width', 'depth', 'average', 't', 'f');
filenames = metaData.phonemeFileNames;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

saveLocation = 'C:\Users\Diede\Documents\Studie\Kunstmatige Intelligentie\Scriptie\1. Writing phase\Spectrogrammen2\';

for i=1:48
    
    indices = find(labels==i);
    for j=1:N
        
       spectrum = getInput(indices(j+1),'train'); 
       figure('visible', 'off');
       plotSpectrogramRelu2(spectrum, t, f);

       filename = [indexToPhone(i) num2str(j)];
       saveas(gcf, strcat(saveLocation,filename), 'png');
    end
end


function [spectrum] = getInput( index, set)
%--------------------------------------------------------------------------
%GETINPUT Loads the spectrum of the phoneme with the given index
%from the given set
% Input:
%   index = index the phoneme that should be loaded
%   set = 'train' or 'test'
%
% Output:
%   spectrum = the spectrum of the phoneme: HxWxD

%load file with given index
filePath = strcat(dataPath, '\', set, '\');
file = strcat(filePath,filenames{index});

%read its spectrogram
spectrum = readSpectrogram(file, metaData.height, metaData.width, metaData.depth);

%normalize data
    avg = metaData.average;
    spectrum(:,:,:) = (spectrum(:,:,:) - avg);

end

end