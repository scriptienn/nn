function [] = plotPhoneFrequency( top3List )
%PLOTPHONEFREQUENCY Plots for the data in the top3List
%   which contains all the phones that were in top3s

xbins = [1:48];
hist(top3List, xbins);

xlabels = indicesToLabels([1:48]);
xlabels = xlabels';
set(gca,'XTick', [1:48], 'XTickLabel', xlabels);

xlabel('Phone labels');
ylabel('Frequency');

end

