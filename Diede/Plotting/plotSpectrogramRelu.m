function [bar] = plotSpectrogramRelu(spectrogram, t, f)
%% plotSpectrogram
% Author: Diede Kemper, Thomas Churchman
% 
% "Plot" a the positive activations within a spectrogram. Sets axes labels, values and orientation.

    colormap('jet');
    imagesc(t, f, max(0,spectrogram(:,:,1)));
    set(gca,'YDir', 'normal', 'XTick', [0 0.04 0.08 0.12]);
    bar = colorbar;

end