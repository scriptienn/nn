function [positions] = plotFeatures( spectraCellArray, t, f)
%plotFeatures Plots multiple features within one figure with one colorbar
%   spectraCellArray = NxF cell array, with in each cell a spectrum. All
%   spectra are of equal size WxH and should have the same range.

[N,F] = size(spectraCellArray);
nrOfSpectra = numel(spectraCellArray);
spectraCellArray = reshape(spectraCellArray, 1, nrOfSpectra);


positions = cell(1,nrOfSpectra);

for spectrumIndex=1:nrOfSpectra
    
    %plot spectrum
    colormap('jet');
    spectrum = spectraCellArray{1,spectrumIndex};
    h = subplot(F,N,spectrumIndex);
    plotSpectrogramReluWithoutColorbar(spectrum, t, f);
    
    ylabel(['Top ' num2str(spectrumIndex)], 'fontweight', 'bold', 'fontsize', 12);
    
    positions{1,spectrumIndex} = get(h,'position');
    
    if(spectrumIndex == nrOfSpectra)
        xlabel('Time (s)');
        %set colorbar to whole figure
        bar = colorbar('Position', [0.92 0.12 0.025 0.80]);
        set(get(bar,'ylabel'),'String', 'Intensity');
    end
    
end

end

