function [] = plotPhoneFrequencyTrainSet( frequencies)
%PLOTPHONEFREQUENCYTRAINSET Plots how often each phone label
%appears in the train set
% frequencies = vector of all frequencies

bar(frequencies);

xlabels = indicesToLabels([1:48]);
xlabels = xlabels';
set(gca,'XTick', [1:48], 'XTickLabel', xlabels);

xlabel('Phone labels');
ylabel('Frequency');

end

