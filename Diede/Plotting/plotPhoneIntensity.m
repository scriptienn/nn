function [] = plotPhoneIntensity( averages )
%PLOTPHONEINTENSITY Plots the average phone intensity for each phone labels
%averages = vector containing the average intensity per phone label

bar(averages);

xlabels = indicesToLabels([1:48]);
xlabels = xlabels';
set(gca,'XTick', [1:48], 'XTickLabel', xlabels);

xlabel('Phone labels');
ylabel('Average max intensity');

end

