function [] = plotSpectrogramReluWithoutColorbar2(spectrogram, t, f)
%% plotSpectrogramWithoutColorbar
% Author: Diede Kemper, Thomas Churchman
% 
% "Plot" a the positive activations within a spectrogram. Sets axes labels, values and orientation.
% without a colorbar, but with y-axis values

    imagesc(t, f, max(0,spectrogram(:,:,1)));
    set(gca,'YDir', 'normal', 'XTick', [0 0.04 0.08 0.12]);

end