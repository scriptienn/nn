function [] = plotFeatureScores(featureScores)
%PLOTFEATURESCORES Summary of this function goes here
%   Detailed explanation goes here

figure('visible', 'off');

hist(featureScores(:,2), [3:21]);
set(gca,'XTick', [3:21]);
set(gca,'YTick', [1:4]);
xlabel('front                                                                                            back');
ylabel('number of features');

end

