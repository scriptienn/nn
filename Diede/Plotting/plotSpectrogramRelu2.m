function [bar] = plotSpectrogramRelu2(spectrogram, t, f)
%% plotSpectrogram
% Author: Diede Kemper, Thomas Churchman
% 
% "Plot" a the positive activations within a spectrogram. Sets axes labels, values and orientation.
% with labels on the axes

    colormap('jet');
    imagesc(t, f, max(0,spectrogram(:,:,1)));
    set(gca,'YDir', 'normal', 'XTick', [0 0.04 0.08 0.12]);
    bar = colorbar;
    
    %with labels
    ylabel('Frequency (Hz)');
    xlabel('Time (s)');
    set(get(bar,'ylabel'),'String', 'Intensity');

end