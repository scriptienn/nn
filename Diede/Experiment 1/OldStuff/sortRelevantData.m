


lagen = [1, 4, 7];

topInputsLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\visualizations\Network2_Real_NoDeltas\oldDeconvolution';
topProjectionsLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\visualizations\Network2_Real_NoDeltas\newDeconvolution';
saveLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\visualizations\Results\';


%loop lagen door: 1-4-7
for i=1:3
    
    layer = lagen(i);
    
    %loop door mogelijke features
    for feature=1:32
        
        %zoek naar bestand: top4inputs
        fileName = strcat(topInputsLocation, ...
            '\L', num2str(layer), ...
            '_F', num2str(feature), ...
            '_top4inputs.png');
        
        if(exist(fileName,'file'))
            %kopieer deze en sla op in specifieke map
            [status,message] = copyfile(fileName,saveLocation);
        end
        
        %zoek naar bestand: top4projections
        fileName = strcat(topProjectionsLocation, ...
            '\L', num2str(layer), ...
            '_F', num2str(feature), ...
            '_top4projections.png');
        
        if(exist(fileName,'file'))
            %kopieer deze en sla op in specifieke map
            copyfile(fileName,saveLocation);
        end
    end
end

