%%

close all;
clear all;
clc;

%%

MatConvNet_path = 'C:\Users\Diede\Documents\MATLAB\matconvnet-win';
net_path        = 'C:\Users\Diede\Documents\Studie\Kunstmatige Intelligentie\Scriptie\4. Deconvnet phase\matconvnet code Umut\imagenet-vgg-s.mat';

%%

run([MatConvNet_path '/matlab/vl_setupnn']);

%%

net = load(net_path);
%net = vl_simplenn_move(net, 'gpu');

%%

X = imread('deconvnet_demo', 'jpeg');
X = imresize(X, net.normalization.imageSize([1 2]));
X = single(X);
X = X - net.normalization.averageImage;
%X = gpuArray(X);

%%

activation  = [1 1];
feature_map = 1;
layer       = 5;

%%

X_hat = deconvnet_main(net, X, 'activation', activation, 'feature_map', feature_map, 'layer', layer);

%%

X_hat = X_hat - min(X_hat(:));
X_hat = X_hat / max(X_hat(:));

%%

imshow(X_hat);