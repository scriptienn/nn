function X_hat = deconvnet_unpool(d, F, G, pad)
%DECONVNET_UNPOOL Summary of this function goes here
%   Detailed explanation goes here

%fix pad: if scalar, make it [scalar, scalar, scalar,
%scalar]. Check whether correct!
[~, pad2] = size(pad);
if(pad2 == 1)
    padff = pad;
    pad = zeros(1,4);
    pad(1) = padff;
    pad(2) = padff;
    pad(3) = padff;
    pad(4) = padff;
end

X_hat       = zeros(d + [sum(pad([1 2])) sum(pad([3 4])) 0], 'single');
X_hat(G(:)) = F(:);
X_hat       = X_hat(pad(1) + 1 : end - pad(2), pad(3) + 1 : end - pad(4), :);

end
