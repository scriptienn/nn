function [F, G] = deconvnet_pool(X, method, pad, pool, stride)
%DECONVNET_POOL Summary of this function goes here
%   inputs:
%   X = HxWxD matrix

%Diede: ik ga er nu vanuit dat we niet padden
%X      = padarray(X, [pad([1 3]) 0], 0, 'pre'); 
%X      = padarray(X, [pad([2 4]) 0], 0, 'post');

[H, W, ~] = size(X);
indices = reshape(1 : numel(X), size(X)); %temp_1 is van dezelfde grootte als X, maar dan gevuld met indices 1 t/m numel(X)

%
% Stap 1: sla de switches op. Dit zijn de indices van de maximale waardes.
%

for featureIndex = size(X, 3) : -1 : 1 %van D tot 1: pak steeds een HxW matrix = een feature
    
    if strcmp(method, 'avg')
        
        error('avg is not supported.')
        
    elseif strcmp(method, 'max')
        
        %im2col --> per pool-vakje een column
        %max --> per column, dus per pool-vakje, het maximum
        [F(:, featureIndex), maxIndices] = max(im2col(X(:, :, featureIndex), pool));
        
        %pak de indices van de elementen, per pool-vakje een column
        indicesPerPoolRegion = im2col(indices(:, :, featureIndex), pool);
        
        %gpu stuff
        if isa(maxIndices, 'gpuArray')
            
            maxIndices = gather(maxIndices);
            
        end
        
        for poolRegionIndex = size(indicesPerPoolRegion, 2) : -1 : 1 %loop door de pool regions heen
            
            %save indices of max values for every poolRegion of every
            %Feature
            G(poolRegionIndex, featureIndex) = indicesPerPoolRegion(maxIndices(poolRegionIndex), poolRegionIndex);
            
        end
        
    else
        
        error('%s is not supported.', method)
        
    end
    
end

%
% Stap 2: pool de input.
%

[~, nrOfFeatures] = size(F); %F = nrOfPoolRegions x nrOfFeatures matrix with maximum values.

F = reshape(F, (H - pool(1) + 1), (W - pool(2) + 1), nrOfFeatures); 
F = F(1 : stride : end, 1 : stride : end, :); % F = newH x newW x nrOfFeatures

if strcmp(method, 'avg')
    
    error('avg is not supported.')
    
elseif strcmp(method, 'max')
    
    G = reshape(G, (H - pool(1) + 1), (W - pool(2) + 1), nrOfFeatures);
    G = G(1 : stride : end, 1 : stride : end, :);
    G = single(G);
    
else
    
    error('%s is not supported.', method)
    
end

end