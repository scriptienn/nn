function [wave] = getWave( index, set)
%--------------------------------------------------------------------------
%GETwave Loads the wave of the phoneme with the given index
%from the given set
% Input:
%   index = index the phoneme that should be loaded
%   set = 'train' or 'test'
%
% Output:
%   wave

%error messaging
if(~strcmp(set, 'train') && ~strcmp(set, 'test'))
    error('Expected set to be either train or test');
end

%stuff to make it possible to load the input data
[~, ~, ~, dataPath] = setPaths();

%check train or test set
if(strcmp(set, 'train'))
    metaDataPath = strcat(dataPath, '\train\_main.mat');
else
    metaDataPath = strcat(dataPath, '\test\_main.mat');
end

%load metaData: info about wave filenames
metaData = load(metaDataPath, 'waveFileNames');
filenames = metaData.waveFileNames;

%load file with given index
filePath = strcat(dataPath, '\', set, '\');
file = strcat(filePath,filenames{index});

%read its wave
ding = load(file, 'phonemeWave');
wave = ding.phonemeWave;

end