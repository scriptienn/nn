%
% CreateAudioFromInputs
%

%set parameters
fs = 16000;
windowSize = floor(fs/60);
framerate = floor(fs/110);
overlap = windowSize - framerate;
nfft = 400;

featuresLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\sections';
saveLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\audio';
layers = [1,4,7];

%walk through all three layers
for i=1:3
    disp(['layer ' num2str(i)]);
    
    %load allFeatures.mat
    layer = layers(i);
    file = strcat(featuresLocation, '\L', num2str(layer), '_allFeatures.mat');
    data = load(file, 'sectionsMatrix');
    features = data.sectionsMatrix;
    
    %normalize
    features = makeRangeAndSizeEqual(features, 8);
    
    %walk through all features of the layer
    for feature=1:length(features)
        %per feature:
        
        disp(['feature ' num2str(feature)]);
        
        %if feature is not empty
        projection = features{feature};
        if(not(isempty(projection)))
            
            % create three spectrograms with feature at different frequency positions
            [spectrum1, spectrum2, spectrum3] = createSpectra(features{feature});
            
            % create inverse signals
            sound1 = invspecgram(spectrum1,nfft,fs,windowSize,overlap);
            sound2 = invspecgram(spectrum2,nfft,fs,windowSize,overlap);
            sound3 = invspecgram(spectrum3,nfft,fs,windowSize,overlap);
            
            % save all three signals in separate sound files
            filename = strcat(saveLocation, ...
                '\L', num2str(i), ...
                '_F', num2str(feature), ...
                '_sound_');
            audiowrite(strcat(filename, '1.wav'),sound1,fs);
            audiowrite(strcat(filename, '2.wav'),sound2,fs);
            audiowrite(strcat(filename, '3.wav'),sound3,fs);
        end
    end
end
