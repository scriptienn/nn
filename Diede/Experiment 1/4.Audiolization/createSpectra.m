function [ spectrum1, spectrum2, spectrum3 ] = createSpectra(feature)
%createSpectra Creates three spectra each with the given feature at a
%different frequency
%   feature = Fx16 matrix with F the number of frequency bins

height = 201;
width = 16;

spectrum1 = zeros(height, width);
spectrum2 = zeros(height, width);
spectrum3 = zeros(height, width);

[F,~] = size(feature);

spectrum1(25:(24+F),:) = feature;
spectrum2(92:(91+F),:) = feature;
spectrum3(159:(158+F),:) = feature;

end

