function [] = saveMaximaPerFeatureMain(net)

%%%%%%%%%%%%%%
%
% save Maxima Per Feature Main
%
%%%%%%%%%%%%%%

%
% Walks through all maxima per input spectrogram and keeps track of
% the maximum values of all features
%

%check indices of relevant layers
layerAndFeatureIndices = getConvLayerIndices(net);

%other stuff
layers = net.layers;
topN = 9;
result = cell(numel(layers),1000);
location = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\maximaPerInput\Network2_Real_NoDeltas';

%walk through all maxima.mat files
for i=1:92
    
    disp(['Save max per feature. Epoch: ' num2str(i)]);
    
    %load maxima_i.mat file
    filename = strcat('\maxima_', num2str(i), '.mat');
    data = load(strcat(location,filename), 'maxima', 'indices');
    
    %walk through all layers
    for j=1:length(layerAndFeatureIndices)
        
        layer = layerAndFeatureIndices{j,1};
        nrOfFeaturesWithinLayer = layerAndFeatureIndices{j,2};
        
        %walk through all features within the layer
        for feature=1:nrOfFeaturesWithinLayer
            % per feature:

            % calculate new topInputs
            newTopInputs = getTopN(data.maxima, layer, feature, topN, data.indices); %topN x 4 matrix

            % compare new topInputs with old topInputs and select best topInputs
            oldTopInputs = result{layer,feature};
            
            if(not(isempty(oldTopInputs)))
                bestTopInputs = selectBestTopInputs(oldTopInputs, newTopInputs);
            else
                bestTopInputs = newTopInputs;    
            end
            
            % save best topInputs
            result{layer,feature} = bestTopInputs;
        end
    end
    
    %clear memory
    clearvars data;
end

savedFile = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\maximaPerFeature\Network2_Real_NoDeltas\featuresMaxima.mat';
save(savedFile,'result');