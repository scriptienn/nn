function [ bestTopInputs ] = selectBestTopInputs( topInputs1, topInputs2 )
%selectBestTopInputs Select from two matrices of topinputs the best
%topinputs
%   Input:
%   -topInputs1 = topNx4 matrix. Per topInput (row) it contains:
    %           - the inputIndex of the spectrum
    %           - the maxValue
    %           - the X-coordinate
    %           - the Y-coordinate
%   -topInputs2 = topNx4 matrix. Has the same structure.
%
%   -bestTopInputs = topNx4 matrix. Contains the topN highest maxValues
%   from topInputs1 and topInputs2.

%error messaging
[nrOfRows1,nrOfColumns1] = size(topInputs1);
[nrOfRows2,nrOfColumns2] = size(topInputs2);
assert(nrOfRows1 == nrOfRows2, 'Nr of rows of topInput matrices is not the same');
assert(nrOfColumns1 == nrOfColumns2, 'Nr of columns of topInput matrices is not the same');

%merge matrices
mergedInputs = [topInputs1 ; topInputs2];

%sort in descending order, based on maxValue column
mergedInputs = sortrows(mergedInputs, -2); 

%take the upper topN rows for the new matrix
bestTopInputs = mergedInputs(1:nrOfRows1,:);

end

