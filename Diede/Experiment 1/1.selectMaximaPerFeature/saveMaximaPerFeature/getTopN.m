function [topValues] = getTopN(maxima, layer, feature, topN, indices)
    %--------------------------------------------------------------------------
    %GETTOPN returns the topN images that give the maximum activities for
    %the given feature in the given layer
    %
    %   input:
    %   - maxima: (L-1)xN cell array, with Dx3 cell arrays inside
    %   - layer
    %   - feature
    %   - topN
    %   - indices: the indices of the inputs of the maxima matrix
    %
    %   output:
    %   - topValues = a topN x 4 matrix. For every row it contains:
    %           - the inputIndex of the spectrum
    %           - the maxValue
    %           - the X-coordinate
    %           - the Y-coordinate
    
    bestInput = zeros(topN,1);
    bestMax = zeros(topN,1);
    bestX = zeros(topN,1);
    bestY = zeros(topN,1);
    
    [~, nrOfInputs] = size(maxima);
    
    for inputIndex=1:nrOfInputs
        
        ff = maxima{layer, inputIndex}; %nrOfFeatures x 3 cell array
        max = ff{feature,1};
        x = ff{feature,2};
        y = ff{feature,3};
        
        [minBest, minIndex] = min(bestMax);
        
        if(max > minBest)
            
            bestMax(minIndex) = max;
            bestInput(minIndex) = indices(inputIndex);
            bestX(minIndex) = x;
            bestY(minIndex) = y;
            
        end
    
    end 
    
    topValues = [bestInput bestMax bestX bestY];
    topValues = sortrows(topValues, -2); %sort in descending order
    
end