function [] = saveMaximaMain(net)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Save Maxima Main
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Saves the maximum values per input image for all features
%Stores these in .mat files per 500 input images

for i=1:92
    
    disp(['Save Max per Input. Epoch: ' num2str(i)]); 

    %info about data
    nrOfInputs = 45744;
    matSize = 500;
    set = 'test';
    
    %select input indices
    from = (i-1)*matSize+1;
    if(i<92)
        to = (i*matSize);
    else
        to = nrOfInputs;
    end
    indices = from:to;
    
    %calculate maximum values
    maxima = SaveMaxActivities(net, indices, set);
    
    %save data
    location = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\maximaPerInput\Network2_Real_NoDeltas';
    filename = strcat(location,'\maxima_',num2str(i));
    save(filename, 'indices', 'set', 'maxima');
    
    %clear workspace except the network
    clearvars -except 'net';
end