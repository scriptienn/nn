function [maxima] = SaveMaxActivities(net, X, set)
%SaveMaxActivities For convnet net and input indices X, compute all maximum
%activities. Input:
%   net = a convnet
%   X = a vector of indices of all input spectrograms
%   set = 'train' or 'test'
% Output:
% maxima = cell array of size (L-1)xNxDx3. L = number of layers of net,
% N = number of input spectrograms, D = number of features within the layer. 
% The fourth dimension has a dimensionality of 3:
    % - max activity
    % - x coordinate
    % - y coordinate
    
    %error messaging
    if(~strcmp(set, 'train') && ~strcmp(set, 'test'))
        error('Expected set to be either train or test');
    end
    
    %stuff to make it possible to load the input data
    [~, ~, ~, dataPath] = setPaths();
    
    if(strcmp(set, 'train'))
        metaDataPath = strcat(dataPath, '\train\_main.mat');
    else %set = test
        metaDataPath = strcat(dataPath, '\test\_main.mat');
    end

metaData = load(metaDataPath, 'phonemeFileNames', 'height', 'width', 'depth', 'average', 't', 'f');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nrOfInputs = numel(X);
nrOfLayers = numel(net.layers);
maxima = cell((nrOfLayers-1), nrOfInputs);

for inputIndex = 1:nrOfInputs
   
    spectrum = getInput(X(inputIndex));
    
    %keep track of sizes, activations and switches
    Y = struct('d', cell(1, nrOfLayers + 1), 'F', cell(1, nrOfLayers + 1), 'G', cell(1, nrOfLayers + 1));
    Y(1).F = spectrum;
    Y(1).d = size(Y(1).F);
        
    %forward pass: generate activations. F = activations, G = switches.
    for layerIndex = 1 : (nrOfLayers-1) %do not need to generate activations of last layer

        switch net.layers{layerIndex}.type

            case 'conv'

                Y(layerIndex + 1).F = vl_nnconv(Y(layerIndex).F, net.layers{layerIndex}.filters, net.layers{layerIndex}.biases, 'pad', net.layers{layerIndex}.pad, 'stride', net.layers{layerIndex}.stride);
                Y(layerIndex + 1).d = size(Y(layerIndex + 1).F);

            case 'relu'

                Y(layerIndex + 1).F = vl_nnrelu(Y(layerIndex).F);
                Y(layerIndex + 1).d = size(Y(layerIndex + 1).F);

            case 'normalize'

                Y(layerIndex + 1).F = vl_nnnormalize(Y(layerIndex).F, net.layers{layerIndex}.param);
                Y(layerIndex + 1).d = size(Y(layerIndex + 1).F);

            case 'pool'
                %only switches when pool layer
                [Y(layerIndex + 1).F, Y(layerIndex + 1).G] = deconvnet_pool(Y(layerIndex).F, net.layers{layerIndex}.method, net.layers{layerIndex}.pad, net.layers{layerIndex}.pool, net.layers{layerIndex}.stride);
                Y(layerIndex + 1).d = size(Y(layerIndex + 1).F);

            case 'dropout'
                Y(layerIndex + 1).F = Y(layerIndex).F;
                Y(layerIndex + 1).d = Y(layerIndex).d;
                
            otherwise

                error('%s is not supported', net.layers{layerIndex}.type);

        end

        %save maxima and corresponding locations
        if(strcmp(net.layers{layerIndex}.type,'conv'))
            maxima{layerIndex,inputIndex} = getMaxima(Y(layerIndex + 1).F);
        end
        
        Y(layerIndex).F = [];

    end

end

function [spectrum] = getInput( index)
    %--------------------------------------------------------------------------
    %GETINPUT Loads the spectrum of the phoneme with the given index
    %from the given set
    % Input:
    %   index = index the phoneme that should be loaded
    %   set = 'train' or 'test'
    %
    % Output:
    %   spectrum = the spectrum of the phoneme: HxWxD
    
    filenames = metaData.phonemeFileNames;
    
    %get spectrum of phoneme
    filePath = strcat(dataPath, '\', set, '\');
    file = strcat(filePath,filenames{index});
    spectrum = readSpectrogram(file, metaData.height, metaData.width, metaData.depth);
    
    %normalize data
    avg = metaData.average;
    spectrum(:,:,:) = (spectrum(:,:,:) - avg);
    
end

function [maxima] = getMaxima(activationMatrix)
    %--------------------------------------------------------------------------
    %GETMAXIMA returns the maximum values of activationMatrix and their
    %coordinates
    %
    %   input:
    %   - activationMatrix = a 3D matrix with dimensions HxWxD
    %
    %   output:
    %   - maxima = a Dx3 cell array. For every HxW submatrix of the
    %   activationMatrix, it contains the maximum value, its x-coordinate
    %   and its y-coordinate
    
    [~, ~, nrOfFeatures] = size(activationMatrix);
    maxima = cell(nrOfFeatures, 3);
    
    for i=1:nrOfFeatures
        
        feature = activationMatrix(:,:,i);          %feature = HxW submatrix
        [subMax, subIndex] = max(feature,[],1);
        [maxValue, rowIndex] = max(subMax,[],2);
        columnIndex = subIndex(rowIndex);
        
        maxima{i,1} = maxValue;     %save maximum value
        maxima{i,2} = columnIndex;  %and its location
        maxima{i,3} = rowIndex;
        
    end
    
end

end



