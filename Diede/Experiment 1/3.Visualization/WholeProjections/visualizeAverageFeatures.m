
%set all paths
dataPath = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\resultPerFeature';
figuresPath = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\visualizations';
matrixPath = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\maximaPerFeature\featuresMaxima.mat';
[~, ~, ~, metaDataPath] = setPaths();

%load phonemeIndices
metaDataPath = strcat(metaDataPath, '\test\_main.mat');
data = load(metaDataPath, 'phonemes');
phonemeIndices = data.phonemes;

%load indicesmatrix of input spectrograms
data = load(matrixPath, 'result');
inputIndices = data.result;

%load f and t to be able to plot spectrograms
data = load('C:\Users\Diede\Documents\MATLAB\nn\FandT.mat', 'f', 't');
f = data.f;
t = data.t;

%take indices of relevant layers
net = getNet();
layerIndices = getConvLayerIndices(net);
[nrOfLayers,~] = size(layerIndices);

%walk through all relevant layers
for i = 1:nrOfLayers
    layer = layerIndices{i,1};
    nrOfFeatures = layerIndices{i,2};
    
    %walk through all deconvolved features within the layer
    for feature = 1:nrOfFeatures
  
        disp(strcat('layer ', num2str(layer), ...
            ', feature ', num2str(feature)));
        
        fileName = strcat(dataPath, ...
            '\l', num2str(layer), ...
            '_f', num2str(feature), ...
            '_top1', ...
            '_singleLocation.mat');
        
        %check whether first file exist for this feature
        if(exist(fileName,'file'))
            
            inputIndicesMatrix = inputIndices{layer, feature};
            
            sumProjection = zeros(201,16,1);
            
            %walk through the topN projections
            for topN=1:9
                
                %file containing projection
                file = strcat(dataPath, ...
                    '\l', num2str(layer), ...
                    '_f', num2str(feature), ...
                    '_top', num2str(topN), ...
                    '_singleLocation.mat');
                
                %check whether the file exist
                if(exist(file,'file'))
                    
                    %load projection
                    data = load(file, 'projection');
                    tempProjection = data.projection;
                    %sum with earlier projections
                    sumProjection = sumProjection + tempProjection;
                end
            end
            
            avgProjection = sumProjection / topN;
            
            %plot average projection
            figure('visible', 'off');
            bar = plotSpectrogramRelu(avgProjection, t, f);
            title(strcat('L', num2str(layer), ...
                'F', num2str(feature), ...
                ' Average Projection'));
            
            ylabel('Frequency (Hz)');
            xlabel('Time (s)');
            set(get(bar,'ylabel'),'String', 'Intensity');
            
            %save figure
            filename = strcat(figuresPath, ...
                '\L', num2str(layer), ...
                '_F', num2str(feature), ...
                '_averageProjection');
            saveas(gcf, filename, 'png');
                      
        end
    end
end