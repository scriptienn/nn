function [] = visualizeFeatures(net, visualizeAverage)

%set all paths
dataPath = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\resultPerFeature\Network2_Real_NoDeltas\newDeconvolution';
figuresPath = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\visualizations\Network2_Real_NoDeltas\newDeconvolution';
matrixPath = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\maximaPerFeature\Network2_Real_NoDeltas\featuresMaxima.mat';
[~, ~, ~, metaDataPath] = setPaths();

%load phonemeIndices
metaDataPath = strcat(metaDataPath, '\test\_main.mat');
data = load(metaDataPath, 'phonemes');
phonemeIndices = data.phonemes;

%load indicesmatrix of input spectrograms
data = load(matrixPath, 'result');
inputIndices = data.result;

%load f and t to be able to plot spectrograms
data = load('C:\Users\Diede\Documents\MATLAB\nn\FandT.mat', 'f', 't');
f = data.f;
t = data.t;

%take indices of relevant layers
layerIndices = getConvLayerIndices(net);
[nrOfLayers,~] = size(layerIndices);

%walk through all relevant layers
for i = 1:1 %veranderd
    layer = layerIndices{i,1};
    nrOfFeatures = layerIndices{i,2};
    
    %walk through all deconvolved features within the layer
    for feature = 1:nrOfFeatures
        
        disp(strcat('visualizeFeatures. layer: ', num2str(layer), ...
            ', feature: ', num2str(feature)));
        
        fileName = strcat(dataPath, ...
            '\l', num2str(layer), ...
            '_f', num2str(feature), ...
            '_top1.mat');
        
        %check whether first file exist for this feature
        if(exist(fileName,'file'))
            
            if(visualizeAverage)
                
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %       STEP 0: average projection
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                
                sumProjection = zeros(201,16,1);
                
                %walk through the topN projections
                for topN=1:9
                    
                    %file containing projection
                    file = strcat(dataPath, ...
                        '\l', num2str(layer), ...
                        '_f', num2str(feature), ...
                        '_top', num2str(topN), ...
                        '.mat');
                    
                    %check whether the file exist
                    if(exist(file,'file'))
                        
                        %load projection
                        data = load(file, 'projection');
                        tempProjection = data.projection;
                        %sum with earlier projections
                        sumProjection = sumProjection + tempProjection;
                    end
                end
                
                avgProjection = sumProjection / topN;
                
                %plot average projection
                figure('visible', 'off');
                bar = plotSpectrogramRelu(avgProjection, t, f);
                title(strcat('L', num2str(layer), ...
                    'F', num2str(feature), ...
                    ' Average Top 9 Projection'));
                
                ylabel('Frequency (Hz)');
                xlabel('Time (s)');
                set(get(bar,'ylabel'),'String', 'Intensity');
                
                %save figure
                filename = strcat(figuresPath, ...
                    '\L', num2str(layer), ...
                    '_F', num2str(feature), ...
                    '_averageProjection');
                saveas(gcf, filename, 'png');
                
            else
                
           
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %       STEP 1: projections
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                figure('visible', 'off');
                
                %walk through the topN projections
                for topN=1:1 %4
                    
                    %file containing projection
                    file = strcat(dataPath, ...
                        '\l', num2str(layer), ...
                        '_f', num2str(feature), ...
                        '_top', num2str(topN), ...
                        '.mat');
                    
                    %check whether the file exist
                    if(exist(file,'file'))
                        
                        %load projection
                        data = load(file, 'projection');
                        
                        %plot projection
                        %subplot(2,2,topN); %veranderd!
                        bar = plotSpectrogramRelu(data.projection, t, f);
                        %title(strcat('Top ', num2str(topN)));
                        
                        %if(topN == 3)
                            ylabel('Frequency (Hz)');
                            xlabel('Time (s)');
                            set(get(bar,'ylabel'),'String', 'Intensity');
                        %end
                        
                    end
                end
                
                superTitle(strcat('L', num2str(layer), ...
                    'F', num2str(feature), ...
                    ' Projections'));
                
                %save four plots within one figure
                filename = strcat(figuresPath, ...
                    '\L', num2str(layer), ...
                    '_F', num2str(feature), ...
                    '_top1projection');
                saveas(gcf, filename, 'png');
                
                %{
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %       STEP 2: Corresponding inputs
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                
                figure('visible', 'off');
                inputIndicesMatrix = inputIndices{layer, feature};
                %walk through the topN corresponding inputs
                for topN=1:4
                    
                    index = inputIndicesMatrix(topN, 1);
                    
                    if(not(index == 0))
                        
                        %load original input spectrogram
                        spectrum = getInput(index, 'test');
                        
                        %load corresponding phoneme label
                        phonemeIndex = phonemeIndices{index};
                        phonemeLabel = indexToPhone(phonemeIndex);
                        
                        %plot original input spectrogram
                        subplot(2,2,topN);
                        bar = plotSpectrogramRelu(spectrum,t,f);
                        title(strcat('Top ', num2str(topN), '. "', ...
                            phonemeLabel, '"', ...
                            ' index ', num2str(index)));
                        
                        if(topN == 3)
                            ylabel('Frequency (Hz)');
                            xlabel('Time (s)');
                            set(get(bar,'ylabel'),'String', 'Intensity');
                        end
                    end
                end
                
                superTitle(strcat('L', num2str(layer), ...
                    'F', num2str(feature), ...
                    ' Inputs'));
                
                %save four plots within one figure
                filename = strcat(figuresPath, ...
                    '\L', num2str(layer), ...
                    '_F', num2str(feature), ...
                    '_top4inputs');
                saveas(gcf, filename, 'png');
                
                %}
            end
        end
    end
end