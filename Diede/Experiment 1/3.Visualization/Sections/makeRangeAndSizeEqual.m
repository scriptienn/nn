function [ spectraCellArray ] = makeRangeAndSizeEqual( spectraCellArray, maxValue)
%makeRangeAndSizeEqual make the range and size of each matrix equal
%   spectraCellArray = NxF cell array, with each cell containing a double
%   matrix of size ?x16. ? varies between all cells

%   Returns the same cell array, but then the values of each matrix range
%   from 0 to maxValue.
%   Also, all matrices are zeropadded such that their first dimension is
%   the same as the first dimension of the largest matrix in the cell array.


[N,F] = size(spectraCellArray);

%decide what is the maximum value of the first dimensions of all matrices
matrixSizes = cell2mat(cellfun(@size,spectraCellArray, 'uni', false));

%remove all even columns
matrixSizes = matrixSizes(:, 1:2:end);
maxSizes = max(matrixSizes);
maxX = max(maxSizes);

% walk through matrices
for n=1:N
    for f=1:F
        
        spectrum = spectraCellArray{n,f};
        
        if(not(isempty(spectrum)))
            [x,y] = size(spectrum);
            
            diffX = maxX - x;
            %if necessary, zeropad the matrix to be of size maxX by y
            if(diffX > 0)
                
                leftOver = mod(diffX,2);
                
                %spectrum needs an odd number of rows zeropadded
                if(leftOver == 1)
                    %make diffX even
                    diffX = diffX - 1;
                    
                    %add extra row of zeros
                    row = zeros(1,y);
                    spectrum = [row; spectrum];
                end
                
                %add an even numbers of rows of zeros
                padsize = [diffX/2,0];
                spectrum = padarray(spectrum,padsize);
                
            end
            
            %make the range of the matrix between 0 and maxValue
            spectrum = setRangeTo(spectrum,maxValue);
                       
            %add spectrum back to cell array
            spectraCellArray{n,f} = spectrum;
        end
    end
    
end

end

