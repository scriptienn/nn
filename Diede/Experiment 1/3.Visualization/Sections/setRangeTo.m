function [ spectrum ] = setRangeTo(spectrum, maxRange )
%setRangeTo Sets the range of the given matrix to range from 0 to maxRange

%select max value within spectrum
maxValue = max(spectrum(:));

%decide with which factor the matrix needs to be multiplied
factor = maxRange/maxValue;

%multiply with factor
spectrum = spectrum * factor;

end

