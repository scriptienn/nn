%
% createSections

dataLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\resultPerFeature\Network2_Real_NoDeltas\newDeconvolution';
layers = [1, 4, 7];

sectionsMatrix = cell(3,8);

for i = 1:1
    layer = layers(i);
    
    %loop door mogelijke features
    for feature=1:8
        
        for topN=1:3
            
            %zoek naar bestand
            fileName = strcat(dataLocation, ...
                '\l', num2str(layer), ...
                '_f', num2str(feature), ...
                '_top', num2str(topN), ...
                '.mat');
            
            if(exist(fileName,'file'))
                data = load(fileName, 'projection');
                
                projection = data.projection;
                projection(all(projection==0,2),:)=[];
                
                sectionsMatrix{topN,feature} = double(projection);
            end
            
        end
    end
end
%save matrix on computer
resultLocation = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\sections';
filename = strcat(resultLocation, ...
    '\L1_allFeatures_new');
save(filename, 'sectionsMatrix');