%
%
% Main: plotSection
%
% Plots only the activated sections of the projections
%

%load t to be able to plot spectrograms
data = load('C:\Users\Diede\Documents\MATLAB\nn\FandT.mat', 'f', 't');
t = data.t;
f = [];

%load cellArray with sections
location = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\sections\';
file = 'L7_allFeatures.mat';
data = load(strcat(location,file), 'sectionsMatrix');
spectraCellArray = data.sectionsMatrix;

%prepare for nice visualizations
spectraCellArray = makeRangeAndSizeEqual(spectraCellArray, 6);

[nrOfTops, nrOfFeatures] = size(spectraCellArray);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
% All features in one plot
%
%{
figure('visible', 'off');
positions = plotFeatures(spectraCellArray,t,f);

%save figure
location = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\visualizations\Sections\';
filename = 'L1_allFeatures_noTitle';
saveas(gcf, strcat(location,filename), 'png');
%}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
% One plot per feature
%
for feature=1:nrOfFeatures
    
    %create figure
    featureCellArray = cell(1,4);
    featureCellArray{1,1} = spectraCellArray{1,feature};
    featureCellArray{1,2} = spectraCellArray{2,feature};
    featureCellArray{1,3} = spectraCellArray{3,feature};
    featureCellArray{1,4} = spectraCellArray{4,feature};
    
    figure('visible', 'off');
    positions = plotMultipleFeatures(featureCellArray,t,f);
    %superTitle(['Feature ' num2str(feature) ' of layer 2']);
    
    %save figure
    location = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\visualizations\Sections\';
    filename = ['L7_F' num2str(feature)];
    saveas(gcf, strcat(location,filename), 'png');
    
end
