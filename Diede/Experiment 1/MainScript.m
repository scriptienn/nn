%
% Performs the whole deconvolution process in four steps:
%   1. Save the maximum values per input image for all features
%   2. Save the maximum value info per feature: 
%       - index of input image
%       - the maximum value
%       - the x-coordinate
%       - the y-coordinate
%   3. Create projections by applying deconvolution on the top N values for (a selection of) the
%   features
%   4. Visualize the projections and save the spectrograms in separate
%   files

clear;

%start matConvnet
MatConvNet_path = 'C:\Users\Diede\Documents\MATLAB\matconvnet-win';
run([MatConvNet_path '/matlab/vl_setupnn']);

%get trained network
netPath = 'C:\Users\Diede\Documents\MATLAB\data\networkTrainedOnRealsAndNoDeltas.mat';
data = load(netPath, 'net');
net = data.net;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       STEP 1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
saveMaximaMain(net);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       STEP 2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clearvars -except net;
saveMaximaPerFeatureMain(net);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       STEP 3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clearvars -except net;

%start matConvnet
MatConvNet_path = 'C:\Users\Diede\Documents\MATLAB\matconvnet-win';
run([MatConvNet_path '/matlab/vl_setupnn']);

deconvolutionMain(net);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       STEP 4.a: projections and inputs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clearvars -except net;

visualizeFeatures(net, false);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       STEP 4.b: average projection
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%clearvars -except net;
%
%visualizeFeatures(net, true);


