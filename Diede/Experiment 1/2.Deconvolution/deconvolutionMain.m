function [] = deconvolutionMain(net)

%%%%%%%%%%%%%%%%%%%%%%%%
%
% deconvolutionMain
%
%%%%%%%%%%%%%%%%%%%%%%%%

%
% Apply deconvolution for each topN input for each feature of each layer
% Save projection back to input space in separate .mat file
%

%get maximum values per feature
maximaPath = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\maximaPerFeature\Network2_Real_NoDeltas\featuresMaxima.mat';
ding = load(maximaPath, 'result');
maxima = ding.result; %18 x 1000 cell array, with
%in each cell array a 9x4 double matrix

%check indices of relevant layers
layerAndFeatureIndices = getConvLayerIndices(net);
nrOfLayers = length(layerAndFeatureIndices);

location = 'C:\Users\Diede\Documents\MATLAB\data\deconvolution\resultPerFeature\Network2_Real_NoDeltas\newDeconvolution';
set = 'test';

%walk through layers
for i=1:nrOfLayers
    
    layer = layerAndFeatureIndices{i,1};
    nrOfFeatures = layerAndFeatureIndices{i,2};
    
    %if more than 100 features, pick 100 at random
    if(nrOfFeatures>100)
        featureIndices = sort(randsample(nrOfFeatures,100));
    else
        featureIndices = 1:nrOfFeatures;
    end
    
    %walk through features within layer
    for j=1:length(featureIndices)
        
        feature = featureIndices(j);

        disp(['Deconvolution. layer: ' num2str(layer) ', feature: ' num2str(feature)]);
        
        featureMatrix = maxima{layer,feature};
        [topN, ~] = size(featureMatrix);
        
        %walk through topN inputs
        for topIndex=1:topN
            
            inputIndex = featureMatrix(topIndex,1);
            if(not(inputIndex==0))
                %apply deconvnet to feature in layer with selected top input
                projection = applyDeconvnet(net, layer, feature, featureMatrix, topIndex, set);

                %save result in .mat file
                filename = strcat(location, ...
                    '\l', num2str(layer), ...
                    '_f', num2str(feature), ...
                    '_top', num2str(topIndex));
                save(filename, 'projection');
            end
        end
    end
end