function [ visualizations ] = applyDeconvnet(net, layer, feature, topInputs, topIndex, set)
%APPLYDECONVNET applies the deconvnet to the values in maxima
%   input:
%   - net = the convnet of which the deconvnet version is applied
%   - maxima = cell array of size (L-1)xNxDx3. L = number of layers of net,
% N = number of input spectrograms, D = number of features within the layer.
% The fourth dimension contains the max activity, its x coordinate and its
% y coordinate.
%   - layer = the layer that is visualized
%   - feature = the feature within the layer that is visualized
%   - topInputs = for the given feature within the given layer the topN top inputs
%   - topIndex = the index of the top input that should be visualized
%   - set = 'train' or 'test'
%
%   output:
%   - visualizations = matrix containing the projection back to input space
%   of the given feature in the given layer for the given top input.
%   Size: H x W x D, with H being the height, W being the width and D being
%   the depth of input space.

%check if feature and layer exist within the network
nrOfLayers = numel(net.layers);
assert(layer <= nrOfLayers);
[~, ~, ~, nrOfFeatures] = size(net.layers{layer}.filters);
assert(feature<= nrOfFeatures);

%get size of spectrogram inputs
[H, W, D] = size(getInput(topInputs(1,1), set));

%create matrix to save result in
if(D==1)
    visualizations = zeros(H, W);
else
    visualizations = zeros(H, W, D);
end

net.layers(layer + 1 : end) = [];

%STEP 1: forward pass: generate activations. F = activations, G = switches.

%create variables for the forward pass
nrOfLayers = numel(net.layers);
Y = struct('d', cell(1, nrOfLayers + 1), 'F', cell(1, nrOfLayers + 1), 'G', cell(1, nrOfLayers + 1));
inputIndex = topInputs(topIndex,1);
input = getInput(inputIndex, set);
Y(1).F = input;
Y(1).d = size(Y(1).F);

%go through all layers
for index = 1 : nrOfLayers
    
    switch net.layers{index}.type
        
        case 'conv'
            
            Y(index + 1).F = vl_nnconv(Y(index).F, net.layers{index}.filters, net.layers{index}.biases, 'pad', net.layers{index}.pad, 'stride', net.layers{index}.stride);
            Y(index + 1).d = size(Y(index + 1).F);
            
            Y(index).F = []; %toegevoegd bij alle lagen behalve relu
            
        case 'relu'
            
            Y(index + 1).F = vl_nnrelu(Y(index).F);
            Y(index + 1).d = size(Y(index + 1).F);
            
        case 'normalize'
            
            Y(index + 1).F = vl_nnnormalize(Y(index).F, net.layers{index}.param);
            Y(index + 1).d = size(Y(index + 1).F);
            
            Y(index).F = [];
            
        case 'pool'
            %only switches in case pool
            [Y(index + 1).F, Y(index + 1).G] = deconvnet_pool(Y(index).F, net.layers{index}.method, net.layers{index}.pad, net.layers{index}.pool, net.layers{index}.stride);
            Y(index + 1).d = size(Y(index + 1).F);
            
            Y(index).F = [];
            
        case 'dropout'
            Y(index + 1).F = Y(index).F;
            Y(index + 1).d = Y(index).d;
            
            Y(index).F = [];
            
        otherwise
            
            error('%s is not supported', net.layers{index}.type);
            
    end
    
end

%STEP 2: build deconvnet
deconvnet = convnet_to_deconvnet(net);

%location of activation within the feature
activation = [topInputs(topIndex,3), topInputs(topIndex,4)];

%set all other activations to zero
d   = size(Y(end).F);

if feature > 0 %activations in all other feature maps
    
    assert(feature <= d(3))
    
    Y(end).F(:, :, [1 : feature - 1 feature + 1 : end]) = 0;
    
end

if activation(1) > 0 %all other activations within feature map, X-axis
    
    assert(activation(1) <= d(1))
    
    Y(end).F([1 : activation(1) - 1 activation(1) + 1 : end], :, :) = 0;
    
end

if activation(2) > 0 %all other activations within feature map, Y-axis
    
    assert(activation(2) <= d(2))
    
    Y(end).F(:, [1 : activation(2) - 1 activation(2) + 1 : end], :) = 0;
    
end

Y = Y(end : -1 : 1);

%STEP 3: apply deconvnet to activation
for index = 1 : nrOfLayers
    
    switch deconvnet.layers{index}.type
        
        case 'deconvnet_conv'
            
%             %fix stride: if scalar, make it [scalar 1]. Check whether
%             %correct!!
%             [~, stride2] = size(deconvnet.layers{index}.stride);
%             if(stride2 == 1)
%                 stride = deconvnet.layers{index}.stride;
%                 deconvnet.layers{index}.stride = zeros(1,2);
%                 deconvnet.layers{index}.stride(1) = stride;
%                 deconvnet.layers{index}.stride(2) = 1;
%             end
%             
%             %fix pad: if scalar, make it [scalar, scalar, scalar,
%             %scalar]. Check whether correct!
%             [~, pad2] = size(deconvnet.layers{index}.pad);
%             if(pad2 == 1)
%                 pad = deconvnet.layers{index}.pad;
%                 deconvnet.layers{index}.pad = zeros(1,4);
%                 deconvnet.layers{index}.pad(1) = pad;
%                 deconvnet.layers{index}.pad(2) = pad;
%                 deconvnet.layers{index}.pad(3) = pad;
%                 deconvnet.layers{index}.pad(4) = pad;
%             end
            
            %Umut's code
            temp_1 = Y(index).F;
            d      = size(deconvnet.layers{index}.filters);
                       
            %zero pad the activity with zeros --> output will be of correct
            %size
            temp_1 = padarray(temp_1, [d([1 2]) - 1 0]);
            
            %apply deconvnet filters (thus flipped en permuted) to activity
            Y(index + 1).F = vl_nnconv(temp_1, deconvnet.layers{index}.filters, deconvnet.layers{index}.biases);
                     
        case 'deconvnet_relu'
            %toegevoegd!! nieuwe vorm deconvolution!
            Y(index).F(Y(index + 1).F < 0) = 0;
            
            Y(index + 1).F = vl_nnrelu(Y(index).F);
            
        case 'deconvnet_normalize'
            
            Y(index + 1).F = Y(index).F;
            
        case 'dropout'
            
            Y(index+1).F = Y(index).F;
            
        case 'deconvnet_unpool'
            
            Y(index + 1).F = deconvnet_unpool(Y(index + 1).d, Y(index).F, Y(index).G, deconvnet.layers{index}.pad);
            
        otherwise
            
            error('%s is not supported', deconvnet.layers{index}.type);
            
    end
    
    
    Y(index).F = [];
end

%save results
visualizations = Y(end).F;


