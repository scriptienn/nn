function [ layerAndFeatureIndices] = getConvLayerIndices( net )
%getConvLayerIndices returns the indices of the conv layers within net
%   layerAndFetureIndices = a Nx2 cell array, with N being the number of
%   conv layers within the given net. For every conv layer the layer index
%   is given along with the number of features within that layer.

%check indices of relevant layers
layers = net.layers;
layerAndFeatureIndices = cell(1,2);
for i=1:numel(layers)
    
    if(strcmp(net.layers{i}.type,'conv'))
        [~, ~, ~, nrOfFilters] = size(net.layers{i}.filters);
        layerAndFeatureIndices{end+1,1} = i;
        layerAndFeatureIndices{end,2} = nrOfFilters;
    end
    
end
%remove empty rows
layerAndFeatureIndices(all(cellfun(@isempty,layerAndFeatureIndices),2),:) = [];

end

