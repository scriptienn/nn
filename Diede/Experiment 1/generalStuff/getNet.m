function [ net ] = getNet( )
%getNet Gives the trained network
%   Detailed explanation goes here

%get trained network
netPath = 'C:\Users\Diede\Documents\MATLAB\data\networkTrainedOnRealsAndNoDeltas.mat';
data = load(netPath, 'net');
net = data.net;

end

