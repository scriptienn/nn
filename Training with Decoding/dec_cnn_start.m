function [net, info] = dec_cnn_start(net)
%% dec_cnn_start
% Author: Thomas Churchman
%
% Initialize and start the training of a convolutional neural network.
%
% Input:
% - net (optional): if passed (and if not continuing training from the networks
%   saved to disk in the experiments folder) the training will start from the 
%   passed network, as opposed to evaluating a new network architecture.
%
% Output:
% - net: the trained network.
% - info: information about the training and validation processes.

    [~, ~, matconvnetPath, inputPath, experimentPath] = setPaths();
    
    % Prepare matconvnet
    run(strcat(matconvnetPath, '\matlab\vl_setupnn.m'));
	
    % Load training metadata
    trainMetaDataPath = strcat(inputPath, '\train\_main.mat');
    trainMetaData = load(trainMetaDataPath, 'phonemes', 'phonemeFileNames', 'height', 'width', 'depth', 'average');

    testMetaDataPath = strcat(inputPath, '\test\_main.mat');
    testMetaData = load(testMetaDataPath, 'phonemes', 'phonemeFileNames', 'height', 'width', 'depth', 'average');

    % Set network architecture
    architecture = 'cnn_autoencoder_allrandomae_convonly';
    
    % Prepare options
    opts = {};
    opts.numEpochs = 500;
    opts.batchSize = 128;
    opts.examplesPerPhoneme = 500; % Take x examples of each phoneme per epoch
    opts.testPerPhoneme = 200; % Take x examples of each phoneme per test epoch
    opts.useGpu = false;
    opts.learningRate = [0.004 0.002];
    opts.continue = true;
    opts.learningRateChanged = false;
    opts.expDir = experimentPath;
    opts.conserveMemory = false;
    opts.sync = true;
    opts.prefetch = false;
    opts.weightDecay = 0.0005;
    opts.momentum = 0.9;
    opts.errorType = 'multiclass'; 
    opts.errorOncePerNEEpochs = true;
    opts.NE = 2;
    opts.plotDiagnostics = false;
    % opts = vl_argparse(opts, varargin);
    % Adagrad:
    opts.ada_master_stepsize = 1e-2; % default: 1e-2
    opts.ada_fudge_factor = 1e-9;
    % Layer-wise updating
    %opts.batchesPerLayer = 188;
    opts.batchesPerLayer = 0;
    % L2 regularization
    opts.l2Lambda = 0.1;
    
    if ~exist(opts.expDir, 'dir')
        mkdir(opts.expDir); 
    end
    
    batchOpts = {};
    batchOpts.train = {};
    batchOpts.test = {};
    
    batchOpts.train.nrOfExamples = length(trainMetaData.phonemes);
    % batchOpts.train.nrOfBatches = floor(batchOpts.train.nrOfExamples/opts.batchSize);
    batchOpts.train.phonemeFileNames = trainMetaData.phonemeFileNames;
    batchOpts.train.phonemeLabels = trainMetaData.phonemes;
    batchOpts.train.spectrogramHeight = trainMetaData.height;
    batchOpts.train.spectrogramWidth  = trainMetaData.width;
    batchOpts.train.spectrogramDepth  = trainMetaData.depth;
    batchOpts.train.average = trainMetaData.average;
    
    batchOpts.test.nrOfExamples = length(testMetaData.phonemes);
    % batchOpts.test.nrOfBatches = floor(batchOpts.train.nrOfExamples/opts.batchSize);
    batchOpts.test.phonemeFileNames = testMetaData.phonemeFileNames;
    batchOpts.test.phonemeLabels = testMetaData.phonemes;    
    batchOpts.test.spectrogramHeight = testMetaData.height;
    batchOpts.test.spectrogramWidth  = testMetaData.width;
    batchOpts.test.spectrogramDepth  = testMetaData.depth;
    batchOpts.test.average = testMetaData.average;
    
    batchOpts.train.phonemeFileNames = cellfun(@(x) strcat(inputPath, '\train\', x), batchOpts.train.phonemeFileNames, 'uniformOutput', false);
    batchOpts.test.phonemeFileNames  = cellfun(@(x) strcat(inputPath, '\test\', x),  batchOpts.test.phonemeFileNames, 'uniformOutput', false);
    
    % Load architecture
    if ~exist('net', 'var')
        net.layers = eval(architecture);
        net.architecture = architecture;
    end
    
    % Initialize network
    net = dec_cnn_initialize(net, opts);
    
    info = {};
    info.train.objective = [];
    info.train.numExamples = [];
    info.train.error = [];
    info.train.normalisedError = [];
    info.train.topFiveError = [];
    info.train.speed = [];
    info.val.objective = [];
    info.val.numExamples = [];
    info.val.error = [];
    info.val.normalisedError = [];
    info.val.topFiveError = [];
    info.val.speed = [];
    info.numBatches = 0;    
    
    info.phones = foldedPhones();
    info.phoneGroups = phoneGroupIndices();
    
    [net, info] = dec_cnn_train(net, info, opts, batchOpts);
end