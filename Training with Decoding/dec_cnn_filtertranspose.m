function filters = dec_cnn_filtertranspose( filters )
%% dec_cnn_filtertranspose 
% Author: Jordi Riemens
% 
%   Computes the "tranpose" of the given filters.
%   This is not an actual transpose. For a H x W x D x K input matrix,
%   the output is a H x W x K x D matrix (i.e., D and K dimensions permuted)
%   with the elements flipped in the H and W dimensions.

filters = flip(flip(permute(filters, [1 2 4 3]), 1), 2);

end

