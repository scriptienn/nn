function [net, info] = dec_cnn_train(net, info, opts, batchOpts)
%% dec_cnn_train
% Author: Thomas Churchman
%
% Run the training of a convolutional neural network. Prepares each epoch,
% calls the training and validation of the network, and finalizes each
% epoch.
%
% Each epoch, the network and other epoch information are stored in the
% experiment directory. Training will resume from the last saved epoch if
% the data is found in the experiment directory.
%
% Input:
% - net: the network to train.
% - info: initialized information structure.
% - opts: general training options.
% - batchOpts: training options and information related to batches,
%   separated on training and testing.
%
% Output:
% - net: the trained network.
% - info: information about the training and validation processes.
    
    for epoch=1:opts.numEpochs
        net2 = net;
        % Fast-forward to where we stopped
        modelPath = @(ep) fullfile(opts.expDir, sprintf('net-epoch-%d.mat', ep));
        modelFigPath = fullfile(opts.expDir, 'net-train.pdf');
        if opts.continue
            if exist(modelPath(epoch),'file') 
                continue; 
            end

            if epoch > 1
                fprintf('Resuming by loading epoch %d\n', epoch-1);
                load(modelPath(epoch-1), 'net', 'info', 'learningRate');
            end
        end
        
        % Generate batch permutations
        %trainPermutation = randperm(batchOpts.train.nrOfExamples);        
        %testPermutation  = randperm(batchOpts.test.nrOfExamples);
        
        trainPermutation = dec_cnn_resampleUniform( ...
            batchOpts.train.phonemeLabels, ...
            opts.examplesPerPhoneme);
        testPermutation  = dec_cnn_resampleUniform( ...
            batchOpts.test.phonemeLabels, ...
            opts.testPerPhoneme);
        
        
        trainPhonemeFileNames = batchOpts.train.phonemeFileNames(trainPermutation);
        testPhonemeFileNames  = batchOpts.test.phonemeFileNames(testPermutation);
        
        trainPhonemeLabels = batchOpts.train.phonemeLabels(trainPermutation);
        testPhonemeLabels  = batchOpts.test.phonemeLabels(testPermutation);
        
        % Initialize error information
        info.train.objective(end+1) = 0;
        info.train.numExamples(end+1) = 0;
        info.train.error(end+1) = 0;
        info.train.topFiveError(end+1) = 0;
        info.train.speed(end+1) = 0;
        
        info.val.objective(end+1) = 0;
        info.val.numExamples(end+1) = 0;
        info.val.error(end+1) = 0;
        info.val.topFiveError(end+1) = 0;
        info.val.speed(end+1) = 0;
        
        data.learningRate = opts.learningRate(min(epoch, numel(opts.learningRate)));
        if epoch > 1
            if learningRate ~= data.learningRate
                % RESET MOMENTUM
            end
        end
            
        %%%%%%%%%%%%%%%%
        % Run training %
        %%%%%%%%%%%%%%%%
        data.net = net;
        data.info = info;
        data.opts = opts;
        data.batchOpts = batchOpts;
        data.set = 'train';
        data.epoch = epoch;
        
        fprintf('Training: epoch %02d\n', epoch);
        data = dec_cnn_run( ...
            data, ...
            batchOpts.train.spectrogramHeight, ...
            batchOpts.train.spectrogramWidth, ...
            batchOpts.train.spectrogramDepth, ...
            numel(trainPermutation), ...
            opts.batchSize, ...
            trainPhonemeFileNames, ...
            trainPhonemeLabels);
        
        % Save training
        net = data.net;
        info = data.info;
        
        %%%%%%%%%%%%%%%%
        % Run testing  %
        %%%%%%%%%%%%%%%%
        data.net = net;
        data.info = info;
        data.opts = opts;
        data.batchOpts = batchOpts;
        data.set = 'test';
        data.epoch = epoch;
        
        fprintf('Testing: epoch %02d\n', epoch);
        data = dec_cnn_run( ...
            data, ...
            batchOpts.test.spectrogramHeight, ...
            batchOpts.test.spectrogramWidth, ...
            batchOpts.test.spectrogramDepth, ...
            numel(testPermutation), ...
            opts.batchSize, ...
            testPhonemeFileNames, ...
            testPhonemeLabels);
        
        % Save testing
        net = data.net;
        info = data.info;
        
        learningRate = data.learningRate;
        save(modelPath(epoch), 'net', 'info', 'learningRate');
        
        fprintf('=============================\n');
        
        %%%%%%%%%%%%%%%
        % Plot errors %
        %%%%%%%%%%%%%%%
        axisLimits = ...
            [ ...
                0 ...
                length(info.train.error) ...
                min(min(info.train.error./info.train.numExamples), min(info.val.error./info.train.numExamples)) ...
                max(max(info.train.error./info.train.numExamples), max(info.val.error./info.train.numExamples)) ...
            ];
        
        subplot(2,1,1);
        plotErrors(info.train, 'Train', axisLimits);
        
        subplot(2,1,2);
        plotErrors(info.val, 'Test', axisLimits);
        
        drawnow;
    end
    
end
