function [errorInfo, batchInfo] = dec_cnn_updateError(opts, errorInfo, phoneGroups, net, res, numExamples)
%% dec_cnn_updateError
% Author: Thomas Churchman 
%
% Input:
% - opts: The training options
% - errorInfo: The current vector of error information per epoch
% - phoneGroups: The phone to phone group mapping vector
% - net: The network the update errors for
% - res: The network result for the batch we're updating errors for
% - numExamples: The number of examples this batch consisted of
% 
% Output:
% - errorInfo: The updated vector of error information per epoch
% - batchInfo: The error information for the current batch

    % Cast to normal array (from GPU array if applicable)
    reconstructions = gather(res(end).x);
    data = gather(res(1).x);
    
    % Objective function unclear so commented out
    %errorInfo.objective(end) = errorInfo.objective(end) + sum(double(gather(res(end).x)));
    
    % Update number of examples seen this epoch
    errorInfo.numExamples(end) = errorInfo.numExamples(end) + numExamples;
    
    % Calculate Euclidean distance over all examples
    errors = sqrt(sum(sum(sum((reconstructions - data) .^ 2, 1), 2), 3)); % Euclidean error of reconstructions and data points, per point
    error = sum(errors);
    
    % Update error information
    batchInfo.error = error / numExamples;
    errorInfo.error(end) = errorInfo.error(end) + error;
    
    
    
    %{    
    switch opts.errorType
        case 'multiclass'
            [~,predictions] = sort(predictions, 3, 'descend');
            
            error = ~bsxfun(@eq, phoneGroups(predictions), reshape(phoneGroups(labels), 1, 1, 1, []));
            %error = ~bsxfun(@eq, predictions, reshape(labels, 1, 1, 1, []));
            
            % Count the number of errors
            numErrors = sum(sum(sum(error(:,:,1,:))))/n;
            
            % Count the top five number of errors
            numTopFiveErrors = sum(sum(sum(min(error(:,:,1:5,:),[],3))))/n;
            
            % Calculate batch error information
            batchInfo.error = numErrors / numExamples;
            batchInfo.topFiveError = numTopFiveErrors / numExamples;
            
            % Update epoch error information
            errorInfo.error(end) = errorInfo.error(end) + numErrors;
            errorInfo.topFiveError(end) = errorInfo.topFiveError(end) + numTopFiveErrors;
        case 'binary'
            error = bsxfun(@times, predictions, labels) < 0;
            
            % Count the number of errors
            numErrors = sum(error(:))/n;
            
            % Calculate batch error information
            batchInfo.error = numErrors / numExamples;
            
            % Update epoch error information
            errorInfo.error(end) = errorInfo.error(end) + numErrors;
    end
    %}
end