/*==========================================================
 * dec_cnn_run.cpp
 * 
 * Run the training of a convolutional neural network.
 *
 * Batch loading and network training are done on two 
 * separate threads.
 *
 * Author: Thomas Churchman, 2015
 *
 *========================================================*/

#include <iostream>
#include <math.h>
#include <boost/thread.hpp>
#include <Windows.h>
#include "mex.h"

using namespace std;

void _main();

/* Class to load phonemes batches. */
class BatchWorker
{
public:
	mxArray *spectra;
	mxArray *labels;
	BatchWorker(
		int numPhonemes,
		int height,
		int width,
		int depth,
		int batchSize,
		int batches,
		int batchNum,
		char **phonemeFileNames,
		mxArray **phonemeLabels);
	void loadFromDisk();
	void loadBatch();
	int batchSize();

private:
	int height;
	int width;
	int depth;
	int numPhonemes;
	float *spectraRaw;
	int startIdx;
	int endIdx;
	int curBatchSize;
	char **phonemeFileNames;
	mxArray **phonemeLabels;
};

/* BatchWorker constructor. */
BatchWorker::BatchWorker(
	int numPhonemes,
	int height,
	int width,
	int depth,
	int batchSize,
	int batches,
	int batchNum,
	char **phonemeFileNames,
	mxArray **phonemeLabels)
{
	this->height = height;
	this->width = width;
	this->depth = depth;
	this->numPhonemes = numPhonemes;
	this->phonemeFileNames = phonemeFileNames;
	this->phonemeLabels = phonemeLabels;
	
	/* Calculate batch indices */
	startIdx = (batchNum - 1)*batchSize;
	endIdx = batchNum*batchSize;
	if (endIdx >= numPhonemes)
	{
		endIdx = numPhonemes - 1;
	}
	curBatchSize = endIdx - startIdx;

	/* Allocate memory to hold the raw spectrograms */
	this->spectraRaw = new float[curBatchSize*height*width*depth];
}

/* Load a phoneme batch's spectrograms from disk (can/should be done asynchronously). */
void BatchWorker::loadFromDisk()
{
	for (int i = 0, idx = startIdx; idx < endIdx; ++i, ++idx)
	{	
		const char *fileName = phonemeFileNames[idx];

		int nElements = height * width * depth;
		FILE* pFile = fopen(fileName, "rb");
		fread(&this->spectraRaw[i*nElements], sizeof(*spectraRaw), nElements, pFile);
		fclose(pFile);
	}
}

/* Load and prepare a phoneme batch. 
   The method this->loadFromDisk should've been called (potentially asynchronously), 
   and must have finished, before this method is called. */
void BatchWorker::loadBatch()
{
	/* Initialize output */
	int dims = 4;
	int dimSize[] = { height, width, depth, curBatchSize };
	spectra = mxCreateNumericArray(dims, dimSize, mxSINGLE_CLASS, mxREAL);
	labels = mxCreateCellMatrix(curBatchSize, 1);
	//dims = 1;
	//dimSize = { curBatchSize };
	//labels = mxCreateNumericArray(dims, dimSize, mxSINGLE_CLASS, mxREAL);

	/* Copy spectrogram data */
	int nElements = height * width * depth * curBatchSize;
	float *matlabData = (float *)mxCalloc(nElements, sizeof(float));
	memcpy(matlabData, spectraRaw, sizeof(float) * nElements);

	/* Delete old data */
	delete [] spectraRaw;
	spectraRaw = NULL;

	/* Set spectrogram data */
	mxSetData(spectra, matlabData);

	/* Load labels */
	for (int i = 0, idx = startIdx; idx < endIdx; ++i, ++idx)
	{
		mxArray *phonemeLabel = mxDuplicateArray(phonemeLabels[idx]);
		mxSetCell(labels, i, phonemeLabel);
	}
}

int BatchWorker::batchSize()
{
	return this->curBatchSize;
}

/* Parses an integer from a scalar mxArray. */
int parseInt(const mxArray *scalarPtr)
{
	int i = (int) mxGetScalar(scalarPtr);
	return i;
}

/* Parses a char array (i.e., "string") from an mxArray. */
char * parseCharArray(const mxArray *charArrayPtr)
{
	char *chars = new char[mxGetN(charArrayPtr) + 1];
	mxGetString(charArrayPtr, chars, (int) mxGetN(charArrayPtr) + 1);

	return chars;
}

/* Parses a char array array (i.e., array of char arrays; "string array") 
   from an mxArray. */
char ** parseCharArrayArray(const mxArray *charArrayPtrsPtr)
{
	int numCharArrayPtrs = (int)mxGetNumberOfElements(charArrayPtrsPtr);

	char** charArrayPtrs;
	charArrayPtrs = new char * [numCharArrayPtrs];
	
	for (int i = 0; i < numCharArrayPtrs; ++i)
	{
		mxArray *charArrayPtr = mxGetCell(charArrayPtrsPtr, i);
		charArrayPtrs[i] = parseCharArray(charArrayPtr);
	}

	return charArrayPtrs;
}

/* Parses an mxArray array from an mxArray. */
mxArray ** parseMXArrayArray(const mxArray *mxArrayPtrsPtr)
{
	int numMXArrayPtrs = (int)mxGetNumberOfElements(mxArrayPtrsPtr);

	mxArray** mxArrayPtrs;
	mxArrayPtrs = new mxArray *[numMXArrayPtrs];

	for (int i = 0; i < numMXArrayPtrs; ++i)
	{
		mxArray *mxArrayPtr = mxGetCell(mxArrayPtrsPtr, i);
		mxArrayPtrs[i] = mxArrayPtr;
	}

	return mxArrayPtrs;
}

/* Code entry-point */
void mexFunction(
		int          	nlhs,
		mxArray      	*plhs[],
		int          	nrhs,
		const mxArray 	*prhs[]
		)
{
	/* Check for proper number of arguments. */
	if (nrhs != 8) 
	{
		mexErrMsgIdAndTxt("MATLAB:dec_cnn_run:nargin", 
		"Invalid number of input arguments into DEC_CNN_RUN.");
	} 
	else if (nlhs != 1) 
	{
		mexErrMsgIdAndTxt("MATLAB:dec_cnn_run:nargout",
		"DEC_CNN_RUN requires one output argument.");
	}

	
	/* Process the input arguments. */
	int arg = 0;
	//mxArray *data = mxDuplicateArray(prhs[arg++]);
	const mxArray *data = prhs[arg++];
	mxArray *dataCopy = mxDuplicateArray(data);
	int height = parseInt(prhs[arg++]);
	int width = parseInt(prhs[arg++]);
	int depth = parseInt(prhs[arg++]);
	int numPhonemes = parseInt(prhs[arg++]);
	int batchSize = parseInt(prhs[arg++]);
	int batches = (int)ceil(((double)numPhonemes) / batchSize);
	char **phonemeFileNames = parseCharArrayArray(prhs[arg++]);
	mxArray **phonemeLabels = parseMXArrayArray(prhs[arg++]);

	mexPrintf("Starting epoch processing.\nPhonemes: %d\nBatch size: %d\nNum batches: %d\n", numPhonemes, batchSize, batches);

	/* ====================================
	   ||       Train the network.       ||
	   ==================================== */
	/* Prefetch the first batch (asynchronously to be consistent). */
	BatchWorker *worker;
	worker = new BatchWorker(
		numPhonemes,
		height,
		width,
		depth,
		batchSize,
		batches,
		1,
		phonemeFileNames,
		phonemeLabels);
	boost::function<void()> threadFunction = boost::bind(&BatchWorker::loadFromDisk, worker);
	boost::thread *loadBatchThread;
	loadBatchThread = new boost::thread(threadFunction);

	for (int i = 0, batchNum = 1; i < batches; ++i, ++batchNum)
	{
		long int beforeTime = GetTickCount();
		/* Collect this batch's information. */
		loadBatchThread->join(); // Wait for other thread to join
		worker->loadBatch();
		mxArray *spectra = worker->spectra;
		mxArray *labels = worker->labels;
		long int afterWorkerSync = GetTickCount();
		int curBatchSize = worker->batchSize();

		/* Destruct worker and thread */
		delete worker;
		worker = NULL;
		delete loadBatchThread;
		loadBatchThread = NULL;

		if (batchNum < batches)
		{
			/* Not the last batch: create a new worker and
			   load next batch asynchronously. */
			worker = new BatchWorker(numPhonemes,
				height,
				width,
				depth,
				batchSize,
				batches,
				batchNum + 1,
				phonemeFileNames,
				phonemeLabels);
			threadFunction = boost::bind(&BatchWorker::loadFromDisk, worker);
			loadBatchThread = new boost::thread(threadFunction);
		}

		/* Call the dec_cnn_processBatch function and collect the result. */
		const int numOutputPointers = 1;
		mxArray *outputPointers[numOutputPointers];

		const int numInputPointers = 4;
		mxArray *batchNumPtr = mxCreateDoubleScalar((double)batchNum);
		mxArray *arguments[numInputPointers] = { dataCopy, batchNumPtr, spectra, labels };

		mexCallMATLAB(numOutputPointers, outputPointers, numInputPointers, arguments, "dec_cnn_processBatch");

		/* Destruct memory addressed by the old pointers. */
		mxDestroyArray(dataCopy);
		//mxDestroyArray(info);

		/* Point pointers to the new memory address. */
		dataCopy = outputPointers[0];
		//info = outputPointers[1];

		/* Destruct the input spectra, labels and batchNum. */
		mxDestroyArray(spectra);
		mxDestroyArray(labels);
		mxDestroyArray(batchNumPtr);

		long int afterProcessing = GetTickCount();

		mexPrintf("Batch %d/%d processing done. Effective batch load time: %d ms. Processing time: %d ms. Images per second: %f\n", 
			batchNum, 
			batches,
			(afterWorkerSync - beforeTime), 
			(afterProcessing - beforeTime), 
			((float) curBatchSize) / (afterProcessing - beforeTime) * 1000);
	}
	
	/* Output net and info. */
	plhs[0] = dataCopy;

	/* Destruct */
	//mxDestroyArray(dataCopy);

	for (int i = 0; i < numPhonemes; ++i)
	{
		delete [] phonemeFileNames[i];
		phonemeFileNames[i] = NULL;
	}
	delete [] phonemeFileNames;
	phonemeFileNames = NULL;

	//for (int i = 0; i < numPhonemes; ++i)
	//{
	//	mxDestroyArray(phonemeLabels[i]);
	//	phonemeLabels[i] = NULL;
	//}
	delete [] phonemeLabels;
	phonemeLabels = NULL;

	//mxDestroyArray(net);
	//mxDestroyArray(set);

	return;
}
