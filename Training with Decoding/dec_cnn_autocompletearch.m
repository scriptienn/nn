function layers = dec_cnn_autocompletearch( inLayers, inputSize, filterStdDev )
%% dec_cnn_autocompletearch
% Author: Jordi Riemens
%
%   Auto-completes an autoencoder architecture, by doing the following:
%   - for unpooling layers, fills in the parameters of the corresponding
%     pooling layer.
%   - for convt layers, fills in the parameters of the corresponding conv
%     layer, initialises filters and biases and deduces the 'size' parameter
%   - for conv layers, sets default parameters given a 'filterSize' parameter
%   
%   Takes as parameters inLayers (the layers struct to autocomplete), the
%   original input size (optional; used to set target sizes for unpooling layers)
%   and the standard deviation of (randomly initialised) elements of filter matrices.

if ~exist('filterStdDev', 'var')
    if ~exist('inputSize', 'var')
        inputSize = [201 16 1];
    end
    filterStdDev = 1/50;
end

layers = inLayers;
n = numel(layers);

for i=1:n
    switch layers{i}.type
        case 'unpool'
            iEnc = findCorrespondingEncodingLayer(layers, i, ...
                    @(x) strcmp(x.type, 'pool') && strcmp(x.method, 'max'), ...
                    @(x) strcmp(x.type, 'unpool'));
            layers{i}.encoderIndex = iEnc;
            lEnc = layers{iEnc};
            layers{i}.pool = lEnc.pool;
            layers{i}.stride = lEnc.stride;
            layers{i}.pad = lEnc.pad;
            layers{i}.size = findInputSizeAtLayer(layers, iEnc, inputSize);
            layers{i}.size = layers{i}.size(1:2);
        case 'conv'
            if isfield(layers{i}, 'filterSize')
                if ~isfield(layers{i}, 'filters')
                    layers{i}.filters = filterStdDev*randn(layers{i}.filterSize, 'single');
                end
                if ~isfield(layers{i}, 'biases')
                    layers{i}.biases = zeros(1, layers{i}.filterSize(4), 'single');
                end
                if ~isfield(layers{i}, 'stride')
                    layers{i}.stride = 1;
                end
                if ~isfield(layers{i}, 'pad')
                    layers{i}.pad = layers{i}.filterSize(1:2) - 1;
                    layers{i}.pad = [ceil(layers{i}.pad(1)/2), floor(layers{i}.pad(1)/2), ...
                             ceil(layers{i}.pad(2)/2), floor(layers{i}.pad(2)/2)];
                end
            else
                layers{i}.filterSize = size(layers{i}.filters);
            end
            if ~isfield(layers{i}, 'learnFilters')
                layers{i}.learnFilters = true;
            end
            if ~isfield(layers{i}, 'learnBiases')
                layers{i}.learnBiases = true;
            end
        case 'convt'
            iEnc = findCorrespondingEncodingLayer(layers, i, ...
                    @(x) strcmp(x.type, 'conv'), @(x) strcmp(x.type, 'convt'));
            layers{i}.encoderIndex = iEnc;
            lEnc = layers{iEnc};
            if ~isfield(layers{i}, 'learnFilters')
                layers{i}.learnFilters = false;
            end
            if ~isfield(layers{i}, 'learnBiases')
                layers{i}.learnBiases = true;
            end
            if ~isfield(layers{i}, 'filters') && layers{i}.learnFilters
                layers{i}.filters = filterStdDev*randn(lEnc.filterSize([1 2 4 3]), 'single');
            elseif ~isfield(layers{i}, 'filters') || ~layers{i}.learnFilters
                layers{i}.filters = dec_cnn_filtertranspose(lEnc.filters);
            end
            if ~isfield(layers{i}, 'biases')
                layers{i}.biases = zeros(1, size(layers{i}.filters, 4), 'single');
            end
            layers{i}.stride = lEnc.stride;
            layers{i}.pad = lEnc.pad;
            layers{i}.filterSize = size(layers{i}.filters);
    end
end

end


function inputSize = findInputSizeAtLayer(layers, layerIndex, originalInputSize)

inputSize = originalInputSize;
for i=1:layerIndex-1
    if strcmp(layers{i}.type, 'conv') || strcmp(layers{i}.type, 'convt')
        if numel(layers{i}.pad) == 1
            pad = [2*layers{i}.pad, 2*layers{i}.pad];
        elseif numel(layers{i}.pad) == 4
            pad = [sum(layers{i}.pad(1:2)), sum(layers{i}.pad(3:4))];
        else
            error('invalid pad');
        end
        inputSize(1:2) = floor((inputSize(1:2) - layers{i}.filterSize(1:2) + pad) ./ layers{i}.stride) + 1;
        
        if layers{i}.filterSize(3) ~= inputSize(3)
            error('invalid filters');
        end
        inputSize(3) = layers{i}.filterSize(4);
    elseif strcmp(layers{i}.type, 'pool')
        if numel(layers{i}.pad) == 1
            pad = [2*layers{i}.pad, 2*layers{i}.pad];
        elseif numel(layers{i}.pad) == 4
            pad = [sum(layers{i}.pad(1:2)), sum(layers{i}.pad(3:4))];
        else
            error('invalid pad');
        end
        if numel(layers{i}.pool) == 1
            pool = [layers{i}.pool layers{i}.pool];
        elseif numel(layers{i}.pool) == 2
            pool = layers{i}.pool;
        else
            error('invalid l.pool')
        end
        inputSize(1:2) = floor((inputSize(1:2) - pool + pad) ./ layers{i}.stride) + 1;
    elseif strcmp(layers{i}.type, 'unpool')
        inputSize(1:2) = layers{i}.size(1:2);
    end
end


end



function index = findCorrespondingEncodingLayer(layers, layerIndex, isEncodingLayer, isDecodingLayer)
% Given the index of a decoding layer in the given network layers,
% and function handles to see if the layer is an encoding or decoding layer
% of the right type, this function returns
% the index of the corresponding encoding layer earlier in the network.

currentLevel = 1;
index = -1;
for i=layerIndex-1:-1:1
if isEncodingLayer(layers{i})
  currentLevel = currentLevel - 1;
elseif isDecodingLayer(layers{i})
  currentLevel = currentLevel + 1;
end

if currentLevel == 0
  index = i;
  break
end
end

if index == -1
error('No encoding layer found for decoding layer %d', layerIndex);
end
end