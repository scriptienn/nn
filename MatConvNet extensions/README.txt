The following files contain the following extensions to the original MatConvNet:
- max-pooling layers now also save the switches (locations of maximum input value inside the pooling region) in a second output matrix. See vl_nnpool.m for more documentation.
- a new unpooling layer, which performs switch upsampling to "invert" max pooling, given the output and switches from max pooling. See vl_nnunpool.m for more documentation.
  blind upsampling is also possible, by pasting the contents of the "Blind upsampling" folder in the same <matconvnetpath>\matlab folder.

To use these extensions, copy everything in this directory to <matconvnetpath>\matlab and replace the original MatConvNet files. Then, recompile MatConvNet using the provided vl_compilenn.m script.

Note: if you want to revert to the original MatConvNet files, you can use the provided .bak backup files (taken from the matconvnet-1.0-beta8 download) and recompile with those.