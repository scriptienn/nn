% VL_NNPOOL  CNN pooling
%    Y = VL_NNPOOL(X, POOL) applies the pooling operator to all
%    channels of the data X using a square filter of size POOL. X is a
%    SINGLE array of dimension H x W x D x N where (H,W) are the
%    height and width of the map stack, D is the image depth (number
%    of feature channels) and N the number of of images in the stack.
%
%    Y = VL_NNPOOL(X, [POOLY, POOLX]) uses a rectangular filter of
%    height POOLY and width POOLX.
%
%    DZDX = VL_NNPOOL(X, POOL, DZDY) computes the derivatives of
%    the network output Z w.r.t. the data X given the derivative DZDY
%    w.r.t the max-pooling output Y.
%    
%    [Y, SWITCHES] = VL_NNPOOL(X, POOL) has a second output SWITCHES,
%    which contains the zero-based indices of the maximum elements
%    within the current dimension of the current data element.
%    In other words, if currX = X(:,:,d,n), then it is a zero-based
%    index inside currX(:), the vectorised form of currX.
%    These indices tick through the column before going to the next one.
%    The matrix SWITCHES has the same dimensions as output matrix Y.
%
%    VL_NNPOOL(..., 'option', value, ...) takes the following options:
%
%    Stride:: [1]
%      The output stride (downsampling factor). It can be either a
%      scalar for isotropic downsampling or a vector [STRIDEY
%      STRIDEX].
%
%    Pad:: [0]
%      The amount of input padding. Input images are padded with zeros
%      by this number of pixels on all sides before the convolution is
%      computed. It can also be a vector [TOP BOTTOM LEFT RIGHT] to
%      specify a different amount of padding in each direction. The
%      size of the pooling filter has to exceed the padding.
%
%    Method:: ['max']
%      Specify method of pooling. It can be either 'max' (retain max value
%      over the pooling region per channel) or 'avg' (compute the average
%      value over the pooling region per channel).
%
%    The pooling window must be not larger than the padded image, i.e.
%
%      1 <= POOLY <= HEIGHT + (PADTOP + PADBOTTOM),
%      1 <= POOLX <= WIDTH + (PADLEFT + PADRIGHT).
%
%    The output a is a SINGLE array of dimension YH x YW x K x N of N
%    images with K channels and size:
%
%      YH = floor((H + (PADTOP+PADBOTTOM) - POOLY)/STRIDEY) + 1,
%      YW = floor((W + (PADLEFT+PADRIGHT) - POOLX)/STRIDEX) + 1.
%
%    The derivative DZDY has the same dimension of the output Y and
%    the derivative DZDX has the same dimension as the input X.
%    SWITCHES has the same dimension as the output Y.



