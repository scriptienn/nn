% VL_NNUNPOOL  CNN unpooling
%    Y = VL_NNUNPOOL(X, POOL, SWITCHES) performs switch unpooling on all
%    channels of the data X using a square filter of size POOL. X is a
%    SINGLE array of dimension H x W x D x N where (H,W) are the height
%    and width of the map stack, D is the image depth (number of feature
%    channels) and N the number of images in the stack.
%    SWITCHES contain the locations of maximum response within the pooling
%    regions, as returned by the VL_NNPOOL to be reversed.
%    
%    Y = VL_NNUNPOOL(X, [POOLY, POOLX], SWITCHES) uses a rectangular
%    filter of height POOLY and width POOLX.
%    
%    DZDX = VL_NNUNPOOL(X, POOL, SWITCHES, DZDY) computes the derivatives
%    of the network output Z w.r.t. the data X, given the derivative DZDY
%    w.r.t. the unpooled output Y.
%    
%    VL_NNUNPOOL(..., 'option', value, ...) takes the following options:
%    
%    Stride:: [1]
%      The output stride (subsampling factor) used by VL_NNPOOL. It can be 
%      either a scalar for isotropic subsampling or a vector 
%      [STRIDEY STRIDEX].
%
%    Pad:: [0]
%      The amount of input padding used by VL_NNPOOL. Input images were padded 
%      with zeros by this number of pixels on all sides before the convolution 
%      was computed. It can also be a vector [TOP BOTTOM LEFT RIGHT] to
%      specify a different amount of padding in each direction. The
%      size of the pooling filter has to exceed the padding.
%    
%    Size:: [-1 -1]
%      The desired output size of this layer. Only relevant when stride > 1
%      in any direction. Given pooled images of size H x W (per channel),
%      the output size YH x YW defaults to the following when settings [-1 -1] are supplied:
%        YH = (H - 1) * STRIDEY + POOLY - (PADTOP+PADBOTTOM)
%        YW = (W - 1) * STRIDEX + POOLX - (PADLEFT+PADRIGHT).
%      Note that size parameters [YH YW] are only valid if an YH x YW image
%      would be reduced to an H x W image again by VL_NNPOOL (i.e.,
%      the size transformations for pooling and unpooling should be each other's inverse)
%
%    The pooling window must be not larger than the padded image, i.e.
%
%      1 <= POOLY <= HEIGHT + (PADTOP + PADBOTTOM),
%      1 <= POOLX <= WIDTH + (PADLEFT + PADRIGHT).
%
%    The output a is a SINGLE array of dimension YH x YW x D x N of N
%    images with D channels and size [YH HW] as indicated by the size parameter.
%
%    The derivative DZDY has the same dimension of the output Y and
%    the derivative DZDX has the same dimension as the input X.
%    SWITCHES has the same dimension as the input X.
