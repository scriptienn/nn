/** @file unpooling.hpp
 ** @brief Max pooling filters
 ** @author Andrea Vedaldi
 ** @author Karel Lenc
 **/

/*
Copyright (C) 2014 Andrea Vedaldi and Karel Lenc.
All rights reserved.

This file is part of the VLFeat library and is made available under
the terms of the BSD license (see the COPYING file).
*/

#ifndef VL_NNUNPOOLING_H
#define VL_NNUNPOOLING_H

template<typename T>
void unpooling_cpu(T* unpooled,
                   T const* data,
                   T const* switches,
                   size_t dataHeight,
                   size_t dataWidth,
                   size_t outputHeight,
                   size_t outputWidth,
                   size_t depth) ;

template<typename T>
void unpoolingBackward_cpu(T* dzdx,
                           T const* switches,
                           T const* dzdy,
                           size_t dataHeight,
                           size_t dataWidth,
                           size_t outputHeight,
                           size_t outputWidth,
                           size_t depth) ;
/*
#ifdef ENABLE_GPU
template<typename T>
void unpooling_gpu(T* unpooled,
                   T const* data,
                   T const* switches,
                   size_t dataHeight,
                   size_t dataWidth,
                   size_t outputHeight,
                   size_t outputWidth,
                   size_t depth) ;

template<typename T>
void unpoolingBackward_gpu(T* dzdx,
                           T const* switches,
                           T const* dzdy,
                           size_t dataHeight,
                           size_t dataWidth,
                           size_t outputHeight,
                           size_t outputWidth,
                           size_t depth) ;
#endif */

#endif /* defined(VL_NNUNPOOLING_H) */
