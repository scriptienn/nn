/** @file pooling_gpu.cu
 ** @brief Max pooling filters (GPU)
 ** @author Andrea Vedaldi
 ** @author Karel Lenc
 **/

/*
Copyright (C) 2014 Andrea Vedaldi and Karel Lenc.
All rights reserved.

This file is part of the VLFeat library and is made available under
the terms of the BSD license (see the COPYING file).
*/

#include "gpu.hpp"
#include "pooling.hpp"

#include <assert.h>
#include <float.h>
#include <sm_20_atomic_functions.h>

/* ---------------------------------------------------------------- */
/*                                                  unpooling (GPU) */
/* ---------------------------------------------------------------- */

template<typename T>
__global__ void unpooling_gpu_kernel
(T* unpooled,
 const T* data,
 const T* switches,
 const int dataHeight,
 const int dataWidth,
 const int outputHeight,
 const int outputWidth,
 const int depth)
{
  int depthIndex = threadIdx.x + blockIdx.x * blockDim.x;
  
  if (depthIndex < depth) {
    data += depthIndex*dataWidth*dataHeight ;
    switches += depthIndex*dataWidth*dataHeight ;
    unpooled += depthIndex*outputWidth*outputHeight ;
    
    for (int i = 0; i < dataWidth*dataHeight; ++i) {
      unpooled[(int)switches[i]] = data[i];
    }
  }
}


template<typename T>
void unpooling_gpu(T* unpooled,
                   T const* data,
                   T const* switches,
                   size_t dataHeight,
                   size_t dataWidth,
                   size_t outputHeight,
                   size_t outputWidth,
                   size_t depth)
{
  maxPooling_gpu_kernel<T>
  <<< divideUpwards(depth, VL_CUDA_NUM_THREADS), VL_CUDA_NUM_THREADS >>>
  (unpooled, data, switches,
   dataWidth, dataHeight,
   outputWidth, outputHeight,
   windowWidth, windowHeight,
   depth);
  if (cudaGetLastError() != cudaSuccess) {
    std::cout
    <<"unpooling_gpu_kernel error ("
    <<cudaGetErrorString(cudaGetLastError())
    <<")"<<std::endl ;
  }
}

template
void unpooling_gpu<float>(float* pooled,
                          float const* data,
                          float const* switches,
                          size_t dataHeight,
                          size_t dataWidth,
                          size_t outputHeight,
                          size_t outputWidth,
                          size_t depth) ;

template
void unpooling_gpu<double>(double* pooled,
                           double const* data,
                           double const* switches,
                           size_t dataHeight,
                           size_t dataWidth,
                           size_t outputHeight,
                           size_t outputWidth,
                           size_t depth) ;

/* ---------------------------------------------------------------- */
/*                                          unpoolingBackward (GPU) */
/* ---------------------------------------------------------------- */




template<typename T>
__global__ void unpoolingBackward_gpu_kernel
(T* dzdx,
 const T* switches,
 const T* dzdy,
 const int dataHeight,
 const int dataWidth,
 const int outputHeight,
 const int outputWidth,
 const int depth)
{
  int depthIndex = threadIdx.x + blockIdx.x * blockDim.x;
  
  if (depthIndex < depth) {
    switches += depthIndex*dataWidth*dataHeight ;
    dzdx += depthIndex*dataWidth*dataHeight ;
    dzdy += depthIndex*outputWidth*outputHeight ;
    
    for(int i = 0; i < dataWidth*dataHeight; ++i) {
      dzdx[i] += dzdy[(int)switches[i]];
    }
  }
}


template<typename T>
void unpoolingBackward_gpu(T* dzdx,
                         T const* switches,
                         T const* dzdy,
                         size_t dataHeight,
                         size_t dataWidth,
                         size_t outputHeight,
                         size_t outputWidth,
                         size_t depth)
{ 
  maxPoolingBackward_gpu_kernel<T>
  <<< divideUpwards(depth, VL_CUDA_NUM_THREADS), VL_CUDA_NUM_THREADS >>>
  (dzdx,
   switches, dzdy,
   dataHeight, dataWidth,
   outputWidth, outputHeight,
   depth);
  if (cudaGetLastError() != cudaSuccess) {
    std::cout
    <<"unpoolingBackward_gpu_kernel error ("
    <<cudaGetErrorString(cudaGetLastError())
    <<")"<<std::endl ;
  }
}

template
void unpoolingBackward_gpu<float>(float* dzdx,
                                  float const* switches,
                                  float const* dzdy,
                                  size_t dataHeight,
                                  size_t dataWidth,
                                  size_t outputHeight,
                                  size_t outputWidth,
                                  size_t depth) ;

#if 0
template
void unpoolingBackward_gpu<double>(double* dzdx,
                                   double const* switches,
                                   double const* dzdy,
                                   size_t dataHeight,
                                   size_t dataWidth,
                                   size_t outputHeight,
                                   size_t outputWidth,
                                   size_t depth) ;
#endif
