/** @file unpooling.cpp
 ** @brief Max pooling filters (CPU)
 ** @author Andrea Vedaldi
 ** @author Karel Lenc
 **/

/*
Copyright (C) 2014 Andrea Vedaldi and Karel Lenc.
All rights reserved.

This file is part of the VLFeat library and is made available under
the terms of the BSD license (see the COPYING file).
*/

#include "unpooling.hpp"

#include<iostream>

/* ---------------------------------------------------------------- */
/*                                            switchUnpooling (CPU) */
/* ---------------------------------------------------------------- */

template<typename T>
void unpooling_cpu(T* unpooled,
                   T const* data,
                   T const* switches,
                   size_t dataHeight,
                   size_t dataWidth,
                   size_t outputHeight,
                   size_t outputWidth,
                   size_t depth)
{
  for (int z = 0; z < depth; ++z) {
    for (int i = 0; i < dataWidth*dataHeight; ++i) {
      unpooled[(int)switches[i]] = data[i];
    }
    
    data += dataWidth*dataHeight ;
    switches += dataWidth*dataHeight ;
    unpooled += outputWidth*outputHeight ;
  }
}

template
void unpooling_cpu<float>(float* unpooled,
                          float const* data,
                          float const* switches,
                          size_t dataHeight,
                          size_t dataWidth,
                          size_t outputHeight,
                          size_t outputWidth,
                          size_t depth) ;

template
void unpooling_cpu<double>(double* unpooled,
                           double const* data,
                           double const* switches,
                           size_t dataHeight,
                           size_t dataWidth,
                           size_t outputHeight,
                           size_t outputWidth,
                           size_t depth) ;


/* ---------------------------------------------------------------- */
/*                                    switchUnpoolingBackward (CPU) */
/* ---------------------------------------------------------------- */

/* 
 assume the output array to be cleared or otherwise
 properly initialised: accumulates the derivative
 */
template<typename T>
void unpoolingBackward_cpu(T* dzdx,
                           T const* switches,
                           T const* dzdy,
                           size_t dataHeight,
                           size_t dataWidth,
                           size_t outputHeight,
                           size_t outputWidth,
                           size_t depth)
{
  for(int z = 0; z < depth; ++z) {
    for(int i = 0; i < dataWidth*dataHeight; ++i) {
      dzdx[i] += dzdy[(int)switches[i]];
    }
    
    switches += dataWidth*dataHeight ;
    dzdx += dataWidth*dataHeight ;
    dzdy += outputWidth*outputHeight ;
  }
}

template
void unpoolingBackward_cpu<float>(float* dzdx,
                                  float const* switches,
                                  float const* dzdy,
                                  size_t dataHeight,
                                  size_t dataWidth,
                                  size_t outputHeight,
                                  size_t outputWidth,
                                  size_t depth) ;

template
void unpoolingBackward_cpu<double>(double* dzdx,
                                   double const* switches,
                                   double const* dzdy,
                                   size_t dataHeight,
                                   size_t dataWidth,
                                   size_t outputHeight,
                                   size_t outputWidth,
                                   size_t depth) ;

