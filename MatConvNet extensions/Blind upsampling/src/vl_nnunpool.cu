/** @file vl_nnpool.cu
 ** @brief Pooling block
 ** @author Andrea Vedaldi
 ** @author Karel Lenc
 **/

/*
Copyright (C) 2014 Andrea Vedaldi and Karel Lenc.
All rights reserved.

This file is part of the VLFeat library and is made available under
the terms of the BSD license (see the COPYING file).
*/

#include "bits/mexutils.h"
#include "bits/nnhelper.h"
#include "bits/unpooling.hpp"

#include <assert.h>

/* option codes */
enum {
  opt_stride = 0,
  opt_pad,
  opt_size,
  opt_verbose
} ;

/* options */
vlmxOption  options [] = {
  {"Stride",           1,   opt_stride            },
  {"Pad",              1,   opt_pad               },
  {"Size",             1,   opt_size              }, //size of unpooled output
  {"Verbose",          0,   opt_verbose           },
  {0,                  0,   0                     }
} ;

enum {
  IN_DATA = 0, IN_POOL_SIZE, IN_SWITCHES, IN_DEROUTPUT, IN_END
} ;

enum {
  OUT_RESULT = 0, OUT_END
} ;

void mexFunction(int nout, mxArray *out[],
                 int nin, mxArray const *in[])
{
  /* inputs */
  PackedData data ;
  PackedData derOutput ;
  PackedData switches ;

  /* outputs */
  PackedData output ;
  PackedData derData  ;
  PackedDataGeometry outputGeom ;
  PackedDataGeometry derDataGeom  ;

  int poolWidth ;
  int poolHeight ;
  int strideX = 1 ;
  int strideY = 1 ;
  int padLeft = 0 ;
  int padRight = 0 ;
  int padTop = 0 ;
  int padBottom = 0 ;
  
  int outWidth = -1 ;
  int outHeight = -1 ;

#ifdef ENABLE_GPU
  bool gpuMode = false ;
#else
  bool const gpuMode = false ;
#endif
  bool backMode = false ;

  int verbosity = 0 ;
  int opt ;
  int next = IN_END ;
  mxArray const *optarg ;
  VlEnumerator *pair ;

  packed_data_init_empty(&data) ;
  packed_data_init_empty(&derOutput) ;
  packed_data_init_empty(&switches) ;
  packed_data_init_empty(&output) ;
  packed_data_init_empty(&derData) ;

  /* -------------------------------------------------------------- */
  /*                                            Check the arguments */
  /* -------------------------------------------------------------- */

  /* Throw an error if the input is not a GPU array. */
  if (nin < 3) {
    mexErrMsgTxt("The arguments are less than three.") ;
  }

  if (nin > 3 && vlmxIsString(in[3],-1)) {
    next = 3 ;
    backMode = 0 ;
  } else {
    backMode = (nin >= 4) ;
  }

  while ((opt = vlmxNextOption (in, nin, options, &next, &optarg)) >= 0) {
    switch (opt) {
      case opt_verbose :
        ++ verbosity ;
        break ;

      case opt_stride :
        if (!vlmxIsPlainMatrix(optarg,-1,-1)) {
          mexErrMsgTxt("STRIDE is not a plain matrix.") ;
        }
        switch (mxGetNumberOfElements(optarg)) {
          case 1:
            strideY = (int)mxGetPr(optarg)[0] ;
            strideX = strideY ;
            break ;
          case 2:
            strideY = (int)mxGetPr(optarg)[0] ;
            strideX = (int)mxGetPr(optarg)[1] ;
            break ;
          default:
            mexErrMsgTxt("STRIDE has neither one nor two elements.") ;
        }
        break ;

      case opt_pad :
        if (!vlmxIsPlainMatrix(optarg,-1,-1)) {
          mexErrMsgTxt("PAD is not a plain matrix.") ;
        }
        switch (mxGetNumberOfElements(optarg)) {
          case 1:
            padLeft = (int)mxGetPr(optarg)[0] ;
            padRight = padLeft ;
            padTop = padLeft ;
            padBottom = padLeft ;
            break ;
          case 4:
            padTop = (int)mxGetPr(optarg)[0] ;
            padBottom = (int)mxGetPr(optarg)[1] ;
            padLeft = (int)mxGetPr(optarg)[2] ;
            padRight = (int)mxGetPr(optarg)[3] ;
            break ;
          default:
            mexErrMsgTxt("PAD has neither one nor four elements.") ;
        }
        break;
      
      case opt_size :
        if (!vlmxIsPlainMatrix(optarg,-1,-1)) {
          mexErrMsgTxt("SIZE is not a plain matrix.") ;
        }
        if (mxGetNumberOfElements(optarg) != 2) {
          mexErrMsgTxt("SIZE does not have two elements.") ;
        }
        
        outHeight = (int)mxGetPr(optarg)[0] ;
        outWidth = (int)mxGetPr(optarg)[1] ;
        break;
      default: break ;
    }
  }

  packed_data_init_with_array(&data, in[IN_DATA]) ;
  packed_data_init_with_array(&switches, in[IN_SWITCHES]) ;
  if (backMode) { packed_data_init_with_array(&derOutput, in[IN_DEROUTPUT]) ; }

#if ENABLE_GPU
  gpuMode = (data.mode == matlabGpuArrayWrapper) ;
  if (gpuMode) {
    mxInitGPU() ;
  }
#endif

  /* check GPU/data class consistency */
  if (gpuMode && (derOutput.mode != matlabGpuArrayWrapper && backMode)) {
    mexErrMsgTxt("DATA is a GPU array but DEROUTPUT is not.") ;
  }
  if (data.geom.classID != mxSINGLE_CLASS) {
    mexErrMsgTxt("DATA is not of class SINGLE.");
  }
  if (backMode && (derOutput.geom.classID != mxSINGLE_CLASS)) {
    mexErrMsgTxt("DEROUTPUT is not of class SINGLE.");
  }

  if (!vlmxIsPlainMatrix(in[IN_POOL_SIZE],-1,-1)) {
    mexErrMsgTxt("POOL_SIZE is not a plain matrix.") ;
  }
  switch (mxGetNumberOfElements(in[IN_POOL_SIZE])) {
    case 1:
      poolHeight = mxGetPr(in[IN_POOL_SIZE])[0] ;
      poolWidth = poolHeight ;
      break ;
    case 2:
      poolHeight = mxGetPr(in[IN_POOL_SIZE])[0] ;
      poolWidth = mxGetPr(in[IN_POOL_SIZE])[1] ;
      break ;
    default:
      mexErrMsgTxt("POOL_SIZE has neither one nor two elements.") ;
  }

  if (strideX < 1 || strideY < 1) {
    mexErrMsgTxt("At least one element of STRIDE is smaller than one.") ;
  }
  
  if (outHeight != -1 && outWidth != -1) {
    if (outHeight <= 0 || outWidth <= 0) {
      mexErrMsgTxt("SIZE can only have positive elements.");
    }
    
    if ((outHeight + (padTop+padBottom) - poolHeight)/strideY + 1 != data.geom.height ||
        (outWidth + (padLeft+padRight) - poolWidth)/strideX + 1 != data.geom.width) {
      mexErrMsgTxt("SIZE parameters not valid for this data (dimensionality mismatch).");
    }
  } else {
    outHeight = (data.geom.height - 1) * strideY + poolHeight - (padTop+padBottom) ;
    outWidth = (data.geom.width - 1) * strideX + poolWidth - (padLeft+padRight) ;
    
    //N.B. Inverse of:
    //data.geom.height = (outHeight + (padTop+padBottom) - poolHeight)/strideY + 1 ;
    //data.geom.width = (outWidth + (padLeft+padRight) - poolWidth)/strideX + 1 ;
  }

  packed_data_geom_init(&outputGeom,
                        mxSINGLE_CLASS,
                        outHeight,
                        outWidth,
                        data.geom.depth,
                        data.geom.size) ;

  derDataGeom = data.geom ;

  if (verbosity > 0) {
    mexPrintf("vl_nnunpool: mode %s; %s\n", gpuMode?"gpu":"cpu", backMode?"backward":"forward") ;
    mexPrintf("vl_nnunpool: stride: [%d %d], pad: [%d %d %d %d]\n",
              strideY, strideX,
              padTop, padBottom, padLeft, padRight) ;
    packed_data_geom_display(&data.geom, "vl_nnunpool: data") ;
    mexPrintf("vl_nnunpool: pooling: %d x %d\n", poolHeight, poolWidth);
    if (backMode) {
      packed_data_geom_display(&derOutput.geom, "vl_nnunpool: derOutput") ;
      packed_data_geom_display(&derDataGeom, "vl_nnunpool: derData") ;
    } else {
      packed_data_geom_display(&outputGeom, "vl_nnunpool: output") ;
    }
  }
  
  if (data.geom.height != switches.geom.height ||
      data.geom.width != switches.geom.width ||
      data.geom.depth != switches.geom.depth ||
      data.geom.size != switches.geom.size)
  {
    mexErrMsgTxt("SWITCHES dimensions are incompatible with X.") ;
  }

  if (backMode) {
    if (derOutput.geom.height != outputGeom.height ||
        derOutput.geom.width != outputGeom.width ||
        derOutput.geom.depth != outputGeom.depth ||
        derOutput.geom.size != outputGeom.size)
    {
      mexErrMsgTxt("DEROUTPUT dimensions are incompatible with X and POOL.") ;
    }
  }

  if (poolHeight == 0 || poolWidth == 0) {
    mexErrMsgTxt("A dimension of the pooling SIZE is void.") ;
  }

  if (strideX == 0 || strideY == 0) {
    mexErrMsgTxt("An element of STRIDE is zero.") ;
  }

  if (padLeft < 0 ||
      padRight < 0 ||
      padTop < 0 ||
      padBottom < 0) {
    mexErrMsgTxt("An element of PAD is negative.") ;
  }

  if (padLeft >= poolWidth ||
      padRight >= poolWidth ||
      padTop >= poolHeight  ||
      padBottom >= poolHeight) {
    mexErrMsgTxt("A padding value is larger or equal than the size of the pooling window.") ;
  }

  /* -------------------------------------------------------------- */
  /*                                                    Do the work */
  /* -------------------------------------------------------------- */

  if (!backMode) {
    packed_data_init_with_geom(&output, gpuMode, outputGeom, false, true, 0) ;
  } else {
    packed_data_init_with_geom(&derData, gpuMode, derDataGeom, false, true, 0) ;
  }

  if (backMode) {
    /* ---------------------------------------------------------- */
    /*                                              Backward mode */
    /* ---------------------------------------------------------- */
    if (gpuMode) {
#ifdef ENABLE_GPU/*
      unpoolingBackward_gpu<float>(derData.memory,
                                   switches.memory,
                                   derOutput.memory,
                                   data.geom.height, data.geom.width,
                                   outHeight, outWidth,
                                   data.geom.depth * data.geom.size) ;
#else*/
      assert(false) ;
#endif
    } else {
      unpoolingBackward_cpu(derData.memory,
                         derOutput.memory,
                         outHeight,
                         outWidth,
                         data.geom.depth * data.geom.size,
                         poolHeight,
                         poolWidth,
                         strideY,
                         strideX,
                         padTop,
                         padBottom,
                         padLeft,
                         padRight) ;
    }
  } else {
    /* ---------------------------------------------------------- */
    /*                                               Forward mode */
    /* ---------------------------------------------------------- */
    if (gpuMode) {
#ifdef ENABLE_GPU/*
      unpooling_gpu<float>(output.memory,
                           data.memory,
                           switches.memory,
                           data.geom.height, data.geom.width,
                           outHeight, outWidth,
                           data.geom.depth * data.geom.size) ;
#else*/
      assert(false) ;
#endif
    } else {
      unpooling_cpu(output.memory,
                         data.memory,
                         outHeight,
                         outWidth,
                         data.geom.depth * data.geom.size,
                         poolHeight,
                         poolWidth,
                         strideY,
                         strideX,
                         padTop,
                         padBottom,
                         padLeft,
                         padRight) ;
    }
  }

  /* -------------------------------------------------------------- */
  /*                                                        Cleanup */
  /* -------------------------------------------------------------- */

  packed_data_deinit(&data) ;
  packed_data_deinit(&switches) ;
  if (backMode) {
    packed_data_deinit(&derOutput) ;
    out[OUT_RESULT] = packed_data_deinit_extracting_array(&derData) ;
  } else {
    out[OUT_RESULT] = packed_data_deinit_extracting_array(&output) ;
  }
}
