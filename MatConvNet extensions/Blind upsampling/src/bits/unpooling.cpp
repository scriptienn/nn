/** @file unpooling.cpp
 ** @brief Max pooling filters (CPU)
 ** @author Andrea Vedaldi
 ** @author Karel Lenc
 **/

/*
Copyright (C) 2014 Andrea Vedaldi and Karel Lenc.
All rights reserved.

This file is part of the VLFeat library and is made available under
the terms of the BSD license (see the COPYING file).
*/

#include "unpooling.hpp"
#include <algorithm>
#include <iostream>

/* ---------------------------------------------------------------- */
/*                                            switchUnpooling (CPU) */
/* ---------------------------------------------------------------- */

template<typename T>
void unpooling_cpu(T* unpooled,
                 T const* data,
                 size_t width,
                 size_t height,
                 size_t depth,
                 size_t windowWidth,
                 size_t windowHeight,
                 size_t strideX,
                 size_t strideY,
                 size_t padLeft,
                 size_t padRight,
                 size_t padTop,
                 size_t padBottom)
{
  int pooledWidth = (width + (padLeft + padRight) - windowWidth)/strideX + 1 ;
  int pooledHeight = (height + (padTop + padBottom) - windowHeight)/strideY + 1 ;
  int* timesUpdated = new int[width*height];
  for (int i = 0; i < width*height; ++i) {
    timesUpdated[i] = 0;
  }
  
  for (int z = 0; z < depth; ++z) {
    for (int y = 0; y < pooledHeight; ++y) {
      for (int x = 0; x < pooledWidth; ++x) {
        int x1 = x * (signed)strideX - (signed)padLeft ;
        int y1 = y * (signed)strideY - (signed)padTop ;
        int x2 = std::min(x1 + windowWidth, width) ;
        int y2 = std::min(y1 + windowHeight, height) ;
        x1 = std::max(x1, 0) ;
        y1 = std::max(y1, 0) ;
        
        for (int v = y1 ; v < y2 ; ++v) {
          for (int u = x1 ; u < x2 ; ++u) {
            unpooled[v * width + u] += data[y * pooledWidth + x];
            timesUpdated[v * width + u] += 1;
          }
        }
      }
    }
    for (int i = 0; i < width*height; ++i) {
      unpooled[i] = timesUpdated[i] > 0 ? unpooled[i] / timesUpdated[i] : 0;
      timesUpdated[i] = 0;
    }
    data += pooledWidth*pooledHeight ;
    unpooled += width*height ;
  }
  
  delete [] timesUpdated;
}

template
void unpooling_cpu<float>(float* unpooled,
                 float const* data,
                 size_t width,
                 size_t height,
                 size_t depth,
                 size_t windowWidth,
                 size_t windowHeight,
                 size_t strideX,
                 size_t strideY,
                 size_t padLeft,
                 size_t padRight,
                 size_t padTop,
                 size_t padBottom) ;

template
void unpooling_cpu<double>(double* unpooled,
                 double const* data,
                 size_t width,
                 size_t height,
                 size_t depth,
                 size_t windowWidth,
                 size_t windowHeight,
                 size_t strideX,
                 size_t strideY,
                 size_t padLeft,
                 size_t padRight,
                 size_t padTop,
                 size_t padBottom) ;


/* ---------------------------------------------------------------- */
/*                                    switchUnpoolingBackward (CPU) */
/* ---------------------------------------------------------------- */

/* 
 assume the output array to be cleared or otherwise
 properly initialised: accumulates the derivative
 */

template<typename T>
void unpoolingBackward_cpu(T* dzdx,
                         T const* dzdy,
                         size_t width,
                         size_t height,
                         size_t depth,
                         size_t windowWidth,
                         size_t windowHeight,
                         size_t strideX,
                         size_t strideY,
                         size_t padLeft,
                         size_t padRight,
                         size_t padTop,
                         size_t padBottom)
{
  int pooledWidth = (width + (padLeft + padRight) - windowWidth)/strideX + 1 ;
  int pooledHeight = (height + (padTop + padBottom) - windowHeight)/strideY + 1 ;
  int* timesUpdated = new int[width*height];
  for (int i = 0; i < width*height; ++i) {
    timesUpdated[i] = 0;
  }
  
  for (int z = 0; z < depth; ++z) {
    for (int i = 0; i < width*height; ++i) {
      timesUpdated[i] = 0;
    }
    
    for (int y = 0; y < pooledHeight; ++y) {
      for (int x = 0; x < pooledWidth; ++x) {
        int x1 = x * (signed)strideX - (signed)padLeft ;
        int y1 = y * (signed)strideY - (signed)padTop ;
        int x2 = std::min(x1 + windowWidth, width) ;
        int y2 = std::min(y1 + windowHeight, height) ;
        x1 = std::max(x1, 0) ;
        y1 = std::max(y1, 0) ;
        
        for (int v = y1 ; v < y2 ; ++v) {
          for (int u = x1 ; u < x2 ; ++u) {
            timesUpdated[v * width + u] += 1;
          }
        }
      }
    }
    
    for (int y = 0; y < pooledHeight; ++y) {
      for (int x = 0; x < pooledWidth; ++x) {
        int x1 = x * (signed)strideX - (signed)padLeft ;
        int y1 = y * (signed)strideY - (signed)padTop ;
        int x2 = std::min(x1 + windowWidth, width) ;
        int y2 = std::min(y1 + windowHeight, height) ;
        x1 = std::max(x1, 0) ;
        y1 = std::max(y1, 0) ;
        
        for (int v = y1 ; v < y2 ; ++v) {
          for (int u = x1 ; u < x2 ; ++u) {
            if(timesUpdated[v * width + u] > 0) {
              dzdx[y * pooledWidth + x] += dzdy[v * width + u] / timesUpdated[v * width + u];
            }
          }
        }
      }
    }
    dzdx += pooledWidth*pooledHeight ;
    dzdy += width*height ;
  }
  
  delete [] timesUpdated;
}

template
void unpoolingBackward_cpu<float>(float* dzdx,
                         float const* dzdy,
                         size_t width,
                         size_t height,
                         size_t depth,
                         size_t windowWidth,
                         size_t windowHeight,
                         size_t strideX,
                         size_t strideY,
                         size_t padLeft,
                         size_t padRight,
                         size_t padTop,
                         size_t padBottom) ;

template
void unpoolingBackward_cpu<double>(double* dzdx,
                         double const* dzdy,
                         size_t width,
                         size_t height,
                         size_t depth,
                         size_t windowWidth,
                         size_t windowHeight,
                         size_t strideX,
                         size_t strideY,
                         size_t padLeft,
                         size_t padRight,
                         size_t padTop,
                         size_t padBottom) ;

