/*==========================================================
* saveSpectrogram.cpp
*
* Save a spectrogram to a binary file.
*
* Author: Thomas Churchman, 2015
*
*========================================================*/

#include <iostream>
#include "mex.h"

using namespace std;

void _main();

/* Parse a char array (i.e., "string"). */
char * parseCharArray(const mxArray *charArrayPtr)
{
	char *chars = new char[mxGetN(charArrayPtr) + 1];
	mxGetString(charArrayPtr, chars, (int)mxGetN(charArrayPtr) + 1);

	return chars;
}

/* Code entry-point. */
void mexFunction(
	int          	nlhs,
	mxArray      	*plhs[],
	int          	nrhs,
	const mxArray 	*prhs[]
	)
{
	// Check for proper number of arguments
	if (nrhs != 2)
	{
		mexErrMsgIdAndTxt("MATLAB:saveSpectrogram:nargin",
			"Invalid number of input arguments into SAVESPECTROGRAM.");
	}
	else if (nlhs != 0)
	{
		mexErrMsgIdAndTxt("MATLAB:saveSpectrogram:nargout",
			"SAVESPECTROGRAM requires no output argument.");
	}


	/* Process input arguments. */
	char *fileName = parseCharArray(prhs[0]);
	float *data = (float *)mxGetData(prhs[1]);
	int nElements = (int)mxGetNumberOfElements(prhs[1]);

	/* Save as binary file */
	FILE* pFile = fopen(fileName, "wb");
	fwrite(data, sizeof(*data), nElements, pFile);
	fclose(pFile);

	/* Destruct */
	delete fileName;
	fileName = NULL;

	return;
}
