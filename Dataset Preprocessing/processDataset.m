function processDataset
%% processDataset
% Author: Thomas Churchman
%
% Process the TIMIT dataset to the output directory. Processes both the
% train and test sets.

    [timitPath, outputPath, ~] = setPaths();

    % Create output folder structure
    if ~exist(outputPath, 'dir')
        mkdir(outputPath); 
        mkdir([outputPath '/train']);
        mkdir([outputPath '/test']);
    else
        error(['Output directory already exists: ' outputPath]);
    end
    
    type = 'fft';
    
    average = process(timitPath, type, strcat(outputPath, '/train'), 'train');
    process(timitPath, type, strcat(outputPath, '/test'), 'test', average);

end

function average = process(timitPath, type, outputPath, set, varargin)
%% process
% Author: Thomas Churchman
%
% Process a set (either train or test) in TIMIT. Performs various functions
% such as keeping track of the average phoneme spectrogram.

    if nargin >= 5
        average = varargin{1};
        collectAverage = false;
    else
        average = 0; 
        collectAverage = true;
    end

    phoneMapFunction = @(x) phoneToIndex(foldPhone(x));
    
    % Process train set
    [waves, phonemes, fs] = extractDataset(timitPath, set, ...
                'maxlength', 2554, 'sentence', '~SA1', 'sentence', '~SA2', 'phoneme', '~q');
    [numWaves, ~] = size(waves);

    numDigits = ceil(log10(numWaves));
    sprintfFormat = strcat('%0', num2str(numDigits), 'd'); % e.g. '%05d'

    phonemes = cellfun(phoneMapFunction, phonemes, 'UniformOutput', false);
    phonemeFileNames = cellfun(@(x) strcat(sprintf(sprintfFormat, x), '_phoneme.bin'), num2cell(1:numWaves), 'UniformOutput', false);
    waveFileNames = cellfun(@(x) strcat(sprintf(sprintfFormat, x), '_wave.mat'), num2cell(1:numWaves), 'UniformOutput', false);

    [p, f, t] = preprocess(waves(1,:), fs, type);
    [height, width, depth] = size(p(:,:,:,1));
    mainOutput = strcat(outputPath, '\_main.mat');
    
    for i = 1:numWaves
        phonemeOutput = strcat(outputPath, '\', phonemeFileNames{i});
        waveOutput = strcat(outputPath, '\', waveFileNames{i});

        % Generate spectrogram
        [spectra, f, t] = preprocess(waves(i,:), fs, type);
        
        phonemeSpectrogram = spectra(:,:,1);
        phonemeWave = waves(i,:);

        if collectAverage
           average = average + phonemeSpectrogram;
        end
        
        % save(strcat(phonemeOutput, '.mat'), 'phonemeSpectrogram');
        saveSpectrogram(phonemeOutput, phonemeSpectrogram);
        save(waveOutput, 'phonemeWave');
    end
    
    if collectAverage
       average = average ./ numWaves; 
    end
    
    save(mainOutput, 'phonemes', 'phonemeFileNames', 'waveFileNames', ...
        'average', 'fs', 'f', 't', 'height', 'width', 'depth');
end