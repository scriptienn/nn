function [spectra, f, t] = preprocess( waves, fs, type )
%% preprocess
% Author: Jordi Riemens, Thomas Churchman
% 
% Preprocess the input wave(s) to a (series of) spectrogram(s).
%
% Input:
% - waves: the signal waveform(s) that will be converted to 
%   a spectrogram/spectrograms. The dimensions of waves are PxS with P the
%   number of phonemes and S the number of samples per wave.
% - fs: the sampling frequency in Hz the waves were recorded at.
% 
% Output:
% - spectra: the spectrogram(s). Dimensions: HxWxDxP, where H is the height
%   (number of frequency bins), W is the width (number of time bins), D is
%   the depth (generally 1 or 2, i.e. real or complex spectrogram), and P
%   is the number of spectrograms (one for each Phoneme).
% - f: vector containing the frequency points represented.
% - t: vector containing the time points represented.

if strcmp(type, 'mel')
    [nrOfWaves, ~] = size(waves);
    wavesCell = num2cell(waves, 2);
    
    numcep = 40;
    
    wintime = 0.025;
    hoptime = 0.010;
    
    minfreq = 0;
    maxfreq = 4000;
    
    spectrograms = cellfun(@(wave) melfcc( ...
            wave, ...
            fs, ...
            'numcep', numcep, ...
            'wintime', wintime, ...
            'hoptime', hoptime, ...
            'minfreq', minfreq, ...
            'maxfreq', maxfreq), ...
        wavesCell, 'UniformOutput', false);
    
    [bins, windows] = size(spectrograms{1});
    
    spectra = zeros(bins, windows, 1, nrOfWaves);
    for i = 1:nrOfWaves
        spectrograms{i}(isnan(spectrograms{i})) = 0;
        spectra(:,:,1,i) = spectrograms{i};
    end
    
    f = 1:bins;
    t = 0:hoptime:hoptime*windows;
    
    % Convert to single precision matrix (as opposed to double precision).
    spectra = single(spectra);
elseif strcmp(type, 'fft')
    
    % 512x32
    % windowSize = floor(fs/60);
    % frameRate = floor(fs/220);
    % nfft = 1024;

    % 201x16
    windowSize = floor(fs/60);
    frameRate = floor(fs/110);
    nfft = 400;
    
    
    % overlap between windows is windowSize-frameRate
    overlap = windowSize - frameRate;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    [nrOfWaves, ~] = size(waves);
    wavesCell = num2cell(waves, 2);
    spectrograms = cellfun(@(wave) spectrogram(wave,windowSize,overlap, nfft, fs), wavesCell, ...
        'UniformOutput', false);
    [~, f, t] = spectrogram(wavesCell{1},windowSize,overlap, nfft, fs);

    [bins, windows] = size(spectrograms{1});

    spectra = zeros(bins, windows, 1, nrOfWaves);
    for i = 1:nrOfWaves
        % spectra(:,:,1,i) = abs(spectrograms{i});
        % spectra(:,:,2,i) = angle(spectrograms{i});
        spectra(:,:,1,i) = real(spectrograms{i});
    end

    % Convert to single precision matrix (as opposed to double precision).
    spectra = single(spectra);
end

end

