function [waves, phonemes, fs] = extractDataset(timitPath, set, varargin)
%% extractDataset
% Author: Thomas Churchman
%
% Gets wave-vectors from the timit database entries and finds the phonemes.
%
% Input:
% - timitPath: the path to timit
% - set: the set to use {train, test}
% 
% Output:
% - waves: the sliced, vectorized phoneme waves
% - phonemes: the phoneme labels
% - fs: waveform sampling frequency in Hz
%
% Filtering:
% Additional filtering can be done by passing more input arguments, e.g.:
% extractDataset('path\to\set', 'train', 'sentence', 'SA1')
% (Read 'MatlabADT - User Manual.pdf' for more information on filtering)
%
% Example usage:
% - [waves, phonemes, fs] = extractDataset('path\to\set', 'test');
% - [waves, phonemes, fs] = extractDataset('path\to\set', 'train');
% - [waves, phonemes, fs] = extractDataset('path\to\set', 'train', 'sentence', 'SA1');
% - [waves, phonemes, fs] = extractDataset('path\to\set', 'train', 'dialect', 'dr1');
% - [waves, phonemes, fs] = extractDataset('path\to\set', 'train', 'word', 'she');

    if(~strcmp(set, 'train') && ~strcmp(set, 'test'))
        error('Expected set to be either train or test');
    end
    
    maxLength = -1;
    
    % If more than two arguments were passed (more than path and set),
    % we have optional arguments to process (which are in varargin)
    if nargin > 2
        if strcmp(varargin{1}, 'maxlength')
            maxLength = varargin{2};
            varargin = varargin(3:end);
        end
    else
        % do nothing
    end
    
    addpath('Toolboxes/MatlabADT');
    
    % Initialise database
    db = ADT('timit', timitPath);
    
    % Query the database for phonemes, with optional filter settings in varargin
    [waves, fs, metadata] = query(db, 'usage', set, 'phoneme', '#all', varargin{:});
    
    
    % Grab phoneme labels from the metadata
    phonemes = cellfun(@(x) x.phoneme, metadata, 'UniformOutput', false);

    % Throw away top 5%
    numPhonemes = length(waves);

    lengths = cell2mat(cellfun(@(x) length(x), waves, 'UniformOutput', false));
    
    if maxLength == -1
        sortLengths = sort(lengths);
        idx = floor(numPhonemes*0.95);
        maxLength = sortLengths(idx);
    end

    idx = lengths <= maxLength;

    waves = waves(idx);
    phonemes = phonemes(idx);

    % Zero pad
    % waves = cellfun(@(x) padarray(x', [0 (maxLength - length(x))], ...
    %     'post'), waves, 'UniformOutput', false);
    
    waves = cellfun(@(x) ...
        padarray( ...
            padarray(x', [0 floor((maxLength - length(x))/2) ]), ...
            [0 rem((maxLength - length(x)), 2)], 'post'), ...
        waves, 'UniformOutput', false);
    
    %{
    % Find the number of samples the largest wave has

    maxSamples = max(cell2mat(cellfun(@(x) length(x), waves, 'UniformOutput', false)));

    % Zero-pad waves
    waves = cellfun(@(x) ...
        padarray( ...
            padarray(x', [0 floor((maxSamples - length(x))/2) ]), ...
            [0 rem((maxSamples - length(x)), 2)], 'post'), ...
        waves, 'UniformOutput', false);

    % Above does the following, but quicker:
    % waves = cellfun(@(x) padarray(x', [0 floor((maxSamples - length(x))/2) ]), waves, 'UniformOutput', false);
    % waves = cellfun(@(x) padarray(x, [0 rem((maxSamples - length(x)), 2) ], 'post'), waves, 'UniformOutput', false);
    % i.e., first pad on left and right sides with equal amount of 0s,
    % then add another zero to the end if the remainder of division is 1
    %}
    
    % To matrix:
    waves = cell2mat(waves);
end