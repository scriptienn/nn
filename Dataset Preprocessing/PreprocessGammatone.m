function [spectra, analyzer, f, t] = preprocessGammatone( waves, fs )
%% preprocessGammatone
% Author: Thomas Churchman
% 
% Preprocess the input wave(s) to a (series of) gammatonogram(s).
%
% Input:
% - waves: the signal waveform(s) that will be converted to 
%   a gammatonogram/gammatonograms. The dimensions of waves are PxS with P 
%   the number of phonemes and S the number of samples per wave.
% - fs: the sampling frequency in Hz the waves were recorded at.
% 
% Output:
% - spectra: the gammatonogram(s). Dimensions: HxWxDxP, where H is the 
%   height (number of frequency bins), W is the width (number of time 
%   bins), D is the depth (generally 1 or 2, i.e. real or complex 
%   gammatonogram), and P is the number of gammatonograms (one for each 
%   Phoneme).
% - f: vector containing the frequency points represented.
% - t: vector containing the time points represented.

flow = 70;
fhigh = 6700;
base_frequency_hz = 1000;
filters_per_ERB = 1.0;
delay = 100 / fs;

analyzer = gfb_analyzer_new(fs, flow, ...
                            base_frequency_hz, fhigh,...
                            filters_per_ERB);

f = analyzer.center_frequencies_hz;
t = (0:length(waves(1,:))) / fs;
                        
[nrOfWaves, ~] = size(waves);
wavesCell = num2cell(waves, 2);
spectrograms = cellfun(@(wave) gfb_analyzer_process(analyzer, wave), wavesCell, ...
    'UniformOutput', false);

% If you do not give at least one wave, you're stupid
[bins, windows] = size(spectrograms{1});

spectra = zeros(bins, windows, 2, nrOfWaves);
for i = 1:nrOfWaves
    spectra(:,:,1,i) = abs(spectrograms{i});
    spectra(:,:,2,i) = angle(spectrograms{i});
end


%waves = fft(waves')'; % fft works on columns, and in waves, a window is a row
%spectra = cat(3, abs(waves), angle(waves));

end

