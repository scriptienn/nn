function setup()
%% setup
% Author: Thomas Churchman
% 
% Set up MATLAB's path.

    addpath 'Phone Mapping';
	addpath 'Dataset Preprocessing';
	addpath 'Plotting';
	addpath 'Spectrograms';
	addpath 'Test Code';
	addpath 'Training';
    addpath 'Training with Decoding';
	addpath 'Training/Architectures';
    addpath 'Training/Architectures/Autoencoder';
    addpath 'Training/Architectures/Autoencoder/convt_performance_comparison';
    addpath 'Training/Architectures/Autoencoder/autoencoder_baselines';
	addpath 'Toolboxes/rastamat'
    addpath 'Postprocessing'
    addpath 'Utils'
    addpath 'Input Reconstruction'
end