function net = gatherNetwork(net)
%% gatherNetwork
% Author: Thomas Churchman
%
% Gathers the matrix values of a distributed (GPU-)network into regular
% matrices.

    net.layers = cellfun(@gatherLayer, net.layers, ...
        'UniformOutput', false);
end

function layer = gatherLayer(layer)
%% gatherLayer
% Author: Thomas Churchman
%
% Gathers the matrix values of a distributed (GPU-)network layer into
% regular matrices.

    if strcmp(layer.type, 'conv')
        layer.filters = ...
            gather(layer.filters);

        layer.biases = ...
            gather(layer.biases);

        layer.filtersMomentum = ...
            gather(layer.filtersMomentum);

        layer.biasesMomentum = ...
            gather(layer.biasesMomentum);

        layer.historicalFilterGradients = ...
            gather(layer.historicalFilterGradients);

        layer.historicalBiasGradients = ...
            gather(layer.historicalBiasGradients);
    end
end