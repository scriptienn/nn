%
%geluidTest2

index = 11;

%load spectrum
spectrum = getInput(index,'test');

%set parameters
fs = 16000;
windowSize = floor(fs/60);
framerate = floor(fs/110);
overlap = windowSize - framerate;
nfft = 400;

testInvSpecgram = invspecgram(spectrum,nfft,fs,windowSize,overlap);

soundsc(testInvSpecgram, fs);

wave = getWave(index, 'test');
soundsc(wave,fs);
