function playSound( spec, t, f )
%PLAYSOUND Summary of this function goes here
%   Detailed explanation goes here

fs = 16000;
windowSize = floor(fs/60);
overlap = floor(windowSize - fs/220);
testInvSpecgram = invspecgram(single(real(spec)),400,fs,windowSize,overlap);
%testInvSpecgram = padarray(testInvSpecgram, [(length(y) - length(testInvSpecgram)) 0], 'post');
soundsc(testInvSpecgram, fs);

end

