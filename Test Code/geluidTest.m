% get a portion of signal
% [y,fs]=audioread('../timit/TIMIT/TRAIN/DR1/FCJF0/SI1027.wav');
%[y, fs]=audioread('D:/music/Spinvis/Goochelaars & geesten/01 Goochelaars & geesten.mp3');
%{
[timitPath, ~, ~, inputPath, outputPath] = setPaths();

%[y, fs] = audioread(strcat(timitPath, '/TRAIN/DR5/FEXM0/SA2.WAV'));
w = load(strcat(inputPath, '/train/00005_wave.mat'));
y = w.phonemeWave';

[mm,aspc,pspc] = melfcc(y, fs, 'minfreq', 0, 'maxfreq', 8000, 'numcep', 64, 'nbands', 80, 'fbtype', 'mel', 'dcttype', 1, 'usecmp', 1, 'wintime', 0.025, 'hoptime', 0.01, 'preemph', 0.97, 'dither', 0);
% .. then convert the cepstra back to audio (same options)
[im,ispc] = invmelfcc(mm, fs, 'minfreq', 0, 'maxfreq', 8000, 'numcep', 64, 'nbands', 80, 'fbtype', 'mel', 'dcttype', 1, 'usecmp', 1, 'wintime', 0.025, 'hoptime', 0.01, 'preemph', 0.97, 'dither', 0);
% listen to the reconstruction
soundsc(im,fs);

subplot(3,1,1);
imagesc(mm);
subplot(3,1,2);
imagesc(aspc);
subplot(3,1,3);
imagesc(pspc);
%}

%{
[y, fs] = audioread(strcat(timitPath, '/TRAIN/DR5/FEXM0/SA2.WAV'));

flow = 70;
fhigh = 6700;
base_frequency_hz = 1000;
filters_per_ERB = 1.0;
delay = 100 / fs;

analyzer = gfb_analyzer_new(fs, flow, ...
                            base_frequency_hz, fhigh,...
                            filters_per_ERB);
synthesizer = gfb_synthesizer_new(analyzer, 1/fs);

f = analyzer.center_frequencies_hz;
t = (0:length(y) / fs);
                        

gammatonogram = gfb_analyzer_process(analyzer, y);
reconstruct = gfb_synthesizer_process(synthesizer, gammatonogram);

% max(xcorr(reconstruct',y,'coeff'))
%}

[timitPath, ~, ~, inputPath, outputPath] = setPaths();
fs = 16000;

% [y, fs] = audioread(strcat(timitPath, '/TRAIN/DR5/FEXM0/SA2.WAV'));
w = load(strcat(inputPath, '/test/00288_wave.mat'));
y = w.phonemeWave';

windowSize = floor(fs/60);
overlap = floor(windowSize - fs/110);
[B,f,t]=spectrogram(y,windowSize, overlap, 400, fs);
testInvSpecgram = invspecgram(single(real(B)),400,fs,windowSize,overlap);
testInvSpecgram = padarray(testInvSpecgram, [(length(y) - length(testInvSpecgram)) 0], 'post');
%soundsc(testInvSpecgram, fs);
max(xcorr(testInvSpecgram,y,'coeff'))

plotSpectrogram(real(B), t, f);

% calculate average xcorr error to average spectrogram waveform
testInvSpecgram = invspecgram(single(real(average)),400,fs,windowSize,overlap);
testInvSpecgram = padarray(testInvSpecgram, [(length(y) - length(testInvSpecgram)) 0], 'post');
avgXCorr = 0;
for i=1:max(numel(waveFileNames), 5000)
    file = waveFileNames{i};
    w = load(strcat(inputPath, '/test/', file));
    avgXCorr = avgXCorr + max(xcorr(testInvSpecgram,y,'coeff'));
end
avgXCorr = avgXCorr ./ max(numel(waveFileNames), 5000)


%testInvSpecgram = invspecgram(single(real(average)),400,fs,windowSize,overlap);
%testInvSpecgram = padarray(testInvSpecgram, [(length(y) - length(testInvSpecgram)) 0], 'post');
%soundsc(testInvSpecgram, fs);
%max(xcorr(testInvSpecgram,y,'coeff'))


%[B,f,t]=specgram(y,1024,fs,windowSize, overlap);

%C = pvsample(B, [1:1000], 1024/4);
%[rows, cols] = size(B);
%C = imresize(B, [rows cols*1.15]);
%testInvSpecgram = invspecgram(C,1024,fs,256,192);
%soundsc(testInvSpecgram, fs);

