function smartUpsamplingTest(net)
%% smartUpsamplingTest
% Author: Thomas Churchman
%
% Function to test an undesirable effect of smart upsampling.

    [~, ~, matconvnetPath, inputPath, ~] = setPaths();

    run([matconvnetPath '\matlab\vl_setupnn.m']);

    data = load ([inputPath '\test\_main.mat']);

    netCopy = net;
    for i = 1:length(netCopy.layers)
        % Find the first convt and set its filters and biases to 0
        if strcmp(netCopy.layers{i}.type, 'convt')
            netCopy.layers{i}.filters = ...
                zeros(size(netCopy.layers{i}.filters), 'single');
            netCopy.layers{i}.biases = ...
                zeros(size(netCopy.layers{i}.biases), 'single');
            break;
        end
    end

    %chirps = [0 8000; 2000 6000; 8000 0; 6000 2000];
    %ps = [28158,5801,21616,5531,38558,27881,23029,33747,3727,6594,26404,40999,14156,4137,11419,18978,26374,6203,1457,8598,13691,28110,32800,715,31381,25133,40804,40188,45343,7155,41341,20426,37249,29579,18838,43586,45261,42999,42763,45368,5974,29369,17306,20161,4533,4755,41842,7203];

    chirps = [0 8000; 6000 2000];
    %ps = [33747,45343,28158];
    ps = [45343,28158];

    d = size(chirps);

    h = d(1) + length(ps);
    w = 3;

    for chirp=1:d(1)
        fs = 16000;
        samples = 2554;

        startFreq = chirps(chirp,1);
        endFreq   = chirps(chirp,2);

        chirpWave = chirpGen(startFreq, endFreq, samples, fs);
        chirpSpectrogram = preprocess(chirpWave, fs, 'fft');

        subplot(h, w, (chirp-1)*w+1);
        
        if chirp == 1
            plotSpectrogramNoTicks(chirpSpectrogram, data.t, data.f);
            title('Input');
        else
            plotSpectrogramNoLabelsNoTicks(chirpSpectrogram, data.t, data.f);
        end
        
        subplot(h, w, (chirp-1)*w+2);
        res = vl_simplenn(net, chirpSpectrogram, [],  []);
        plotSpectrogramNoLabelsNoTicks(res(end).x, data.t, data.f);
        if chirp == 1
            title('Output');
        end
        
        subplot(h, w, (chirp-1)*w+3);
        res = vl_simplenn(netCopy, chirpSpectrogram, [],  []);
        plotSpectrogramNoLabelsNoTicks(res(end).x, data.t, data.f);
        if chirp == 1
            title('Mangled Outp.');
        end
    end

    
    for i=1:length(ps)
        p = ps(i);
        input = readSpectrogram([inputPath '\test\' data.phonemeFileNames{p}], data.height, data.width, data.depth);

        subplot(h, w, (d(1)+i-1)*w+1);
        plotSpectrogramNoLabelsNoTicks(input, data.t, data.f);
        
        subplot(h, w, (d(1)+i-1)*w+2);
        res = vl_simplenn(net, input, [],  []);
        plotSpectrogramNoLabelsNoTicks(res(end).x, data.t, data.f);
        
        subplot(h, w, (d(1)+i-1)*w+3);
        res = vl_simplenn(netCopy, input, [],  []);
        plotSpectrogramNoLabelsNoTicks(res(end).x, data.t, data.f);
    end
    
end