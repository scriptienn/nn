function [] = testNetworkDimensions( architecture, index)
%TESTNETWORKDIMENSIONS tests the network dimensions by running one
%spectrogram through the network
%   architecture = the network architecture that is tested
%   index = the index of the spectrogram with which the network is tested

% Prepare options
    opts = {};
    opts.numEpochs = 10;
    opts.batchSize = 128;
    opts.examplesPerPhoneme = 500; % Take x examples of each phoneme per epoch
    opts.testPerPhoneme = 200; % Take x examples of each phoneme per test epoch
    opts.useGpu = false;
    opts.learningRate = [0.08 0.08 0.08 0.06 0.04 0.02 0.01 0.005 0.002 0.001];
    opts.continue = true;
    opts.learningRateChanged = false;
    %opts.expDir = experimentPath;
    opts.conserveMemory = false;
    opts.sync = true;
    opts.prefetch = false;
    opts.weightDecay = 0.0005;
    opts.momentum = 0.9;
    opts.errorType = 'multiclass'; 
    opts.errorOncePerNEEpochs = true;
    opts.NE = 2;
    opts.plotDiagnostics = false;
    % opts = vl_argparse(opts, varargin);
    % Adagrad:
    opts.ada_master_stepsize = 1e-2;
    opts.ada_fudge_factor = 1e-6;


% Load architecture
net.layers = eval(architecture);
net.architecture = architecture;
    
% Initialize network
net = cnn_initialize(net, opts);

X = getInput(index, 'train');

vl_simplennPrint(net,X);


end

