function layers = cnn_arch_timeDelay_512x32()
%% cnn_arch_timeDelay_512x32
% Author: Thomas Churchman
%
% A small network, based on the idea of time-delay networks:
% http://www.inf.ufrgs.br/~engel/data/media/file/cmp121/waibel89_TDNN.pdf
%
% data in: 512 frequency bins, 32 time windows.
% Please see in-line comments for more information about the architecture.

	f = 1/10;
	layers = {}; 
    % layers{end+1} = struct('type', 'dropout', 'rate', 0.2);
    
    layers{end+1} = struct('type', 'dropout', 'rate', 0.2);
    
    % In:  512 x 32 x 1
    % Out: 1   x 30 x 256
    % CONVOLUTION
    layers{end+1} = struct('type', 'conv', ...
                               'filters', f*randn(512,2,1,256, 'single'), ...
                               'biases', zeros(1, 256, 'single'), ...
                               'stride', 1, ...
                               'pad', 0);
                           
    layers{end+1} = struct('type', 'dropout', 'rate', 0.3);
                           
    % In:  1   x 30 x 256
    % Out: 1   x 15 x 256
    % POOLING
    % layers{end+1} = struct('type', 'pool', ...
    %                            'method', 'max', ...
    %                            'pool', [1 2], ...
    %                            'stride', 2, ...
    %                            'pad', 0);
    
    layers{end+1} = struct('type', 'relu');
    
    layers{end+1} = struct('type', 'dropout', 'rate', 0.3);
    
    % In:  1 x 15 x 256
    % Out: 1   x 1 x 512
    % FULLY CONNECTED
    layers{end+1} = struct('type', 'conv', ...
                               'filters', f*randn(1,31,256,512, 'single'), ...
                               'biases', zeros(1, 512, 'single'), ...
                               'stride', 1, ...
                               'pad', 0);
                           
    layers{end+1} = struct('type', 'relu');
    
    layers{end+1} = struct('type', 'dropout', 'rate', 0.3);
    
    % In:  1   x 1  x 512
    % Out: 1   x 1  x 39
    % FULLY CONNECTED.
    layers{end+1} = struct('type', 'conv', ...
                               'filters', f*randn(1,1,512,768, 'single'),...
                               'biases', zeros(1,768,'single'), ...
                               'stride', 1, ...
                               'pad', 0);
    
    layers{end+1} = struct('type', 'relu');
    
    layers{end+1} = struct('type', 'dropout', 'rate', 0.3);
    
    layers{end+1} = struct('type', 'conv', ...
                               'filters', f*randn(1,1,768,39, 'single'),...
                               'biases', zeros(1,39,'single'), ...
                               'stride', 1, ...
                               'pad', 0);
                           
    layers{end+1} = struct('type', 'softmaxloss');
    
    
    %{
    
    % layers{end+1} = struct('type', 'dropout', 'rate', 0.2);
    
    % In:  1   x 15 x 256
    % Out: 1   x 8  x 512
    % CONVOLUTION
    layers{end+1} = struct('type', 'conv', ...
                               'filters', f*randn(1,8,256,512, 'single'),...
                               'biases', zeros(1,512,'single'), ...
                               'stride', 1, ...
                               'pad', 0); 
    layers{end+1} = struct('type', 'relu');
    
    % layers{end+1} = struct('type', 'dropout', 'rate', 0.2);
    
    % In:  1   x 8 x 512
    % Out: 1   x 2  x 768
    % CONVOLUTION
    layers{end+1} = struct('type', 'conv', ...
                               'filters', f*randn(1,7,512,768, 'single'),...
                               'biases', zeros(1,768,'single'), ...
                               'stride', 1, ...
                               'pad', 0);
                           
    % In:  1   x 2 x 768
    % Out: 1   x 1  x 768                  
    % POOLING
    layers{end+1} = struct('type', 'pool', ...
                               'method', 'max', ...
                               'pool', [1 2], ...
                               'stride', 2, ...
                               'pad', 0);
	layers{end+1} = struct('type', 'relu');
    
    %layers{end+1} = struct('type', 'dropout', 'rate', 0.2);
    
    % In:  1   x 1  x 768
    % Out: 1   x 1  x 768
    % FULLY CONNECTED.
    layers{end+1} = struct('type', 'conv', ...
                               'filters', f*randn(1,1,768,768, 'single'),...
                               'biases', zeros(1,768,'single'), ...
                               'stride', 1, ...
                               'pad', 0);
    layers{end+1} = struct('type', 'relu');
    
    % layers{end+1} = struct('type', 'dropout', 'rate', 0.2);
    
    % In:  1   x 1  x 768
    % Out: 1   x 1  x 39
    % FULLY CONNECTED.
    layers{end+1} = struct('type', 'conv', ...
                               'filters', f*randn(1,1,768,39, 'single'),...
                               'biases', zeros(1,39,'single'), ...
                               'stride', 1, ...
                               'pad', 0);
    layers{end+1} = struct('type', 'softmaxloss');
    %}
    
end