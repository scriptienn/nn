function layers = cnn_autoencoder_randomae_fullunit()

net = load('exp\trained_real_padded_nodeltas_e372.mat', 'net');
net = net.net;

layers = {};
layers{end+1} = struct('type', 'conv', ...
                       'filterSize', size(net.layers{1}.filters), ...
                       'learnFilters', false, ...
                       'learnBiases', true);
layers{end+1} = net.layers{3};
layers{end+1} = struct('type', 'relu');
layers{end+1} = struct('type', 'unpool');
layers{end+1} = struct('type', 'convt', ...
                       'learnFilters', true, ...
                       'learnBiases', true);

layers = dec_cnn_autocompletearch(layers);

end

