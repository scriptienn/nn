function layers = cnn_autoencoder_allrandomae_convonly()

net = load('D:/Uni2/Scriptie/output/experiments cnn_arch_frequencyConvolution/experiment 2015-06-18 cnn_arch_frequencyConvolution_fullPadding newMap real fft 201 freq bins 16 time bins/net-epoch-372-GATHERED.mat', 'net');
net = net.net;

layers = {};
layers{end+1} = struct('type', 'conv', ...
                       'filterSize', size(net.layers{1}.filters), ...
                       'learnFilters', false, ...
                       'learnBiases', true);
layers{end+1} = struct('type', 'conv', ...
                       'filterSize', dec_cnn_filtertransposesize(size(net.layers{1}.filters)), ...
                       'learnFilters', false, ...
                       'learnBiases', true);

layers = dec_cnn_autocompletearch(layers);

end

function filterSize = dec_cnn_filtertransposesize(filterSize)
filterSize = filterSize([1 2 4 3]);
end

%{
cnn_autoencoder_allrandomae_fullunit.m

function layers = cnn_autoencoder_allrandomae_fullunit()

net = load('D:/Uni2/Scriptie/output/experiments cnn_arch_frequencyConvolution/experiment 2015-06-18 cnn_arch_frequencyConvolution_fullPadding newMap real fft 201 freq bins 16 time bins/net-epoch-372-GATHERED.mat', 'net');
net = net.net;

layers = {};
layers{end+1} = struct('type', 'conv', ...
                       'filterSize', size(net.layers{1}.filters), ...
                       'learnFilters', false, ...
                       'learnBiases', true);
layers{end+1} = net.layers{3};
layers{end+1} = struct('type', 'relu');
layers{end+1} = struct('type', 'unpool');
layers{end+1} = struct('type', 'conv', ...
                       'filterSize', cnn_filtertransposesize(size(net.layers{1}.filters)), ...
                       'learnFilters', false, ...
                       'learnBiases', true);

layers = dec_cnn_autocompletearch(layers);

end

function filterSize = cnn_filtertransposesize(filterSize)
filterSize = filterSize([1 2 4 3])
end
%}