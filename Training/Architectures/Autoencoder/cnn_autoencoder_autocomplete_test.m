function layers = cnn_autoencoder_autocomplete_test()
%CNN_AUTOENCODER_AUTOCOMPLETE_TEST Summary of this function goes here
%   Detailed explanation goes here

layers = {};
layers{1} = struct('type', 'conv', 'filterSize', [3 2 1 8]);
layers{2} = struct('type', 'pool', 'method', 'max', 'pool', [5 2], 'stride', 2, 'pad', 0);
layers{3} = struct('type', 'conv', 'filterSize', [8 1 8 6]);
layers{4} = struct('type', 'pool', 'method', 'max', 'pool', [7 4], 'stride', 3, 'pad', 0);
layers{5} = struct('type', 'unpool');
layers{6} = struct('type', 'convt');
layers{7} = struct('type', 'unpool');
layers{8} = struct('type', 'convt');

layers = dec_cnn_autocompletearch(layers);


end

