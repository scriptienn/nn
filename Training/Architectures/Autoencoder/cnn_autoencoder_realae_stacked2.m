function layers = cnn_autoencoder_realae_stacked2()

net = load('exp\trained_real_padded_nodeltas_e372.mat', 'net');
net = net.net;
ae = load('exp\cnn_autoencoder_realae_fullunit\net-epoch-66.mat', 'net');
ae = ae.net;

layers = {};
layers{end+1} = struct('type', 'conv', ...
                       'filters', ae.layers{1}.filters, ...
                       'biases', ae.layers{1}.biases, ...
                       'filterSize', size(ae.layers{1}.filters), ...
                       'learnFilters', false, ...
                       'learnBiases', false);
layers{end+1} = net.layers{3};
layers{end+1} = struct('type', 'relu');
layers{end+1} = struct('type', 'conv', ...
                       'filterSize', size(net.layers{4}.filters), ...
                       'learnFilters', true, ...
                       'learnBiases', true);
layers{end+1} = net.layers{6};
layers{end+1} = struct('type', 'relu');
layers{end+1} = struct('type', 'unpool');
layers{end+1} = struct('type', 'convt', ...
                       'learnFilters', true, ...
                       'learnBiases', true);
layers{end+1} = struct('type', 'relu');
layers{end+1} = struct('type', 'unpool');
layers{end+1} = struct('type', 'conv', ...
                       'filters', ae.layers{5}.filters, ...
                       'biases', ae.layers{5}.biases, ...
                       'filterSize', size(ae.layers{5}.filters), ...
                       'learnFilters', false, ...
                       'learnBiases', false);

layers = dec_cnn_autocompletearch(layers);

end

