function layers = cnn_arch_timeDelay_fullyConnected()
%% cnn_arch_timeDelay_fullyConnected
% Author: Thomas Churchman
%
% A small network, based on the idea of time-delay networks:
% http://www.inf.ufrgs.br/~engel/data/media/file/cmp121/waibel89_TDNN.pdf
%

	f = 1/100;
	layers = {};
    
    % IN: 201 x 16 x 1
    % OUT: 1  x 16 x 256
    layers{end+1} = struct('type', 'conv', ...
                               'filters', f*randn(201,1,3,256, 'single'), ...
                               'biases', zeros(1, 256, 'single'), ...
                               'stride', 1, ...
                               'pad', 0);
	% IN: 1 x 16 x 256
    % OUT: 1 x 8 x 256
	layers{end+1} = struct('type', 'pool', ...
                               'method', 'max', ...
                               'pool', [1 2], ...
                               'stride', 2, ...
                               'pad', 0);
    layers{end+1} = struct('type', 'relu');
    
    % IN: 1 x 8 x 256
    % OUT: 1 x 4 x 512
    layers{end+1} = struct('type', 'conv', ...
                               'filters', f*randn(1,5,256,512, 'single'),...
                               'biases', zeros(1,512,'single'), ...
                               'stride', 1, ...
                               'pad', 0);
                           
	% IN: 1 x 4 x 512
    % OUT: 1 x 2 x 512
	layers{end+1} = struct('type', 'pool', ...
                               'method', 'max', ...
                               'pool', [1 2], ...
                               'stride', 2, ...
                               'pad', 0);
	layers{end+1} = struct('type', 'relu');
    
    % IN: 1 x 2 x 512
    % OUT: 1 x 1 x 1024
    % FULLY CONNECTED
    layers{end+1} = struct('type', 'conv', ...
                               'filters', f*randn(1,2,512,1024, 'single'),...
                               'biases', zeros(1,1024,'single'), ...
                               'stride', 1, ...
                               'pad', 0);
    layers{end+1} = struct('type', 'relu');
    
    % DROPOUT 60%
    layers{end+1} = struct('type', 'dropout', 'rate', 0.8);
    
    % IN: 1 x 1 x 1024
    % OUT: 1 x 1 x 39
    % FULLY CONNECTED
    layers{end+1} = struct('type', 'conv', ...
                               'filters', f*randn(1,1,1024,39, 'single'),...
                               'biases', zeros(1,39,'single'), ...
                               'stride', 1, ...
                               'pad', 0);
    % layers{end+1} = struct('type', 'relu');
    
    layers{end+1} = struct('type', 'softmaxloss');
    
    
end