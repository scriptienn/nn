function [ spectraWithDeltas ] = createDeltas( spectra )
%% createDeltas
% Author: Diede Kemper, Thomas Churchman
% 
% Calculates the deltas for the given spectra.
%
% Input:
% - spectra: the input spectrograms, dimensions Height x Width x Depth x Nr
% of Spectrograms.
% 
% Output:
% - spectraWithDeltas: the spectrograms along with their first and
% second temporal derivatives added as additional channels (depth). 
% Size: H x W x (Dx3) x N
%
% Note: the derivative matrices are zeropadded at the start to make
% them of equal size as the spectra.

firstDerivatives = diff(spectra, 1, 2);
secondDerivatives = diff(spectra, 2, 2);

firstDerivatives = padarray(firstDerivatives, [0 1 0 0 ], 0, 'pre');
secondDerivatives = padarray(secondDerivatives, [0 2 0 0 ], 0, 'pre');

spectraWithDeltas = cat(3, spectra, firstDerivatives, secondDerivatives);

end

