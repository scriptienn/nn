function net = cnn_initialize(net, opts)
%% cnn_initialize
% Author: Thomas Churchman
% 
% Initialize training data (e.g. momentum) for the given network with given
% options.
%
% Input: 
% - net: the network to initialize.
% - opts: initialization options.
%
% Output:
% - net: the initialized network.
    
    for i=1:numel(net.layers)
        if ~strcmp(net.layers{i}.type,'conv') 
            continue; 
        end
        
        % Filters and biases should be real and full (not sparse)
        %#ok<*ZEROLIKE>
        net.layers{i}.filtersMomentum = zeros(size(net.layers{i}.filters), ...
            class(net.layers{i}.filters));
        net.layers{i}.biasesMomentum = zeros(size(net.layers{i}.biases), ...
            class(net.layers{i}.biases));
        net.layers{i}.historicalFilterGradients = zeros(size(net.layers{i}.filters), ...
            class(net.layers{i}.filters));
        net.layers{i}.historicalBiasGradients = zeros(size(net.layers{i}.biases), ...
            class(net.layers{i}.biases));
        
        if ~isfield(net.layers{i}, 'filtersLearningRate')
            net.layers{i}.filtersLearningRate = 1;
        end
        
        if ~isfield(net.layers{i}, 'biasesLearningRate')
            net.layers{i}.biasesLearningRate = 1;
        end
        
        if ~isfield(net.layers{i}, 'filtersWeightDecay')
            net.layers{i}.filtersWeightDecay = 1;
        end
        
        if ~isfield(net.layers{i}, 'biasesWeightDecay')
            net.layers{i}.biasesWeightDecay = 1;
        end
    end

    if opts.useGpu
        net = vl_simplenn_move(net, 'gpu');
        for i=1:numel(net.layers)
            if ~strcmp(net.layers{i}.type,'conv')
                continue; 
            end
            net.layers{i}.filtersMomentum = gpuArray(net.layers{i}.filtersMomentum);
            net.layers{i}.biasesMomentum = gpuArray(net.layers{i}.biasesMomentum);
        end
    end
end