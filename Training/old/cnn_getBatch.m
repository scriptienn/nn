function [spectra, labels] = cnn_getBatch(batchNumber, batchSize, permutation, set)
	%--------------------------------------------------------------------------
	%GET_BATCH Loads the spectra and labels of the given batch of the given
	% set.
	% Input: 
	%   batchNumber = the current batch number
	%   set = 'train' or 'test'
	%
	% Output:
	%   spectra = the spectra of the phonemes: HxWxDxN
	%   labels = the phoneme labels of the phonemes: N
	
	%error messaging
	if(~strcmp(set, 'train') && ~strcmp(set, 'test'))
		error('Expected set to be either train or test');
	end
	
	%initialise variables
	[~, ~, ~, dataPath, ~] = setPaths();
	
	startIdx = batchSize * (batchNumber-1) + 1;
	endIdx = batchSize * batchNumber;
	if endIdx > length(permutation)
		endIdx = length(permutation)
	end
	indices = permutation(startIdx:endIdx);
	
	%get metaData
	filePath = strcat(dataPath, '\', set, '\');
	metaDataPath = strcat(filePath, '_main.mat');
	metaData = load(metaDataPath, 'phonemeFileNames', 'phonemes', 'average');
	
	average = metaData.average; % Get average spectrogram (note that the test set gets its average from the train set)
	
	filenames = metaData.phonemeFileNames;
	labels = cell2mat(metaData.phonemes);
	labels = labels(indices);
	
	%first phoneme within batch
	file = strcat(filePath,filenames{indices(1)});
	data = load(file, 'phonemeSpectrogram');
	spectrum = data.phonemeSpectrogram;
	
	%variables to save data
	[H, W, D] = size(spectrum);
	spectra = single(zeros(H, W, D, batchSize));
	
	%save data of first phoneme
	spectra(:,:,:,1) = (spectrum-average)*255;
	
	%all other phonemes within batch
	for i=2:batchSize
		idx = indices(i);
		
		%load data
		file = strcat(filePath,filenames{idx});
		data = load(file, 'phonemeSpectrogram');
		spectrum = data.phonemeSpectrogram;
		%save data
		spectra(:,:,:,i) = (spectrum-average)*255;
    end
end