function [net, info] = cnn_traincommon(net, getBatch, nrOfTrainExamples, nrOfTestExamples, varargin)
% Common file for standard CNN training on our data set
% 
% Adapted from:
% CNN_TRAIN   Demonstrates training a CNN
%    CNN_TRAIN() is an example learner implementing stochastic gradient
%    descent with momentum to train a CNN for image classification.
%    It can be used with different datasets by providing a suitable
%    getBatch function.

[~, ~, ~, ~, experimentPath] = setPaths();
%opts.train = [] ;
%opts.val = [] ;
opts.numEpochs = 10 ;%changed!
opts.batchSize = 32 ;%changed!
opts.useGpu = true ;
opts.learningRate = [0.01 0.008 0.006 0.004 0.002 0.001]; %changed!
opts.continue = true ;
opts.learningRateChanged = false ;
opts.expDir = experimentPath ;
opts.conserveMemory = false ; % default parameter; may need to be changed if relevant
opts.sync = true ;
opts.prefetch = false ;
opts.weightDecay = 0.0005 ; % default parameter; may need to be changed
opts.momentum = 0.9 ; % default parameter; may need to be changed
opts.errorType = 'multiclass' ; % default parameter; may need to be changed
opts.errorOncePerNEEpochs = true; % only validate on the test set once every NE epochs
opts.NE = 2;
opts.plotDiagnostics = true ;
opts = vl_argparse(opts, varargin) ;
% Adagrad:
opts.ada_master_stepsize = 1e-2;
opts.ada_fudge_factor = 1e-6;

if ~exist(opts.expDir, 'dir'), mkdir(opts.expDir) ; end
%{
if isempty(opts.train), opts.train = find(imdb.images.set==1) ; end
if isempty(opts.val), opts.val = find(imdb.images.set==2) ; end
if isnan(opts.train), opts.train = [] ; end
%}

% -------------------------------------------------------------------------
%                                                    Network initialization
% -------------------------------------------------------------------------

for i=1:numel(net.layers)
  if ~strcmp(net.layers{i}.type,'conv'), continue; end
  net.layers{i}.filtersMomentum = zeros(size(net.layers{i}.filters), ...
    class(net.layers{i}.filters)) ;
  net.layers{i}.biasesMomentum = zeros(size(net.layers{i}.biases), ...
    class(net.layers{i}.biases)) ; %#ok<*ZEROLIKE>
  net.layers{i}.historicalFilterGradients = zeros(size(net.layers{i}.filters), ...
      class(net.layers{i}.filters));
  net.layers{i}.historicalBiasGradients = zeros(size(net.layers{i}.biases), ...
      class(net.layers{i}.biases));
  if ~isfield(net.layers{i}, 'filtersLearningRate')
    net.layers{i}.filtersLearningRate = 1 ;
  end
  if ~isfield(net.layers{i}, 'biasesLearningRate')
    net.layers{i}.biasesLearningRate = 1 ;
  end
  if ~isfield(net.layers{i}, 'filtersWeightDecay')
    net.layers{i}.filtersWeightDecay = 1 ;
  end
  if ~isfield(net.layers{i}, 'biasesWeightDecay')
    net.layers{i}.biasesWeightDecay = 1 ;
  end
end

if opts.useGpu
  net = vl_simplenn_move(net, 'gpu') ;
  for i=1:numel(net.layers)
    if ~strcmp(net.layers{i}.type,'conv'), continue; end
    net.layers{i}.filtersMomentum = gpuArray(net.layers{i}.filtersMomentum) ;
    net.layers{i}.biasesMomentum = gpuArray(net.layers{i}.biasesMomentum) ;
  end
end

% -------------------------------------------------------------------------
%                                                        Train and validate
% -------------------------------------------------------------------------

rng(0) ;

if opts.useGpu
  one = gpuArray(single(1)) ;
else
  one = single(1) ;
end

info.train.objective = [] ;
info.train.error = [] ;
info.train.topFiveError = [] ;
info.train.speed = [] ;
info.val.objective = [] ;
info.val.error = [] ;
info.val.topFiveError = [] ;
info.val.speed = [] ;

lr = 0 ;
res = [] ;
for epoch=1:opts.numEpochs
  prevLr = lr ;
  lr = opts.learningRate(min(epoch, numel(opts.learningRate))) ;
  
  % fast-forward to where we stopped
  modelPath = @(ep) fullfile(opts.expDir, sprintf('net-epoch-%d.mat', ep));
  modelFigPath = fullfile(opts.expDir, 'net-train.pdf') ;
  if opts.continue
    if exist(modelPath(epoch),'file'), continue ; end
    if epoch > 1
      fprintf('resuming by loading epoch %d\n', epoch-1) ;
      load(modelPath(epoch-1), 'net', 'info') ;
    end
  end

  train = randperm(nrOfTrainExamples); % changed!
  val = randperm(nrOfTestExamples);

  info.train.objective(end+1) = 0 ;
  info.train.error(end+1) = 0 ;
  info.train.topFiveError(end+1) = 0 ;
  info.train.speed(end+1) = 0 ;
  
  if ~opts.errorOncePerNEEpochs || mod(epoch, opts.NE) == 1
    info.val.objective(end+1) = 0 ;
    info.val.error(end+1) = 0 ;
    info.val.topFiveError(end+1) = 0 ;
    info.val.speed(end+1) = 0 ;
  end

  % reset momentum if needed
  if prevLr ~= lr || (opts.continue && opts.learningRateChanged)
    opts.learningRateChanged = false;
    fprintf('learning rate changed (%f --> %f): resetting momentum\n', prevLr, lr) ;
    for l=1:numel(net.layers)
      if ~strcmp(net.layers{l}.type, 'conv'), continue ; end
      net.layers{l}.filtersMomentum = 0 * net.layers{l}.filtersMomentum ;
      net.layers{l}.biasesMomentum = 0 * net.layers{l}.biasesMomentum ;
    end
  end

  for t=1:opts.batchSize:nrOfTrainExamples % walk through batches within train set
      % get next image batch and labels
      batch = train(t:min(t+opts.batchSize-1, nrOfTrainExamples)) ; % changed!
      batch_time = tic ;
      fprintf('training: epoch %02d: processing batch %3d of %3d ...', epoch, ...
          fix(t/opts.batchSize)+1, ceil(nrOfTrainExamples/opts.batchSize)) ; % debug message; changed!
      [im, labels] = getBatch(batch, 'train') ; %changed! parameters: indices, binNr, set
      %if opts.prefetch
      %  nextBatch = train(t+opts.batchSize:min(t+2*opts.batchSize-1, trainBinSize)) ; % if prefetch used, may need to be changed to suit our batch structure
      %  getBatch(imdb, nextBatch) ;
      %end
      if opts.useGpu
          im = gpuArray(im) ;
      end
      
      % backprop
      net.layers{end}.class = labels ;
      res = vl_simplenn(net, im, one, res, ...
          'conserveMemory', opts.conserveMemory, ...
          'sync', opts.sync) ;
      clear im
      % gradient step
      for l=1:numel(net.layers)
          if ~strcmp(net.layers{l}.type, 'conv'), continue ; end
          
          %{
          gradient_filter = res(l).dzdw{1};
          net.layers{l}.historicalFilterGradients = net.layers{l}.historicalFilterGradients + gradient_filter .^ 2;          
          adjusted_filter_gradient = gradient_filter ./ (opts.ada_fudge_factor + sqrt(net.layers{l}.historicalFilterGradients));
          
          gradient_bias = res(l).dzdw{2};
          net.layers{l}.historicalBiasGradients = net.layers{l}.historicalBiasGradients + gradient_bias .^ 2;          
          adjusted_bias_gradient = gradient_bias ./ (opts.ada_fudge_factor + sqrt(net.layers{l}.historicalBiasGradients));
          
          net.layers{l}.filters = net.layers{l}.filters - opts.ada_master_stepsize .* adjusted_filter_gradient;
          net.layers{l}.biases  = net.layers{l}.biases  - opts.ada_master_stepsize .* adjusted_bias_gradient;
          %}
          
          net.layers{l}.filtersMomentum = ...
              opts.momentum * net.layers{l}.filtersMomentum ...
              - (lr * net.layers{l}.filtersLearningRate) * ...
              (opts.weightDecay * net.layers{l}.filtersWeightDecay) * net.layers{l}.filters ...
              - (lr * net.layers{l}.filtersLearningRate) / numel(batch) * res(l).dzdw{1} ;
          
          net.layers{l}.biasesMomentum = ...
              opts.momentum * net.layers{l}.biasesMomentum ...
              - (lr * net.layers{l}.biasesLearningRate) * ....
              (opts.weightDecay * net.layers{l}.biasesWeightDecay) * net.layers{l}.biases ...
              - (lr * net.layers{l}.biasesLearningRate) / numel(batch) * res(l).dzdw{2} ;
          
          net.layers{l}.filters = net.layers{l}.filters + net.layers{l}.filtersMomentum ;
          net.layers{l}.biases = net.layers{l}.biases + net.layers{l}.biasesMomentum ;
      end
      
      % print information
      batch_time = toc(batch_time) ;
      speed = numel(batch)/batch_time ;
      
      info.train = updateError(opts, info.train, net, res, batch_time) ;
      
      fprintf(' %.2f s (%.1f images/s)', batch_time, speed) ;
      n = t + numel(batch) - 1;
      fprintf(' err %.1f err5 %.1f', ...
          info.train.error(end)/n*100, info.train.topFiveError(end)/n*100) ;
      fprintf('\n') ;
      
      % debug info
      if opts.plotDiagnostics && (~opts.errorOncePerNEEpochs || mod(epoch, opts.NE) == 1)
          figure(2) ; vl_simplenn_diagnose(net,res) ; drawnow ;
      end
  end % next batch within trainset
  
  % If we only check errors once per NE epochs, check if we are at the NE-th (N.B. we start at 1)
  if ~opts.errorOncePerNEEpochs || mod(epoch, opts.NE) == 1
    % evaluation on validation set
    for t=1:opts.batchSize:nrOfTestExamples % changed!
        batch_time = tic ;
        batch = val(t:min(t+opts.batchSize-1, nrOfTestExamples)) ; % changed!
        fprintf('validation: epoch %02d: processing batch %3d of %3d ...', epoch, ...
            fix(t/opts.batchSize)+1, ceil(nrOfTestExamples/opts.batchSize)) ; % debug message; changed!
        [im, labels] = getBatch(batch, 'test') ; %changed! parameters: indices, set
        %if opts.prefetch
        %  nextBatch = val(t+opts.batchSize:min(t+2*opts.batchSize-1, numel(val))) ; % if prefetch used, may need to be changed to suit our batch structure
        %  getBatch(imdb, nextBatch) ;
        %end
        if opts.useGpu
            im = gpuArray(im) ;
        end
        
        net.layers{end}.class = labels ;
        res = vl_simplenn(net, im, [], res, ...
            'disableDropout', true, ...
            'conserveMemory', opts.conserveMemory, ...
            'sync', opts.sync) ;
        clear im
        % print information
        batch_time = toc(batch_time) ;
        speed = numel(batch)/batch_time ;
        info.val = updateError(opts, info.val, net, res, batch_time) ;
        
        fprintf(' %.2f s (%.1f images/s)', batch_time, speed) ;
        n = t + numel(batch) - 1;
        fprintf(' err %.1f err5 %.1f', ...
            info.val.error(end)/n*100, info.val.topFiveError(end)/n*100) ;
        fprintf('\n') ;
    end
  end
  
  info.train.objective(end) = info.train.objective(end) / numel(train) ;
  info.train.error(end) = info.train.error(end) / numel(train)  ;
  info.train.topFiveError(end) = info.train.topFiveError(end) / numel(train) ;
  info.train.speed(end) = numel(train) / info.train.speed(end) ;
  
  % If we only check errors once per NE epochs, check if we are at the NE-th (N.B. we start at 1)
  if ~opts.errorOncePerNEEpochs || mod(epoch, opts.NE) == 1
    info.val.objective(end) = info.val.objective(end) / numel(val) ;
    info.val.error(end) = info.val.error(end) / numel(val) ;
    info.val.topFiveError(end) = info.val.topFiveError(end) / numel(val) ;
    info.val.speed(end) = numel(val) / info.val.speed(end) ;
  end
  
  % save
  save(modelPath(epoch), 'net', 'info');

  % If we only check errors once per NE epochs, check if we are at the NE-th (N.B. we start at 1)
  if ~opts.errorOncePerNEEpochs || mod(epoch, opts.NE) == 1
    figure(1) ; clf ;
    subplot(1,2,1) ;
    semilogy(1:epoch, info.train.objective, 'k') ; hold on ;
    semilogy(1:opts.NE:epoch, info.val.objective, 'b') ;
    xlabel('training epoch') ; ylabel('energy') ;
    grid on ;
    h=legend('train', 'val') ;
    set(h,'color','none');
    title('objective') ;
    subplot(1,2,2) ;
    switch opts.errorType
      case 'multiclass'
        plot(1:epoch, info.train.error, 'k') ; hold on ;
        plot(1:epoch, info.train.topFiveError, 'k--') ;
        plot(1:opts.NE:epoch, info.val.error, 'b') ;
        plot(1:opts.NE:epoch, info.val.topFiveError, 'b--') ;
        h=legend('train','train-5','val','val-5') ;
      case 'binary'
        plot(1:epoch, info.train.error, 'k') ; hold on ;
        plot(1:opts.NE:epoch, info.val.error, 'b') ;
        h=legend('train','val') ;
    end
    grid on ;
    xlabel('training epoch') ; ylabel('error') ;
    set(h,'color','none') ;
    title('error') ;
    drawnow ;
    print(1, modelFigPath, '-dpdf') ;
  end
end

% -------------------------------------------------------------------------
function info = updateError(opts, info, net, res, speed)
% -------------------------------------------------------------------------
predictions = gather(res(end-1).x) ;
sz = size(predictions) ;
n = prod(sz(1:2)) ;

labels = net.layers{end}.class ;
info.objective(end) = info.objective(end) + sum(double(gather(res(end).x))) ;
info.speed(end) = info.speed(end) + speed ;
switch opts.errorType
  case 'multiclass'
    [~,predictions] = sort(predictions, 3, 'descend') ;
    error = ~bsxfun(@eq, predictions, reshape(labels, 1, 1, 1, [])) ;
    info.error(end) = info.error(end) +....
      sum(sum(sum(error(:,:,1,:))))/n ;
    info.topFiveError(end) = info.topFiveError(end) + ...
      sum(sum(sum(min(error(:,:,1:5,:),[],3))))/n ;
  case 'binary'
    error = bsxfun(@times, predictions, labels) < 0 ;
    info.error(end) = info.error(end) + sum(error(:))/n ;
end
end

end