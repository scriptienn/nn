function [net, info] = cnn_timitsainath(varargin)
% Defines common TIMIT convnet; parameters taken from Sainath et. al
% 
% Adapted from:
% CNN_MNIST  Demonstrated MatConvNet on MNIST

% Change depending on your file structure
%run(fullfile(fileparts(mfilename('fullpath')),'..','..','files','matconvnet-1.0-beta8','matlab','vl_setupnn.m')) ;

[~, ~, matconvnetPath, inputPath] = setPaths();

run(strcat(matconvnetPath, '\matlab\vl_setupnn.m'));

opts.dataDir = fullfile('data','mnist') ; % only needed in set-up script, if at all
opts.expDir = fullfile('data','mnist-baseline') ; % needs to be changed to some directory to store experimental results (may be big files)
opts.imdbPath = fullfile(opts.expDir, 'imdb.mat'); % only needed in set-up script, if at all
opts.train.batchSize = 100 ; % needs to be changed to our own batch size
opts.train.numEpochs = 100 ; % probably needs to be changed
opts.train.continue = true ;
opts.train.useGpu = false ;
opts.train.learningRate = 0.003 ; % needs to be tweaked once everything runs
opts.train.expDir = opts.expDir ;
opts = vl_argparse(opts, varargin) ;

trainMetaDataPath = strcat(inputPath, '\train\_main.mat');
trainMetaData = load(trainMetaDataPath, 'phonemes');

testMetaDataPath = strcat(inputPath, '\test\_main.mat');
testMetaData = load(testMetaDataPath, 'phonemes');

nrOfTrainExamples = length(trainMetaData.phonemes);%changed!
nrOfTestExamples = length(testMetaData.phonemes);
% --------------------------------------------------------------------
%                                                         Prepare data
% --------------------------------------------------------------------

%{
% needs to be changed or removed; reads in the data set
if exist(opts.imdbPath, 'file')
  imdb = load(opts.imdbPath) ;
else
  imdb = getMnistImdb(opts) ;
  mkdir(opts.expDir) ;
  save(opts.imdbPath, '-struct', 'imdb') ;
end
%}

% --------------------------------------------------------------------
%                                                         Define network
% --------------------------------------------------------------------

% Define a network similar to Sainath et al., 2015
f=1/100 ;
net.layers = {} ;
net.layers{end+1} = struct('type', 'conv', ... % 256 2-dimensional 9x9 convolutional filters
                           'filters', f*randn(9,9,1,256, 'single'), ...
                           'biases', zeros(1, 256, 'single'), ...
                           'stride', 1, ...
                           'pad', 0) ;
net.layers{end+1} = struct('type', 'pool', ... % max pooling by a factor of 2
                           'method', 'max', ...
                           'pool', [2 2], ...
                           'stride', 2, ...
                           'pad', 0) ;
net.layers{end+1} = struct('type', 'conv', ... % 256 256-dimensional 4x3 convolutional filters
                           'filters', f*randn(4,3,256,256, 'single'),...
                           'biases', zeros(1,256,'single'), ...
                           'stride', 1, ...
                           'pad', 0) ;
net.layers{end+1} = struct('type', 'conv', ... % 1024 256-dimensional fully connected units
                           'filters', f*randn(1,1,256,1024, 'single'),...
                           'biases', zeros(1,1024,'single'), ...
                           'stride', 1, ...
                           'pad', 0) ;
net.layers{end+1} = struct('type', 'conv', ... % 1024 1024-dimensional fully connected units
                           'filters', f*randn(1,1,1024,1024, 'single'),...
                           'biases', zeros(1,1024,'single'), ...
                           'stride', 1, ...
                           'pad', 0) ;
net.layers{end+1} = struct('type', 'conv', ... % 1024 1024-dimensional fully connected units
                           'filters', f*randn(1,1,1024,1024, 'single'),...
                           'biases', zeros(1,1024,'single'), ...
                           'stride', 1, ...
                           'pad', 0) ;
net.layers{end+1} = struct('type', 'conv', ... % 39 1024-dimensional fully connected output units
                           'filters', f*randn(1,1,1024,39, 'single'),...
                           'biases', zeros(1,39,'single'), ...
                           'stride', 1, ...
                           'pad', 0) ;
net.layers{end+1} = struct('type', 'softmaxloss') ; % softmax classification layer
% N.B. Assumes that there are 39 output classes (i.e. assumes discussed phoneme
% mapping is in place)

% --------------------------------------------------------------------
%                                                                Train
% --------------------------------------------------------------------

% Take the mean out and make GPU if needed
%imdb.images.data = bsxfun(@minus, imdb.images.data, mean(imdb.images.data,4)) ; % may need to be removed; subtracts mean per spectrogram pixel per channel
%if opts.train.useGpu % not applicable until GPU mode is used
%  imdb.images.data = gpuArray(imdb.images.data) ;
%end

[net, info] = cnn_traincommon(net, @getBatch, ...
    nrOfTrainExamples, nrOfTestExamples) ; % changed!

%--------------------------------------------------------------------------
    function [spectra, labels] = getBatch( indices, set)
        %--------------------------------------------------------------------------
        %GET_BATCH Loads the spectra and labels of the given batch of the given
        %set and indices
        % Input:
        %   indices = indices of all phonemes within the batch
        %   set = 'train' or 'test'
        %
        % Output:
        %   spectra = the spectra of the phonemes: HxWxDxN
        %   labels = the phoneme labels of the phonemes: N
        
        %error messaging
        if(~strcmp(set, 'train') && ~strcmp(set, 'test'))
            error('Expected set to be either train or test');
        end
        
        %initialise variables
        dataPath = inputPath;
        batchSize = numel(indices);
        
        %get metaData
        filePath = strcat(dataPath, '\', set, '\');
        metaDataPath = strcat(filePath, '_main.mat');
        metaData = load(metaDataPath, 'phonemeFileNames', 'phonemes', 'average');
        
        average = metaData.average; % Get average spectrogram (note that the test set gets its average from the train set)
        
        filenames = metaData.phonemeFileNames;
        labels = cell2mat(metaData.phonemes);
        labels = labels(indices);
        
        %first phoneme within batch
        file = strcat(filePath,filenames{indices(1)});
        data = load(file, 'phonemeSpectrogram');
        spectrum = data.phonemeSpectrogram;
        
        %variables to save data
        [H, W, D] = size(spectrum);
        spectra = single(zeros(H, W, D, batchSize));
        
        %save data of first phoneme
        spectra(:,:,:,1) = (spectrum-average)*255;
        
        %all other phonemes within batch
        for i=2:batchSize
            idx = indices(i);
            
            %load data
            file = strcat(filePath,filenames{idx});
            data = load(file, 'phonemeSpectrogram');
            spectrum = data.phonemeSpectrogram;
            %save data
            spectra(:,:,:,i) = (spectrum-average)*255;
        end  
    end
end

%{
% --------------------------------------------------------------------
function imdb = getMnistImdb(opts) % not needed for our data, but we might need something similar to set up our data? (e.g. run preprocess script if data doesn't exist yet)
% --------------------------------------------------------------------
files = {'train-images-idx3-ubyte', ...
         'train-labels-idx1-ubyte', ...
         't10k-images-idx3-ubyte', ...
         't10k-labels-idx1-ubyte'} ;

mkdir(opts.dataDir) ;
for i=1:4
  if ~exist(fullfile(opts.dataDir, files{i}), 'file')
    url = sprintf('http://yann.lecun.com/exdb/mnist/%s.gz',files{i}) ;
    fprintf('downloading %s\n', url) ;
    gunzip(url, opts.dataDir) ;
  end
end

f=fopen(fullfile(opts.dataDir, 'train-images-idx3-ubyte'),'r') ;
x1=fread(f,inf,'uint8');
fclose(f) ;
x1=permute(reshape(x1(17:end),28,28,60e3),[2 1 3]) ;

f=fopen(fullfile(opts.dataDir, 't10k-images-idx3-ubyte'),'r') ;
x2=fread(f,inf,'uint8');
fclose(f) ;
x2=permute(reshape(x2(17:end),28,28,10e3),[2 1 3]) ;

f=fopen(fullfile(opts.dataDir, 'train-labels-idx1-ubyte'),'r') ;
y1=fread(f,inf,'uint8');
fclose(f) ;
y1=double(y1(9:end)')+1 ;

f=fopen(fullfile(opts.dataDir, 't10k-labels-idx1-ubyte'),'r') ;
y2=fread(f,inf,'uint8');
fclose(f) ;
y2=double(y2(9:end)')+1 ;

imdb.images.data = single(reshape(cat(3, x1, x2),28,28,1,[])) ;
imdb.images.labels = cat(2, y1, y2) ;
imdb.images.set = [ones(1,numel(y1)) 3*ones(1,numel(y2))] ;
imdb.meta.sets = {'train', 'val', 'test'} ;
imdb.meta.classes = arrayfun(@(x)sprintf('%d',x),0:9,'uniformoutput',false) ;
    
%}

