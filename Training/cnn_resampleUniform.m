function evenlyDistributedIndices = cnn_resampleUniform(phonemes, examplesPerPhoneme)
%% cnn_resampleUniform
% Author: Thomas Churchman
%
% Generate a "permutation" of class indices in the given input vector, 
% such that from each class stated in the input vector n examples will be
% present in the permutation, with n := examplesPerPhoneme.
%
% Input:
% - phonemes: the input vector; a vector of class labels
% - examplesPerPhoneme: the number of examples ("indices") of each class 
%   label to put in the permutation.
%
% Output:
% - evenlyDistributedIndices: a vector of indices corresponding to the
%   resampled indices from the input vector.

    phonemesMat = cell2mat(phonemes);
    
    % Gather all unique phonemes present in the data
    uniquePhonemes = unique(phonemesMat);
    trainPermutation = [];
    for i=1:numel(uniquePhonemes)
        % For each present phoneme
        phone = uniquePhonemes(i);
        
        % Count the number of times this phoneme is present
        n = sum(phonemesMat == phone);
        
        % Calculate how many times we will repeat the phoneme indices
        repCount = floor(examplesPerPhoneme / n);
        
        % Find the indices of this phoneme
        indices = find(phonemesMat == phone);
        
        % Extend the collection permutation by appending the indices of
        % this phoneme "rep" times
        trainPermutation = [trainPermutation; repmat(indices, repCount, 1)];
        
        % Calculate how many individual indices we have to add to the
        % collection permutation (i.e., we can not append a vector, for
        % example, 0.283 times, so we have to select individual indices to
        % add at random to get an even distribution).
        appendRandPermCount = mod(examplesPerPhoneme, n);
        permIndices = randperm(numel(indices), appendRandPermCount);
        trainPermutation = [trainPermutation; indices(permIndices)];
    end
    
    % Shuffle
    finalIndices = randperm(numel(trainPermutation));
    evenlyDistributedIndices = trainPermutation(finalIndices);
end