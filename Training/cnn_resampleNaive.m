function evenlyDistributedIndices = cnn_resampleNaive(phonemes)
%% cnn_resampleNaive
% Author: Thomas Churchman
%
% Naively generate a "permutation" of indices of the given input vector, 
% such that all input elements are present and each element class is 
% represented evenly.
%
% Input:
% - phonemes: the input vector; a vector of class labels
%
% Output:
% - evenlyDistributedIndices: a vector of indices corresponding to the
%   resampled indices from the input vector.

    phonemesMat = cell2mat(phonemes);
    
    % Gather all unique phonemes present in the data
    uniquePhonemes = unique(phonemesMat);
    
    % Find the most frequently occurring phoneme
    mostFrequent = mode(phonemesMat); 
    
    % Count the number of times the most frequently occurring phoneme is
    % present
    frequency = sum(phonemesMat == mostFrequent);

    % Add the indices of the most frequently occurring phoneme to the 
    % collection permutation 
    trainPermutation = find(phonemesMat == mostFrequent);
    
    for i=1:numel(uniquePhonemes)
        % For each present phoneme
        phone = uniquePhonemes(i);
        if phone == mostFrequent
            % Skip the most frequent phoneme
            continue
        end
        
        % Count the number of times this phoneme is present
        n = sum(phonemesMat == phone);
        
        % Calculate how many times we will repeat the phoneme indices
        repCount = floor(frequency / n);
        
        % Find the indices of this phoneme
        indices = find(phonemesMat == phone);
        
        % Extend the collection permutation by appending the indices of
        % this phoneme "rep" times
        trainPermutation = [trainPermutation; repmat(indices, repCount, 1)];

        % Calculate how many individual indices we have to add to the
        % collection permutation (i.e., we can not append a vector, for
        % example, 6.283 times, so we have to select individual indices to
        % add at random to get an even distribution).
        appendRandPermCount = mod(frequency, n);
        permIndices = randperm(numel(indices), appendRandPermCount);
        trainPermutation = [trainPermutation; indices(permIndices)];
    end
    
    % Shuffle
    finalIndices = randperm(numel(trainPermutation));
    evenlyDistributedIndices = trainPermutation(finalIndices);
end