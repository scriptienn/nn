function cnn_run(batches, set)
    
    global cluster
    global spectra
    global labels
    
    global opts
	global batchOpts
	
	batchSize = opts.batchSize;
	
	if(strcmp(set, 'train'))
		permutation = batchOpts.trainPermutation;
	else
		permutation = batchOpts.valPermutation;
    end
    
    [spectra, labels] = cnn_getBatch(1, batchSize, permutation, set);
    job = batch(cluster, 'cnn_getBatch', 2, {1, batchSize, permutation, set});
    
    
    
    for batchNum=1:batches
        % For each batch
        % Fetch loaded batch data
        job.wait();
        data = job.fetchOutputs();
        spectra = data{1};
        labels = data{2};
        
        if batchNum < batches
            % This is not the last batch:
            % asyncronously load next batch data
            job = batch(cluster, 'cnn_getBatch', 2, {batchNum+1, batchSize, permutation, set});
        end
        
        cnn_processBatch(spectra, labels);
    end
end