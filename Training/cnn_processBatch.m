function data = cnn_processBatch(data, batchNum, spectra, labels)
%% cnn_processBatch
% Author: Thomas Churchman
% 
% Process a training/validation batch (i.e, a set of spectra with
% corresponding labels that are fed to the network in a single update).
%
% Input:
% - data: structure containing batch information (e.g., the network,
%   network info, set type ('train'/'validation') and options).
% - batchNum: the number this batch is in the epoch (e.g., the fiftieth
%   batch in the epoch to be processed).
% - spectra: the spectra ("images") that are fed to the network
%   (Height x Width x Depth x Number of Spectrograms).
% - labels: the labels that correspond to the spectrograms.
%
% Output:
% - data: structure containig batch information after
%   processing/validation. Contains the same fields as were put into the
%   function. Fields "net" and "info" are updated.

    % Make data aliases (as long as data is not changed, it's not copied)
    net = data.net;
    info = data.info;
    set = data.set;
    opts = data.opts;
    batchOpts = data.batchOpts;
    learningRate = data.learningRate;
    
    %spectra = createDeltas(spectra);
    %avg = createDeltas(batchOpts.train.average);
    avg = batchOpts.train.average;
    
    numExamples = numel(labels);
    for i=1:numExamples
        spectra(:,:,:,i) = (spectra(:,:,:,i) - avg);
    end
    
    % Prepare data for use
    if opts.useGpu
        spectra = gpuArray(spectra);
        topLevelDerivative = gpuArray(single(1));
    else
        topLevelDerivative = single(1);
    end

    % Set class labels
    net.layers{end}.class = cell2mat(labels);

    
    if strcmp(set, 'train')
        % Run network and collect result
        res = [];
        res = vl_simplenn(net, spectra, topLevelDerivative, res, ...
            'conserveMemory', opts.conserveMemory, ...
            'sync', opts.sync, ...
            'autoencoder', true);
        clear spectra;

        % Back-propagation
        for layer = 1:numel(net.layers)
            if ~strcmp(net.layers{layer}.type, 'conv')
                continue
            end

            if net.layers{layer}.learnFilters
                %gradient_filter = res(layer).dzdw{1} + net.layers{layer}.filters * opts.l2Lambda;
                gradient_filter = res(layer).dzdw{1};
                net.layers{layer}.historicalFilterGradients = net.layers{layer}.historicalFilterGradients + gradient_filter .^ 2;          
                adjusted_filter_gradient = gradient_filter ./ (opts.ada_fudge_factor + sqrt(net.layers{layer}.historicalFilterGradients));
                net.layers{layer}.filters = net.layers{layer}.filters - opts.ada_master_stepsize .* adjusted_filter_gradient;
            elseif strcmp(net.layers{layer}.type, 'convt') % convt without filter learning
                net.layers{layer}.filters = dec_cnn_filtertranspose(net.layers{net.layers{layer}.encoderIndex}.filters);
            end
            
            if net.layers{layer}.learnBiases
                %gradient_bias = res(layer).dzdw{2} + net.layers{layer}.biases * opts.l2Lambda;
                gradient_bias = res(layer).dzdw{2};
                net.layers{layer}.historicalBiasGradients = net.layers{layer}.historicalBiasGradients + gradient_bias .^ 2;          
                adjusted_bias_gradient = gradient_bias ./ (opts.ada_fudge_factor + sqrt(net.layers{layer}.historicalBiasGradients));
                net.layers{layer}.biases  = net.layers{layer}.biases  - opts.ada_master_stepsize .* adjusted_bias_gradient;
            end
            
            %{
            net.layers{layer}.filtersMomentum = ...
                opts.momentum * net.layers{layer}.filtersMomentum ...
                - (learningRate * net.layers{layer}.filtersLearningRate) * ...
                (opts.weightDecay * net.layers{layer}.filtersWeightDecay) * net.layers{layer}.filters ...
                - (learningRate * net.layers{layer}.filtersLearningRate) / numel(labels) * res(layer).dzdw{1} ;

            net.layers{layer}.biasesMomentum = ...
                opts.momentum * net.layers{layer}.biasesMomentum ...
                - (learningRate * net.layers{layer}.biasesLearningRate) * ...
                (opts.weightDecay * net.layers{layer}.biasesWeightDecay) * net.layers{layer}.biases ...
                - (learningRate * net.layers{layer}.biasesLearningRate) / numel(labels) * res(layer).dzdw{2};

            net.layers{layer}.filters = net.layers{layer}.filters + net.layers{layer}.filtersMomentum;
            net.layers{layer}.biases = net.layers{layer}.biases + net.layers{layer}.biasesMomentum;
            %}
        end

        [info.train, batchInfo] = cnn_updateError(opts, info.train, info.phoneGroups, net, res, numExamples);

        % n = (batchNum-1) * opts.batchSize + numExamples;
        n = info.train.numExamples(end);
        fprintf(' err %.1f err5 %.1f; avg err %.1f avg err5 %.1f; ', ...
              batchInfo.error*100, batchInfo.topFiveError*100, ...
              info.train.error(end)/n*100, info.train.topFiveError(end)/n*100);

        if opts.plotDiagnostics
            figure(2);
            vl_simplenn_diagnose(net,res); 
            drawnow;
        end

        % Prepare function result
        data.net = net;
        data.info = info;
    elseif strcmp(set, 'test')
        res = [];
        res = vl_simplenn(net, spectra, [], res, ...
            'disableDropout', true, ...
            'conserveMemory', opts.conserveMemory, ...
            'sync', opts.sync, ...
            'autoencoder', true);
        clear spectra
        
        [info.val, batchInfo] = cnn_updateError(opts, info.val, info.phoneGroups, net, res, numExamples);
        
        % n = (batchNum-1) * opts.batchSize + numExamples;
        n = info.val.numExamples(end);
        fprintf(' err %.1f err5 %.1f; avg err %.1f avg err5 %.1f; ', ...
              batchInfo.error*100, batchInfo.topFiveError*100, ...
              info.val.error(end)/n*100, info.val.topFiveError(end)/n*100);

        % Prepare function result
        data.net = net;
        data.info = info;
    end
    
    %{
    net.layers{end}.class = labels;
	res = vl_simplenn(net, im, one, res, ...
          'conserveMemory', opts.conserveMemory, ...
          'sync', opts.sync) ;
    %}
end