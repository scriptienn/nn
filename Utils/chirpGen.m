function wave = chirpGen(startFreq, endFreq, samples, fs)
    length = (samples-1)/fs;
    t = 0:1/fs:length;
    wave = chirp(t,startFreq,length,endFreq, 'quadratic');
    wave = wave ./ 100;
end