function predictions = utils_getPredictions(net, input, class)
%% utils_getPredictions
% Author: Thomas Churchman
%
% Get the predictions of a network for a given input.

    net.layers{end}.class = class;
    res = vl_simplenn(net, input, [], [], 'disableDropout', true);
    
    % Cast to normal array (from GPU array if applicable)
    predictions = gather(res(end-1).x);
    
    [~,predictions] = sort(predictions, 3, 'descend');
end