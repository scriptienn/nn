function wave = spectrogramToWave(spectrogram, varargin)
%% spectrogramToWave
% Author: Thomas Churchman
%
% Convert a spectrogram to a waveform represented by that spectrogram.
%
% Input:
% - spectrogram: The spectrogram to convert to a waveform
% 
% Input Options:
% - nfft (400): Number of points for which the spectrogram was generated
% - fs (16000): Sampling-rate in Hz of the waveform
% - samples (2554): Number of samples the waveform should minimally 
%   consist of
% - windowSize (266): Size of fourier windows in samples
% - overlap (121): Size of overlap between windows in samples
%
% Output:
% - wave: The waveform generated from the spectrogram
%
% Example:
% w = spectrogramToWave(s, 'nfft', 1024);

    % Default settings
    opts.nfft = 400;
    opts.fs = 16000;
    opts.samples = 2554;
    opts.windowSize = floor(opts.fs/60);
    
    frameRate = floor(opts.fs/110);
    opts.overlap = opts.windowSize - frameRate;
    
    opts = vl_argparse(opts, varargin);
    
    invSpecgram = invspecgram(spectrogram, opts.nfft, opts.fs, opts.windowSize, opts.overlap);
    invSpecgram = padarray(invSpecgram, [(opts.samples - length(invSpecgram)) 0], 'post');
    
    wave = invSpecgram;
end