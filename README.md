What is this?
=============

This project can be seen as a wrapper for [Matconvnet](http://www.vlfeat.org/matconvnet/) to train a Convolutional Neural Network on the [TIMIT](https://catalog.ldc.upenn.edu/LDC93S1) dataset.

Using the code
==============

To use the code add the code's root to MATLAB's search path, such as by browsing to the code root folder in MATLAB. Next, run `setup`; this adds all code to MATLAB's path. Finally, create `setPaths.m` and point all the paths to the correct location.

Example of `setPaths.m`:
```
function [timitPath, outputPath, matconvnetPath, inputPath, experimentPath] = setPaths()
    timitPath      = 'path/to/timit';                      % (e.g.: ./timit/TIMIT)
    outputPath     = 'path/to/data/output';                % (e.g.: ./output/data)
    matconvnetPath = 'path/to/matconvnet';                 % (e.g.: ./matconvnet)
    inputPath      = 'path/to/training/and/testing/input'; % (e.g.: ./output/data)
    experimentPath = 'path/to/experiment/storage';         % (e.g.: ./output/experiment)
end
```

### Preprocessing
##### Compiling `saveSpectrogram`
Before preprocessing the dataset, you need to compile `saveSpectrogram.cpp`. 

To compile `saveSpectrogram.cpp`, run the following command in MATLAB's command window:
```
mex 'Dataset Preprocessing\saveSpectrogram.cpp' -output 'Dataset Preprocessing\saveSpectrogram'
```
For debugging run:
```
mex -g -v 'Dataset Preprocessing\saveSpectrogram.cpp' -output 'Dataset Preprocessing\saveSpectrogram'
```

##### Preprocessing
Run `processDataset`.

### Training
##### Compiling `cnn_run`
Before running the training code, you need to compile `cnn_run.cpp`. The code uses the Boost library for multithreading. Precompiled versions of the library for a Visual Studio compiler can be found [here](http://boost.teeks99.com/). The code has only been tested with `Boost 1.57.0`.

To compile `cnn_run`, run the following command in MATLAB's command window:
```
mex -L"path/to/boost/lib64-msvc-10.0" -l"libboost_thread-vc100-mt-1_57" -I"path/to/boost" Training/cnn_run.cpp -output Training/cnn_run
```
For debugging run:
```
mex -g -v -L"path/to/boost/lib64-msvc-10.0" -l"libboost_thread-vc100-mt-1_57" -I"path/to/boost" Training/cnn_run.cpp -output Training/cnn_run
```
###### Note:
For older versions of MATLAB, you might have to specify each library to compile against explicitly. In that case, run:
```
mex -g -L"path/to/boost/lib64-msvc-10.0" -l"libboost_thread-vc100-mt-1_57" -l"libboost_date_time-vc100-mt-1_57" -l"libboost_system-vc100-mt-1_57" -l"libboost_chrono-vc100-mt-1_57" -I"path/to/boost" -v Training/cnn_run.cpp -output Training/cnn_run
```
##### Training
Run `cnn_start`. Every epoch the intermediate network will be saved.

###  Reading .bin files
###### Compiling
To read .bin files, you need to compile `readSpectrogram.cpp`. 

To compile `readSpectrogram.cpp`, run the following command in MATLAB's command window:
```
mex readSpectrogram.cpp
```

###### Reading
To correctly read a binary spectrogram file, you need to know the dimensions of the spectrogram inside the binary file. These dimensions are supplied by other means (e.g., through the _main.mat-files in the case of preprocessing).

```
height x width x depth single precision matrix :: readSpectrogram(string path, int height, int width, int depth);
```
For example:
```
spectrogram = readSpectrogram('path/to/spectrogram.bin', 201, 16, 1);
```

Repository Structure
=============
| Folder | Explanation |
|---|---| 
| ```./Autoencoder Jordi``` | Contains (Jordi's) autoencoder code |
| ```./Compiling``` | Some notes on compiling |
| ```./Dataset Preprocessing``` | Functions to preprocess the TIMIT dataset |
| ```./Input Reconstruction``` | Contains (Thomas') code for reconstructing network inputs
| ```./MatConvNet extensions``` | Contains (Jordi's) code for MatConvNet extensions (unpool & switches) |
| ```./Phone Mapping``` | Code to fold and map phones |
| ```./Plotting``` | Some utility functions for plotting |
| ```./Postprocessing``` | Functions to postprocess a trained network |
| ```./Spectrograms``` | Some additional functions for working with spectrograms |
| ```./Test Code``` | Some very raw testing functions |
| ```./Toolboxes``` | External toolboxes | 
| ```./Training``` | Code for training classifier networks |
| ```./Training with Decoding``` | Code for training autoencoders / decoders |
| ```./Utils``` | Utility functions | 
| ```./Diede ``` | Contains (Diede's) deconvolution code | 