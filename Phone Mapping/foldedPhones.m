function phones = foldedPhones
%% foldedPhones
% Author: Thomas Churchman
% 
% Gets the array of folded phones.

    phones = unique(cellfun(@foldPhone, allPhones, 'UniformOutput', false));
end