function phones = allPhones
%% allPhones
% Author: Thomas Churchman
%
% Represents all phones that are to be found in the TIMIT dataset.

    phones = {    ...
        'bcl',      ...
        'b',        ...
        'dcl',      ...
        'd',        ...
        'gcl',      ...
        'g',        ...
        'pcl',      ...
        'p',        ...
        'tcl',      ...
        't',        ...
        'kcl',      ...
        'k',        ...
        'dx',       ...
        ... % 'q',  NOTE: should not be fed to the network
        'jh',       ...
        'ch',       ...
        's',        ...
        'sh',       ...
        'z',        ...
        'zh',       ...
        'f',        ...
        'th',       ...
        'v',        ...
        'dh',       ...
        'm',        ...
        'n',        ...
        'ng',       ...
        'em',       ...
        'en',       ...
        'eng',      ...
        'nx',       ...
        'l',        ...
        'r',        ... 
        'w',        ...
        'y',        ...
        'hh',       ...
        'hv',       ...
        'el',       ...
        'iy',       ...
        'ih',       ...
        'eh',       ...
        'ey',       ...
        'ae',       ...
        'aa',       ...
        'aw',       ...
        'ay',       ...
        'ah',       ...
        'ao',       ...
        'oy',       ...
        'ow',       ...
        'uh',       ...
        'uw',       ...
        'ux',       ...
        'er',       ...
        'ax',       ...
        'ix',       ...
        'axr',      ...
        'ax-h',     ...
        'pau',      ...
        'epi',      ...
        'h#',      ...
    };
end