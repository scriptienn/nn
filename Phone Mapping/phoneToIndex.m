function idx = phoneToIndex(phone)
%% phoneToIndex
% Author: Thomas Churchman
%
% Maps phones to numerical indices.

    idx = find(strcmp(foldedPhones, phone));
    if isempty(idx)
        error(['Phone not recognized ''', phone, '''.']);
    end
end