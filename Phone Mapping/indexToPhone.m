function phone = indexToPhone(idx)
%% indexToPhone
% Author: Thomas Churchman
%
% Maps numerical indices to phones.

    phones = foldedPhones();
    
    if idx < 1 || idx > length(phones)
        error(['Expected idx to be numerical and between 1 and ', num2str(length(phones)), ' (inclusive) (got: ', num2str(idx), ').']);
    end
    
    phone = phones{idx};
end