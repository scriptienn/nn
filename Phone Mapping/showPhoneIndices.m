%% showPhoneIndices
% Author: Diede Kemper

for i=1:48
    phone = indexToPhone(i);
    disp([num2str(i) ', ' phone]);
end