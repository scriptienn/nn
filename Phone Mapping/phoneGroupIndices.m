function indices = phoneGroupIndices()
%% phoneGroupIndices
% Author: Thomas Churchman
%
% Generate a vector of phone group mapping indices. For example,
% if phones 3, 5 and 13 belong to a group, then it could be that
% indices(3) = indices(5) = indices(13) = 3.
% If phone 10 doesn't belong to a (non-singleton) group, then 
% indices(10) = 10.
%
% In effect, indices will be an array with values pointing to the index of
% the first phone in the group that the phone on the index of the value belongs 
% to. (E.g., indices(phoneToIndex('cl')) = phoneToIndex('sil'), and 
% indices(phoneToIndex('b')) = phoneToIndex('b')).
% It can be used to efficiently map vectors of phones to vectors of phone
% groups.
%
% Non-singleton groups are: {sil, cl, vcl, epi}, {el, l}, {en, n}, 
% {sh, zh}, {ao, aa}, {ih, ix}, {ah, ax}.

    phones = foldedPhones();
    
    indices = 1:length(phones);

    indices(phoneToIndex('cl'))  = phoneToIndex('sil');
    indices(phoneToIndex('vcl')) = phoneToIndex('sil');
    indices(phoneToIndex('epi')) = phoneToIndex('sil');
    
    indices(phoneToIndex('l')) = phoneToIndex('el');
    
    indices(phoneToIndex('n')) = phoneToIndex('en');
    
    indices(phoneToIndex('zh')) = phoneToIndex('sh');
    
    indices(phoneToIndex('aa')) = phoneToIndex('ao');
    
    indices(phoneToIndex('ix')) = phoneToIndex('ih');
    
    indices(phoneToIndex('ax')) = phoneToIndex('ah');
    
end