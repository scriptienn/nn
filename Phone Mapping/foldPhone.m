function phone = foldPhone(phone) 
%% foldPhone
% Authors: Diede Kemper, Thomas Churchman, Jordi Riemens
% 
% Map phone labels to other labels. Mainly this function "folds" certain
% allophones to a single group label.
% See: http://www.intechopen.com/books/speech-technologies/phoneme-recognition-on-the-timit-database
%
% NOTE: phone 'q' should not be included in the training/testing sets at
% all and must be discarded in the processing step.

    mapping = containers.Map;
    
    mapping('ux')  = 'uw';
    mapping('axr') = 'er';
    mapping('em')  = 'm';
    mapping('nx')  = 'n';
    mapping('eng') = 'ng';
    mapping('hv')  = 'hh';
    
    mapping('pcl') = 'cl';
    mapping('tcl') = 'cl';
    mapping('kcl') = 'cl';
    
    mapping('bcl') = 'vcl';
    mapping('dcl') = 'vcl';
    mapping('gcl') = 'vcl';
    
    mapping('h#')  = 'sil';
    mapping('pau') = 'sil';
    
    % Lee and Hon don't specify 'ax-h', but other authors seem to map it to
    % 'ah'.
    mapping('ax-h') = 'ah';
    
    if isKey(mapping, phone)
        phone = mapping(phone);
    end
end