function group = areWithinGroup(phone1, phone2)
%% areWithinGroup
% Author: Thomas Churchman
%
% Check whether two phones are within the same group.
% Non-singleton groups are: {sil, cl, vcl, epi}, {el, l}, {en, n}, 
% {sh, zh}, {ao, aa}, {ih, ix}, {ah, ax}.

    if isnumeric(phone1)
        phone1 = indexToPhone(phone1);
    end
    
    if isnumeric(phone2)  
        phone2 = indexToPhone(phone2);
    end
    
    mapping = containers.Map;
    
    mapping('cl')  = 'sil';
    mapping('vcl') = 'sil';
    mapping('epi') = 'sil';
    
    mapping('l')   = 'el';
    
    mapping('n')   = 'en';
    
    mapping('zh')  = 'sh';
    
    mapping('aa')  = 'ao';
    
    mapping('ix')  = 'ih';
    
    mapping('ax')  = 'ah';
    
    if isKey(mapping, phone1)
        phone1 = mapping(phone1);
    end
    if isKey(mapping, phone2)
        phone2 = mapping(phone2);
    end
    
    group = strcmp(phone1, phone2);
end