function [ phoneLabels ] = indicesToLabels( phoneIndices )
%% indicesToLabels 
% Author: Diede Kemper
%
% Turns the phoneIndices vector into a cell array of phone labels
%
% Input: 
% - phoneIndices = nrOfPhones x 1 vector
% 
% Output:
% - phoneLabels = nrOfPhones x 1 cell array of strings

nrOfPhones = length(phoneIndices);
phoneLabels = cell(nrOfPhones,1);

for i=1:nrOfPhones
    
    phoneLabels{i,1} = indexToPhone(phoneIndices(i));
    
end

end

