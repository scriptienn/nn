/*==========================================================
 * readSpectrogram.cpp
 * 
 * Read a spectrogram from a binary file.
 *
 * Author: Thomas Churchman, 2015
 *
 *========================================================*/

#include <iostream>
#include "mex.h"

using namespace std;

void _main();

/* Parses an integer from a scalar mxArray. */
int parseInt(const mxArray *scalarPtr)
{
	int i = (int) mxGetScalar(scalarPtr);
	return i;
}

/* Parses a char array (i.e., "string") from an mxArray. */
char * parseCharArray(const mxArray *charArrayPtr)
{
	char *chars = new char[mxGetN(charArrayPtr) + 1];
	mxGetString(charArrayPtr, chars, (int) mxGetN(charArrayPtr) + 1);

	return chars;
}

/* Code entry-point */
void mexFunction(
		int          	nlhs,
		mxArray      	*plhs[],
		int          	nrhs,
		const mxArray 	*prhs[]
		)
{
	/* Check for proper number of arguments. */
	if (nrhs != 4) 
	{
		mexErrMsgIdAndTxt("MATLAB:readSpectrogram:nargin", 
		"Invalid number of input arguments into READSPECTROGRAM.");
	} 
	else if (nlhs != 1) 
	{
		mexErrMsgIdAndTxt("MATLAB:readSpectrogram:nargout",
		"READSPECTROGRAM requires precisely one output argument.");
	}


	/* Process the input arguments. */
	char *fileName = parseCharArray(prhs[0]);
	int height = parseInt(prhs[1]);
	int width = parseInt(prhs[2]);
	int depth = parseInt(prhs[3]);

	/* Initialize variables. */
	int nElements = height * width * depth;
	float *spectrogram = (float *)mxCalloc(nElements, sizeof(float));

	/* Load file. */
	FILE* pFile = fopen(fileName, "rb");
	if (pFile == NULL)
	{
		mexPrintf("Could not open '%s'. Did you spell the filename correctly?\n", fileName);
		return;
	}
	
	fread(spectrogram, sizeof(*spectrogram), nElements, pFile);
	fclose(pFile);
	
	/* Create MATLAB data structure. */
	int dims = 3;
	int dimSize[] = { height, width, depth };
	mxArray *matlabSpectrogram = mxCreateNumericArray(dims, dimSize, mxSINGLE_CLASS, mxREAL);
	mxSetData(matlabSpectrogram, spectrogram);
	
	/* Set output pointers. */
	plhs[0] = matlabSpectrogram;
	
	/* Destruct. */
	delete fileName;
	fileName = NULL;
	
	return;
}
