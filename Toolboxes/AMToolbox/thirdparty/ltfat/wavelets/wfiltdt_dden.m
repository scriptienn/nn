function [h,g,a,info] = wfiltdt_dden(N)
%WFILTDT_DDEN  Double-Density Dual-Tree DWT filters 
%
%   Usage: [h,g,a] = wfiltdt_dden(N);
%
%   [h,g,a]=WFILTDT_DDEN(N) with N in {1,2} returns filters suitable
%   dor dual-tree double density complex wavelet transform. 
%
%   Examples:
%   ---------
%   :
%     wfiltdtinfo('dden1');
%
%   :
%     wfiltdtinfo('dden2');
% 
%   References:
%     I. Selesnick. The double-density dual-tree DWT. Signal Processing, IEEE
%     Transactions on, 52(5):1304-1314, May 2004.
%     
%     
%
%
%   Url: http://ltfat.sourceforge.net/doc/wavelets/wfiltdt_dden.php

% Copyright (C) 2005-2014 Peter L. Soendergaard <soender@users.sourceforge.net>.
% This file is part of LTFAT version 2.0.0
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% AUTHOR: Zdenek Prusa

info.istight = 1;
a = [2;2;2];

switch(N)
 case 1
    % Example 1. from the reference. 
    harr = [
        0.0691158205  0.0000734237  0.0001621689  0.0138231641  0.0003671189  0.0008108446
        0.3596612703  0.0003820788  0.0008438861  0.1825175668  0.0048473455  0.0107061875
        0.6657851023 -0.0059866448 -0.0136616968  0.5537956151  0.0129572726  0.0264224754
        0.4659189433 -0.0343385512 -0.0781278793  0.6403205201 -0.0061082309 -0.0424847245
       -0.0191014398 -0.0554428419 -0.0840435464  0.2024025378 -0.0656840149 -0.2095602589
       -0.1377522956  0.0018714327  0.2230705831 -0.1327035751 -0.0968519623 -0.0055184660
       -0.0087922813  0.1386271745  0.3945086960 -0.0714378446 -0.0211208454  0.6504107366
        0.0194794983  0.3321168878 -0.6566499317  0.0179754457  0.5492354832 -0.4735663386
        0.0000995795 -0.5661664438  0.2138977202  0.0085233088 -0.4154148634  0.0427795440
       -0.0002006352  0.1888634841  0            -0.0010031763  0.0377726968  0
    ];


case 2
    % Example 2. From the reference. 
    harr = [
         0.0116751500  0.0000002803  0.0000009631  0.0016678785  0.0000019623  0.0000067421
         0.1121045343  0.0000026917  0.0000092482  0.0427009907  0.0000502404  0.0001726122
         0.3902035988 -0.0000945824 -0.0003285657  0.2319241351  0.0002359631  0.0007854598
         0.6376600221 -0.0009828317 -0.0034113692  0.5459409911 -0.0003026422 -0.0016861130
         0.4515927116 -0.0032260080 -0.0098485834  0.6090383368 -0.0044343824 -0.0181424716
        -0.0177905271 -0.0033984723  0.0011435281  0.2145936637 -0.0123017187 -0.0350847982
        -0.1899509889  0.0053478454  0.0535846285 -0.1629587558 -0.0156330903  0.0180629832
        -0.0363317137  0.0269410607  0.0710003404 -0.1283958243  0.0044955076  0.1356963431
         0.0511638041  0.0499929334 -0.0732656061  0.0309676536  0.0781684245  0.0980877181
         0.0130979774 -0.0076424664 -0.2335672955  0.0373820215  0.1319270081 -0.1963413775
        -0.0081410874 -0.2115533011 -0.0478802585 -0.0038525812 -0.1244353736 -0.3762491967
        -0.0016378610 -0.1367235355  0.5808457358 -0.0053106600 -0.4465930970  0.5674107094
         0.0005650673  0.6180972127 -0.4014544851  0.0003304362  0.5772994700 -0.2017431422
         0.0000043492 -0.3981725189  0.0631717194  0.0001955983 -0.1972513705  0.0090245313
        -0.0000014745  0.0614116921  0            -0.0000103221  0.0087730988  0
    ];

  otherwise
        error('%s: No such filters.',upper(mfilename)); 

end

htmp=mat2cell(harr,size(harr,1),ones(1,size(harr,2)));

h(:,1) = cellfun(@(hEl)struct('h',hEl,'offset',-size(harr,2)/2+1),htmp(1:3),...
                   'UniformOutput',0);
h(:,2) = cellfun(@(hEl)struct('h',hEl,'offset',-size(harr,2)/2+1),htmp((4:6)),...
                   'UniformOutput',0);
               

g = h;

[info.defaultfirst, info.defaultfirstinfo] = fwtinit('symdden2');
[info.defaultleaf, info.defaultleafinfo] = ...
    deal(info.defaultfirst,info.defaultfirstinfo);





